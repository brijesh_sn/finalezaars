function equalHeight(group) {    
    var tallest = 0;    
    group.each(function() {       
        var thisHeight = jQuery(this).height();       
        if(thisHeight > tallest) {          
            tallest = thisHeight;       
        }    
    });    
    group.each(function() { jQuery(this).height(tallest); });
} 
function openNav() {
    document.getElementById("mySidenav").style.width = "100%";

   //document.getElementById("main").style.marginLeft = "260px";
   
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    // document.getElementById("main").style.marginLeft = "0";
   
}

jQuery(document).ready(function () {

var max = 50;
    jQuery('.input-text').keypress(function(e) {
        if (e.which < 0x20) {
            // e.which < 0x20, then it's not a printable character
            // e.which === 0 - Not a character
            return;     // Do nothing
        }
        if (this.value.length == max) {
            e.preventDefault();
        } else if (this.value.length > max) {
            // Maximum exceeded
            
            this.value = this.value.substring(0, max);
            }
    });
    jQuery('.input-text').addClass('validate-length maximum-length-50');

var width = jQuery(window).width();
if(width<768)
{
  var menu=jQuery('.custom-menu').html();
  console.log(menu);
jQuery('.main-menu').html(menu);
  jQuery('.custom-menu').html('');
}
else{
  jQuery('.main-menu').html('');
}
  jQuery( ".Burgermenu" ).click(function() {
  jQuery(this).hide();
  jQuery( ".closebtn" ).show();
  
});
    jQuery( ".closebtn" ).click(function() {
  jQuery(this).hide();
  jQuery( ".Burgermenu" ).show();
  
});

/**************************** menu start*********************/
jQuery(".top-search").click(function() {
        jQuery("#swipemobile").hide();
        jQuery("#searchmobile").slideToggle();
    });
    jQuery(".swipe-control").click(function() {
        jQuery("#searchmobile").hide();
        jQuery("#swipemobile").slideToggle("slow");
    });
/**************************** menu end*********************/


    var myGroup = jQuery('.navbar-nav');
    myGroup.on('show.bs.collapse','.collapse', function() {
      myGroup.find('.collapse.in').collapse('hide');


    });

  jQuery('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    video: true,
    videoWidth: '100%',
    nav:true,
    responsive:{
        0:{
            items:1,
            videoHeight: '200'
        },
        600:{
            items:1,
            videoHeight: '320'
        },
        1000:{
            items:1,
            videoHeight: '520'

        }
    }
});
   
  jQuery( ".view-more1" ).click(function() {
      var URL= jQuery('.base_url').val();

      URL=URL+'/skin/frontend/default/default/images/opc-ajax-loader.gif';
      
          jQuery.ias({
              container : ".category-products",
              item : "div.item",
              next : "a.next",
              pagination : '.pages',
              loader : "<img src='"+URL+"' />",
              triggerPageThreshold : 0
          });

     });
  jQuery( ".thumbnail1" ).hover(
  function() {
    jQuery(this).find(".custom-action").show();
  }, function() {
    jQuery(this).find(".custom-action").hide();
  });
});

jQuery(window).on('load',function() {
  // Run code
  //equalHeight(jQuery(".thumbnail1"));
 // equalHeight(jQuery(".thumbnail1 .product-name"));

  //equalHeight(jQuery(".home .detail .main-class .sub-child"));
  });


jQuery( window ).resize(function() {
var width = jQuery(window).width();
if(width<768)
{
  var menu=jQuery('.custom-menu').html();
  jQuery('.main-menu').html(menu);
  jQuery('.custom-menu').html('');
}
else{
  jQuery('.main-menu').html('');
}
jQuery('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    video: true,
    videoWidth: '100%',
    nav:true,
    responsive:{
        0:{
            items:1,
            videoHeight: '200'
        },
        600:{
            items:1,
            videoHeight: '320'
        },
        1000:{
            items:1,
            videoHeight: '520'

        }
    }
  });
jQuery( ".thumbnail1" ).hover(
  function() {
    jQuery(this).find(".custom-action").show();
  }, function() {
    jQuery(this).find(".custom-action").hide();
  });
  //  equalHeight(jQuery(".thumbnail1"));
 // equalHeight(jQuery(".thumbnail1 .product-name"));
  });

jQuery(document).ready(function () {

  
 
  jQuery(".navbar-nav > li.active").find('ul').addClass('in');
  jQuery(".navbar-nav > li.active").addClass('minus-icon');

  jQuery(".navbar-nav > li").click(function(){
   if(!jQuery(this).hasClass("minus-icon"))
   {
    jQuery('.navbar-nav > li.minus-icon').removeClass('minus-icon');
   }
    
    jQuery(this).toggleClass("minus-icon"); 
  });

});

jQuery(document).ready(function (){
   if(jQuery("#tier_counter").val())
      {
        var x=jQuery("#tier_counter").val();
        x=x++;
      }
      else
      {
		var x = 1;
      }
	  var counter = 0;
	  var x = 1;
	jQuery(document).on('click', '.add_clone_add', function(e) {
		counter++;
      jQuery(this).hide();
      jQuery(".tire_price_main").append("<div id='div_"+x+"'><input type='hidden' name='tier_price["+x+"][website_id]' value='0'/><input type='hidden' name='tier_price["+x+"][cust_group]' value='32000'/><div class='col-md-5 form-group'><input type='text' id='tier_price' name='tier_price["+x+"][price_qty]' value='' class='input-text form-control validate-number' placeholder='QTY'/></div><div class='col-md-5 form-group'><input type='text' id='tier_price' name='tier_price["+x+"][price]' value='' class='input-text form-control validate-number' placeholder='Price'/></div><div class='col-md-2 action_class nopadding'><a id='add_"+x+"' data-id='"+x+"' href='javascript:void(0);' class='add_clone_add'>+</a><a id='remove_"+x+"' data-id='"+x+"' href='javascript:void(0);'  class='remove_clone_add'>X</a></div></div>");
        x++;
    });
	jQuery(document).on('click', '.remove_clone_add', function(e) {
      
        e.preventDefault();
		
		//remove whole div
		var current_div_id = jQuery(this).attr("data-id");
		jQuery("#div_" + current_div_id).remove();
		jQuery(".add_clone_add").last().show();
		jQuery(".remove_clone_add").last().show();

    });
jQuery(document).on('click', '.add_clone_edit', function(e) {
	counter++;
      jQuery(this).hide();
      jQuery(".tire_price_main").append("<div id='div_"+x+"' class='input-container'><input type='hidden' name='tier_price["+x+"][website_id]' value='0'/><input type='hidden' name='tier_price["+x+"][cust_group]' value='32000'/><div class='col-md-5 form-group'><input type='text' id='tier_price' name='tier_price["+x+"][price_qty]' value='' class='input-text form-control validate-number' placeholder='QTY'/></div><div class='col-md-5 form-group'><input type='text' id='tier_price' name='tier_price["+x+"][price]' value='' class='input-text form-control validate-number' placeholder='Price'/></div><div class='col-md-2 action_class nopadding'><a id='add_"+x+"' data-id='"+x+"' href='javascript:void(0);' class='add_clone_edit'>+</a><a id='remove_"+x+"' data-id='"+x+"' href='javascript:void(0);' class='remove_clone_edit'>X</a></div></div>");
        x++;
    });
   
    jQuery(document).on('click', '.remove_clone_edit', function(e) {
		
		  e.preventDefault();
		
		//remove whole div
		var current_div_id = jQuery(this).attr("data-id");
		jQuery("#div_" + current_div_id).remove();
		jQuery(".add_clone_edit").last().show();
		jQuery(".remove_clone_edit").last().show();
      
       /* 

       if(jQuery(this).parent('div').parent('div').prev().find(".add_clone_edit").attr('class'))
       {
       
       
        jQuery(this).parent('div').parent('div').prev().find(".add_clone_edit").show();
        jQuery(this).parent('div').parent('div').remove(); 

        x--;
      }
      else
      {
       // jQuery(this).parent('div').parent('div').find('.input-text').attr('value',"");
        // jQuery(this).parent('div').parent('div').find('.input-text').attr('text',"");
       
      }*/

    });
    jQuery(document).on('click', '.remove_video', function(e) {
    var URL= jQuery('#base_url').val();
         URL = URL+"supplier/product/removevideo";
         var pid= jQuery('#product_id').val();
         var vid= jQuery('#value_id').val();
     jQuery.ajax({
               type: "POST",
               url: URL,
               data:{"pid":pid,"vid":vid},
               dataType: "json",
               success: function (result) {
               
                   if (result.successs) {
                      location.reload();
                   } else {
                    alert("Something went wrong please try again later..!!");  
                   }
               },

               
           });

        });

    jQuery(".super-attributes li input:checkbox").change(function() {
                    var ischecked= jQuery(this).is(':checked');
                    if(ischecked)
                    {
                      jQuery(this).parent().find('.main_input').show();
                    }
                    else
                    {
                     jQuery(this).parent().find('.main_input').hide();
                      jQuery(this).parent().find('.bootstrap-tagsinput').html('<input placeholder="" type="text">');
                    }
                }); 

    jQuery('#product_create_form').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});
});
/* 3-5-2017 rinkal equal height js */
equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 jQuery(container).each(function() {

   $el = jQuery(this);
   jQuery($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

jQuery(window).on('load',function() {
  equalheight('.products-grid .item');
});


jQuery(window).resize(function(){
  equalheight('.products-grid .item');
});

/***************** For Special Price validation **************/
jQuery(document).ready(function () {
	 jQuery('#special_price').on('blur', function() {        
		if(jQuery(this).val().length == 0) {				
			 jQuery('#special_price_from_date').removeClass('required-entry validation-failed');
			 jQuery('#special_price_to_date').removeClass('required-entry validation-failed');				 
			 jQuery('#advice-required-entry-special_price_from_date').css('display','none');
			 jQuery('#advice-required-entry-special_price_to_date').css('display','none');
			
		}else{
			 jQuery('#special_price_from_date').addClass('required-entry');
			 jQuery('#special_price_to_date').addClass('required-entry');
		}
	});

	jQuery('select[name=is_deal]').on('change', function() {
		if(jQuery(this).val() == 1){
			jQuery('#special_price').addClass('required-entry');				
		 }else{
			jQuery('#special_price').removeClass('required-entry validation-failed');								 
			jQuery('#advice-required-entry-special_price').css('display','none');				
		 }
	});
	 jQuery(document).on('click', '#create_product', function() {
		if(jQuery('#special_price').val() != '' || jQuery('#special_price_from_date').val()  != ''  || jQuery('#special_price_to_date').val()  != ''){
			//alert('Input can not be left blank');
			jQuery('#special_price').addClass('required-entry');
			//return false;
		}
		return true;
	 });

  /*********** Color drop-down set background color By:Sachin **************/

    var color = jQuery('select[name="color"] option');
    color.each(function() {
        jQuery(this).css({"background-color": jQuery(this).text(), "color": "#FFF"});
    });
	
});
/********************* End Here ****************/