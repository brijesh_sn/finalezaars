jQuery(function () {
  'use strict'
  var result   
  var currentFile
  var coordinates
  function updateResults (img, data) {
    var content
    if (!(img.src || img instanceof HTMLCanvasElement)) {
      //content = jQuery('<span>Loading image file failed</span>')
    } else {
      
      content = img;
    }
    result.children().replaceWith(content)
    result.children().attr('style','min-height:145px')
    result.children().attr('style','max-height:145px')
    if (data && data.exif) {
      displayExifData(data.exif)
    }
  }
  function displayImage (file, options) {
    currentFile = file
    if (!loadImage(
        file,
        updateResults,
        options
      )) {
      result.children().replaceWith(
        jQuery('<span>' +
          'Your browser does not support the URL or FileReader API.' +
          '</span>')
      )
    }
  }

  function dropChangeHandler (e) {
   result = jQuery('.banner')
    e.preventDefault()
    e = e.originalEvent
    var target = e.dataTransfer || e.target
    var file = target && target.files && target.files[0]
    var options = {
      maxWidth: result.width(),
      maxHeight: 150,
      height: 150,
      canvas: true,
      pixelRatio: window.devicePixelRatio,
      downsamplingRatio: 0.5,
      orientation: true
    }
    if (!file) {
      return
    }
    
    displayImage(file, options)
  }

  function dropChangeHandler1 (e) {
   result = jQuery('.logo')
    e.preventDefault()
    e = e.originalEvent
    var target = e.dataTransfer || e.target
    var file = target && target.files && target.files[0]
    var options = {
      maxWidth: result.width(),
      maxHeight: 150,
      height: 150,
      canvas: true,
      pixelRatio: window.devicePixelRatio,
      downsamplingRatio: 0.5,
      orientation: true
    }
    if (!file) {
      return
    }
    
    displayImage(file, options)
  }

  // Hide URL/FileReader API requirement message in capable browsers:
  if (window.createObjectURL || window.URL || window.webkitURL ||
      window.FileReader) {
   // result.children().hide()
  }

  jQuery(document)
    .on('dragover', function (e) {
      e.preventDefault()
      e = e.originalEvent
      e.dataTransfer.dropEffect = 'copy'
    })
    .on('drop', dropChangeHandler)

  jQuery('#banner')
    .on('change', dropChangeHandler)

    jQuery('#logo')
    .on('change', dropChangeHandler1)

})
