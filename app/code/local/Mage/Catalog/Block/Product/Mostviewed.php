<?php
class Mage_Catalog_Block_Product_Mostviewed extends Mage_Catalog_Block_Product_Abstract{
    public function __construct(){
        parent::__construct();

          $today = time();
       $startdate = $today - (60*60*24*15);
 
       $from = date("Y-m-d", $startdate);
       $to = date("Y-m-d", $today);
       
       $storeId = Mage::app()->getStore()->getId();
       $products = Mage::getResourceModel('reports/product_collection')
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('status', array('eq' => 1))
                            ->setStoreId($storeId)
                            ->addStoreFilter($storeId)
                            ->addViewsCount()
                            ->addViewsCount($from, $to)
                            ->addAttributeToFilter('type_id', array('neq' => 'grouped'))
                            ->setPageSize($productCount)
                            ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
                           
      
       
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        //Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
 
        
        $this->setProductCollection($products);


      
    }
}
?>