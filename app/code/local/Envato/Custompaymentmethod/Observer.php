<?php
class Envato_Custompaymentmethod_Observer {

    public function filterpaymentmethod(Varien_Event_Observer $observer) {
    /* call get payment method */
    $method = $observer->getEvent()->getMethodInstance();

    /*   get  Quote  */
    $quote = $observer->getEvent()->getQuote();

    $result = $observer->getEvent()->getResult();
        $method2 = Mage::helper('core')->isModuleEnabled('custompaymentmethod');

        /* Disable Your payment method for   adminStore */
    if($method->getCode()=='custompaymentmethod' ){
        if($method2){
            foreach ($quote->getAllItems() as $item) {
                // get Cart item product Type //
                if($item->getProductType() =='simple' || $item->getProductType() == 'configurable'):
                $result->isAvailable = true;
                else:
                $result->isAvailable = false;     
                endif;
            }   
        }
        }

    }
}