<?php 

class Envato_Custompaymentmethod_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $_code = 'custompaymentmethod';
    protected $_isInitializeNeeded = true;

   	protected $_canUseInternal = true;

 	protected $_canUseForMultishipping = true;
}
?>