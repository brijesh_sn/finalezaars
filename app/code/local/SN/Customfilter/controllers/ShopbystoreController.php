<?php 
class SN_Customfilter_ShopbystoreController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$this->loadLayout();  
        $this->renderLayout();
	}
	public function infoAction()
	{
		/*if($this->getRequest()->getPost())
		{*/

			$resource = Mage::getSingleton('core/resource');
			$readConnection = $resource->getConnection('core_read');
			$writeConnection = $resource->getConnection('core_write');
			$storeArray = array();
			$this->getRequest()->getPost('vid');

			$_productCollection = Mage::getModel('catalog/product')
                             ->getCollection()
                             ->addAttributeToSelect('creator_id')
                             ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
                             ->addAttributeToFilter('category_id', array('in' => 51));
            
            //print_r($_productCollection->getData()); die;
            foreach ($_productCollection as $_product) {
            	
            	$_product = Mage::getModel('catalog/product')->load($_product->getId());
            	
            	if($_product['creator_id'])
            	{array_push($storeArray, $_product['creator_id']);}
            	
            }
            
            Mage::getSingleton('core/session')->setStoredata(array_unique($storeArray));
            /*$block = $this->getLayout()->createBlock(
                'Mage_Core_Block_Template',
                'product_list.swatches',
                array('template' => 'searchnative/shop-by-store-main1.phtml')
            );*/
            $array=Mage::app()->getLayout()->createBlock('Mage_Core_Block_Template')
			    ->setData('area','frontend')
			    ->setTemplate('searchnative/shop-by-store-main1.phtml')
			    ->toHtml();
           
			   
			/*$array= Mage::getModel('shippinginfo/shippinginfo')->getCollection()
				->addFieldToFilter('status', array('eq' => '1'))
				->addFieldToFilter('state', array('like' => $this->getRequest()->getPost('vid')))
				->getData();
			$array = $array[0];*/
			echo json_encode(array('status' => 'true', 'message' => $array));
		/*}
		else
		{
				echo json_encode(array('status' => 'false', 'message' => 'something went wrong please try again later!!!'));
		}*/
	}
} 