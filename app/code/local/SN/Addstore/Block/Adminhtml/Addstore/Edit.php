<?php
      class SN_Addstore_Block_Adminhtml_Addstore_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
      {
          public function __construct()
          {
              parent::__construct();                     
              $this->_objectId = 'id';
              $this->_blockGroup = 'addstore';
              $this->_controller = 'adminhtml_addstore';           
              $this->_updateButton('save', 'label', Mage::helper('addstore')->__('Save Item')); 
              $this->_updateButton('delete', 'label', Mage::helper('addstore')->__('Delete Item'));  
			  
			  $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        		), -100);
				
				 $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('addstores_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'addstores_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'addstores_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        	";
          }           
          public function getHeaderText()  
          {  
              if( Mage::registry('addstore_data') && Mage::registry('addstore_data')->getId() ) {  
                  return Mage::helper('addstore')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('addstore_data')->getTitle()));  
              } else {  
                  return Mage::helper('addstore')->__('Add Item');  
              }  
          }  
      }