<?php
 
class SN_Addstore_Block_Adminhtml_Addstore_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('addstoreGrid');
        $this->setDefaultSort('addstore_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('addstore/addstore')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('addstore_id', array(
          'header'    => Mage::helper('addstore')->__('ID'),
          'align'     =>'right',
          'width'     => '10px',
          'index'     => 'addstore_id',
        ));
        $this->addColumn('supplier_id', array(
            'header'    => Mage::helper('addstore')->__('supplier id'),
            'width'     => '70px',
            'index'     => 'supplier_id',
        ));
		
		    $this->addColumn('authorized_representative', array(
          'header'    => Mage::helper('addstore')->__('Name'),
          'align'     =>'left',
          'index'     => 'authorized_representative',
          'width'     => '150px',
        ));
 
        
         $this->addColumn('address', array(
          'header'    => Mage::helper('addstore')->__('address'),
          'align'     =>'left',
          'index'     => 'address',
          'width'     => '150px',
        ));
          $this->addColumn('city', array(
          'header'    => Mage::helper('addstore')->__('city'),
          'align'     =>'left',
          'index'     => 'city',
          'width'     => '150px',
        ));
           $this->addColumn('state', array(
          'header'    => Mage::helper('addstore')->__('state'),
          'align'     =>'left',
          'index'     => 'state',
          'width'     => '150px',
        ));
         $this->addColumn('country', array(
          'header'    => Mage::helper('addstore')->__('country'),
          'align'     =>'left',
          'index'     => 'country',
          'width'     => '150px',
        ));
        $this->addColumn('status', array( 
            'header'    => Mage::helper('addstore')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
           'options'   => array(
                2 => 'Rejected',
                1 => 'Accepted',
                0 => 'Pending',
            ),
        ));
        return parent::_prepareColumns();
    }
	public function getRowUrl($row)
    {
       // return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
	
	protected function _prepareMassaction()
    {
        $this->setMassactionIdField('addstore_id');
        $this->getMassactionBlock()->setFormFieldName('addstore');
 
        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('addstore')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('addstore')->__('Are you sure?')
        ));
 
        //$statuses = Mage::getSingleton('addstore/addstore')->getOptionArray();
 
        //array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('addstore')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('addstore')->__('Status'),
                         'values' => array(
                      array(
                          'value'     => 2,
                          'label'     => Mage::helper('addstore')->__('Rejected'),
                      ),
                      array(
                          'value'     => 1,
                          'label'     => Mage::helper('addstore')->__('Accepted'),
                      ),
                      array(
                          'value'     => 0,
                          'label'     => Mage::helper('addstore')->__('Pending'),
                      ),
					  ),
        )
        )
        ));
        return $this;
    }
}