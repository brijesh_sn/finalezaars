<?php
      class SN_Addstore_Block_Adminhtml_Addstore_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
      {      
          public function __construct()
          {
              parent::__construct();
              $this->setId('addstore_tabs');
              $this->setDestElementId('edit_form');
              $this->setTitle(Mage::helper('addstore')->__('Addstore Information'));
          }   
          protected function _beforeToHtml()
          {
              $this->addTab('form_section', array(
                  'label'     => Mage::helper('addstore')->__('Item Information'),
                  'title'     => Mage::helper('addstore')->__('Item Information'),
                  'content'   => $this->getLayout()->createBlock('addstore/adminhtml_addstore_edit_tab_form')->toHtml(),
              ));
              return parent::_beforeToHtml();
          }
      }