<?php
      class SN_Addstore_Block_Adminhtml_Addstore_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
      {
          protected function _prepareForm()
          {
              $form = new Varien_Data_Form();
              $this->setForm($form);
              $fieldset = $form->addFieldset('addstore_form', array('legend'=>Mage::helper('addstore')->__('Item information')));
			  
			     $object = Mage::getModel('addstore/addstore')->load( $this->getRequest()->getParam('id') );
	  		   
			  
              $fieldset->addField('brand_name', 'text', array(
                  'label'     => Mage::helper('addstore')->__('Title'),
                  'class'     => 'required-entry',
                  'required'  => true,
                  'name'      => 'brand_name',
              ));
			  
			  $fieldset->addField('addstoreimage', 'file', array(
          			'label'     => Mage::helper('addstore')->__('Addstore Image'),
          			'required'  => false,
          			'name'      => 'addstoreimage',
	  			));
	  
	  		if( $object->getId() ){
		  		$tempArray = array(
				  'name'      => 'filethumbnail',
				  'style'     => 'display:none;',
			  	);
		  	$fieldset->addField($imgPath, 'thumbnail',$tempArray);
	  		}
	  
	  		$fieldset->addField('link', 'text', array(
          		'label'     => Mage::helper('addstore')->__('Link'),
          		'required'  => false,
          		'name'      => 'link',
	  		));
	  
	 		$fieldset->addField('target', 'select', array(
          		'label'     => Mage::helper('addstore')->__('Target'),
          		'name'      => 'target',
          		'values'    => array(
              		array(
                  		'value'     => '_blank',
                  		'label'     => Mage::helper('addstore')->__('Open in new window'),
              		),

              		array(
                  		'value'     => '_self',
                  		'label'     => Mage::helper('addstore')->__('Open in same window'),
              		),
          		),
      		));
	 
	  		$fieldset->addField('sort_order', 'text', array(
          		'label'     => Mage::helper('addstore')->__('Sort Order'),
          		'required'  => false,
          		'name'      => 'sort_order',
      		));
			
			
			$fieldset->addField('store_id','multiselect',array(
			'name'      => 'stores[]',
            'label'     => Mage::helper('addstore')->__('Store View'),
            'title'     => Mage::helper('addstore')->__('Store View'),
            'required'  => true,
			'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true)
			));

              $fieldset->addField('status', 'select', array(
                  'label'     => Mage::helper('addstore')->__('Status'),
                  'name'      => 'status',
                  'values'    => array(
                      array(
                          'value'     => 1,
                          'label'     => Mage::helper('addstore')->__('Active'),
                      ),
                      array(
                          'value'     => 0,
                          'label'     => Mage::helper('addstore')->__('Inactive'),
                      ),
                  ),
              ));
              $fieldset->addField('content', 'editor', array(
                  'name'      => 'content',
                  'label'     => Mage::helper('addstore')->__('Content'),
                  'title'     => Mage::helper('addstore')->__('Content'),
                  'style'     => 'width:98%; height:400px;',
                  'wysiwyg'   => false,
                  'required'  => true,
              ));
              if ( Mage::getSingleton('adminhtml/session')->getAddstoreData() )
              {
                  $form->setValues(Mage::getSingleton('adminhtml/session')->getAddstoreData());
                  Mage::getSingleton('adminhtml/session')->setAddstoreData(null);
              } elseif ( Mage::registry('addstore_data') ) {
                  $form->setValues(Mage::registry('addstore_data')->getData());
              }
              return parent::_prepareForm();
          }
      }