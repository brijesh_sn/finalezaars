<?php
class SN_Addstore_Block_Adminhtml_Addstore extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_Addstore';
    $this->_blockGroup = 'addstore';
    $this->_headerText = Mage::helper('addstore')->__('Addstore Manager');
    //$this->_addButtonLabel = Mage::helper('addstore')->__('Add Addstore');
    parent::__construct();
    $this->_removeButton('add');
  }
}