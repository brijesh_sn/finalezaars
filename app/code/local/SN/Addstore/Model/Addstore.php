<?php
class SN_Addstore_Model_Addstore extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('addstore/addstore'); // this is location of the resource file.
    }
}
