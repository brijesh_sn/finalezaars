<?php
	  class SN_Addstore_Adminhtml_AddstoreController extends Mage_Adminhtml_Controller_Action
      {
          protected function _initAction()
          {
              $this->loadLayout()
                  ->_setActiveMenu('addstore/items')
                  ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
              return $this;
          }   
          public function indexAction() {
              $this->_initAction();       
              $this->_addContent($this->getLayout()->createBlock('addstore/adminhtml_addstore'));
              $this->renderLayout();
          }
          public function editAction()
          {
              $addstoreId     = $this->getRequest()->getParam('id');
              $addstoreModel  = Mage::getModel('addstore/addstore')->load($addstoreId);
              if ($addstoreModel->getId() || $addstoreId == 0) {
                  Mage::register('addstore_data', $addstoreModel);
                  $this->loadLayout();
                  $this->_setActiveMenu('addstore/items');
                  $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
                  $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
                  $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
                  $this->_addContent($this->getLayout()->createBlock('addstore/adminhtml_addstore_edit'))
                       ->_addLeft($this->getLayout()->createBlock('addstore/adminhtml_addstore_edit_tabs'));
                  $this->renderLayout();
              } else {
                  Mage::getSingleton('adminhtml/session')->addError(Mage::helper('addstore')->__('Item does not exist'));
                  $this->_redirect('*/*/');
              }
          }
          public function newAction()
          {
              $this->_redirect('*/*/edit');
          }
          public function saveAction()
          {
              if ($data = $this->getRequest()->getPost()) {
			
			
			if(isset($_FILES['addstoreimage']['name']) && $_FILES['addstoreimage']['name'] != '') {
	  			 //this way the name is saved in DB
	  			$data['addstoreimage'] = $_FILES['addstoreimage']['name'];
				
				//Save Image Tag in DB for GRID View
				$imgName = $_FILES['addstoreimage']['name'];
				$imgPath = Mage::getBaseUrl('media')."Addstore/images/thumb/".$imgName;
				$data['filethumbgrid'] = '<img src="'.$imgPath.'" border="0" width="75" height="75" />';
			}
			
			$model = Mage::getModel('addstore/addstore');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();
				
				
				if(isset($_FILES['addstoreimage']['name']) && $_FILES['addstoreimage']['name'] != '') {
					try {	
						
						$path = Mage::getBaseDir('media')."/Addstore". DS ."images". DS ;
						/* Starting upload */							
						$uploader = new Varien_File_Uploader('addstoreimage');
						// Any extention would work
						$uploader->setAllowedExtensions(array('jpg','JPG','jpeg','gif','GIF','png','PNG'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						// We set media as the upload dir
						$uploader->save($path, $_FILES['addstoreimage']['name'] );
						
						
						//Create Thumbnail and upload
						$imgName = $_FILES['addstoreimage']['name'];
						$imgPathFull = $path.$imgName;
						$resizeFolder = "thumb";
						$imageResizedPath = $path.$resizeFolder.DS.$imgName;
						$imageObj = new Varien_Image($imgPathFull);
						$imageObj->constrainOnly(TRUE);
						$imageObj->keepAspectRatio(TRUE);
						$imageObj->resize(150,150);
						$imageObj->save($imageResizedPath);
						
						//Create View Size and upload
						$imgName = $_FILES['addstoreimage']['name'];
						$imgPathFull = $path.$imgName;
						$resizeFolder = "medium";
						$imageResizedPath = $path.$resizeFolder.DS.$imgName;
						$imageObj = new Varien_Image($imgPathFull);
						$imageObj->constrainOnly(TRUE);
						$imageObj->keepAspectRatio(TRUE);
						$imageObj->resize(400,400);
						$imageObj->save($imageResizedPath);
						
						
					} catch (Exception $e) {}
				}
				
				//Mage::helper('addstore')->generateXML();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('addstore')->__('Addstore was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('addstore')->__('Unable to find Addstore to save'));
        $this->_redirect('*/*/');
          }
          public function deleteAction()
          {
              if( $this->getRequest()->getParam('id') > 0 ) {
                  try {
                      $addstoreModel = Mage::getModel('addstore/addstore');
                      $addstoreModel->setId($this->getRequest()->getParam('id'))
                          ->delete();
                      Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                      $this->_redirect('*/*/');
                  } catch (Exception $e) {
                      Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                      $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                  }
              }
              $this->_redirect('*/*/');
          }
          /**

           * Product grid for AJAX request.

           * Sort and filter result for example.

           */
          public function gridAction()
          {
              $this->loadLayout();
              $this->getResponse()->setBody(
                     $this->getLayout()->createBlock('importedit/adminhtml_addstore_grid')->toHtml()
              );
          }
		  public function massStatusAction()
		  {
			  $ids = $this->getRequest()->getParam('addstore');
			  foreach($ids as $id)
			  {
				  $model = Mage::getModel('addstore/addstore');
				  $model->setId($id)
				  		->setStatus($this->getRequest()->getParam('status'))
						->save();
			  }
			  $this->_redirect('*/*/');
			  
		  }
		  public function massDeleteAction()
		  {
			  $ids = $this->getRequest()->getParam('addstore');
			  foreach($ids as $id)
			  {
				  $model = Mage::getModel('addstore/addstore');
				  $model->setId($id)
						->delete();
			  }
			  Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
			  $this->_redirect('*/*/');
		  }
      }	