<?php
$installer = $this;  //Getting Installer Class Object In A Variable
$installer->startSetup();
$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('addstore')};
CREATE TABLE {$this->getTable('addstore')} (
  `addstore_id` int(11) unsigned NOT NULL auto_increment,
  `contact_number` varchar(30) NOT NULL default '',
  `address` varchar(255) NOT NULL default '',
  `authorized_representative` varchar(30) NOT NULL default '',
  `city` varchar(30) NOT NULL default '',
  `state` varchar(30) NOT NULL default '',
  `country` varchar(30) NOT NULL default '',
  `supplier_id` int(11) unsigned NOT NULL,
  `status` smallint(6) NOT NULL default '0',
  PRIMARY KEY (`addstore_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
?>