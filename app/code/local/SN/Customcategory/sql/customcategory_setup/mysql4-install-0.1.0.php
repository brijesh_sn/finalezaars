<?php
$installer = $this;  //Getting Installer Class Object In A Variable
$installer->startSetup();
$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('customcategory')};
CREATE TABLE {$this->getTable('customcategory')} (
  `customcategory_id` int(11) unsigned NOT NULL auto_increment,
  `category_name` varchar(255) NOT NULL default '',
  `supplier_id` int(11) unsigned NOT NULL,
  `status` smallint(6) NOT NULL default '0',
  PRIMARY KEY (`customcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
?>