<?php
class SN_Customcategory_Model_Customcategory extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('customcategory/customcategory'); // this is location of the resource file.
    }
}
