<?php
class SN_Customcategory_Block_Adminhtml_Customcategory extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_Customcategory';
    $this->_blockGroup = 'customcategory';
    $this->_headerText = Mage::helper('customcategory')->__('Customcategory Manager');
    //$this->_addButtonLabel = Mage::helper('customcategory')->__('Add Customcategory');
    parent::__construct();
    $this->_removeButton('add');
  }
}