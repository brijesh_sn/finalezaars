<?php
      class SN_Customcategory_Block_Adminhtml_Customcategory_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
      {      
          public function __construct()
          {
              parent::__construct();
              $this->setId('customcategory_tabs');
              $this->setDestElementId('edit_form');
              $this->setTitle(Mage::helper('customcategory')->__('Customcategory Information'));
          }   
          protected function _beforeToHtml()
          {
              $this->addTab('form_section', array(
                  'label'     => Mage::helper('customcategory')->__('Item Information'),
                  'title'     => Mage::helper('customcategory')->__('Item Information'),
                  'content'   => $this->getLayout()->createBlock('customcategory/adminhtml_customcategory_edit_tab_form')->toHtml(),
              ));
              return parent::_beforeToHtml();
          }
      }