<?php
 
class SN_Customcategory_Block_Adminhtml_Customcategory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('customcategoryGrid');
        $this->setDefaultSort('customcategory_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('customcategory/customcategory')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('customcategory_id', array(
          'header'    => Mage::helper('customcategory')->__('ID'),
          'align'     =>'right',
          'width'     => '10px',
          'index'     => 'customcategory_id',
        ));
		
		    $this->addColumn('category_name', array(
          'header'    => Mage::helper('customcategory')->__('Category Name'),
          'align'     =>'left',
          'index'     => 'category_name',
          'width'     => '70px',
        ));
 
        $this->addColumn('supplier_id', array(
            'header'    => Mage::helper('customcategory')->__('supplier id'),
            'width'     => '350px',
            'index'     => 'supplier_id',
        ));

        $this->addColumn('status', array( 
            'header'    => Mage::helper('customcategory')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
           'options'   => array(
                2 => 'Rejected',
                1 => 'Accepted',
                0 => 'Pending',
            ),
        ));
        return parent::_prepareColumns();
    }
	public function getRowUrl($row)
    {
       // return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
	
	protected function _prepareMassaction()
    {
        $this->setMassactionIdField('customcategory_id');
        $this->getMassactionBlock()->setFormFieldName('customcategory');
 
        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('customcategory')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('customcategory')->__('Are you sure?')
        ));
 
        //$statuses = Mage::getSingleton('customcategory/customcategory')->getOptionArray();
 
        //array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('customcategory')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('customcategory')->__('Status'),
                         'values' => array(
                      array(
                          'value'     => 2,
                          'label'     => Mage::helper('customcategory')->__('Rejected'),
                      ),
                      array(
                          'value'     => 1,
                          'label'     => Mage::helper('customcategory')->__('Accepted'),
                      ),
                      array(
                          'value'     => 0,
                          'label'     => Mage::helper('customcategory')->__('Pending'),
                      ),
					  ),
        )
        )
        ));
        return $this;
    }
}