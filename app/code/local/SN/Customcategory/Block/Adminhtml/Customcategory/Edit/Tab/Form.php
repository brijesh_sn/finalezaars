<?php
      class SN_Customcategory_Block_Adminhtml_Customcategory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
      {
          protected function _prepareForm()
          {
              $form = new Varien_Data_Form();
              $this->setForm($form);
              $fieldset = $form->addFieldset('customcategory_form', array('legend'=>Mage::helper('customcategory')->__('Item information')));
			  
			     $object = Mage::getModel('customcategory/customcategory')->load( $this->getRequest()->getParam('id') );
	  		   
			  
              $fieldset->addField('brand_name', 'text', array(
                  'label'     => Mage::helper('customcategory')->__('Title'),
                  'class'     => 'required-entry',
                  'required'  => true,
                  'name'      => 'brand_name',
              ));
			  
			  $fieldset->addField('customcategoryimage', 'file', array(
          			'label'     => Mage::helper('customcategory')->__('Customcategory Image'),
          			'required'  => false,
          			'name'      => 'customcategoryimage',
	  			));
	  
	  		if( $object->getId() ){
		  		$tempArray = array(
				  'name'      => 'filethumbnail',
				  'style'     => 'display:none;',
			  	);
		  	$fieldset->addField($imgPath, 'thumbnail',$tempArray);
	  		}
	  
	  		$fieldset->addField('link', 'text', array(
          		'label'     => Mage::helper('customcategory')->__('Link'),
          		'required'  => false,
          		'name'      => 'link',
	  		));
	  
	 		$fieldset->addField('target', 'select', array(
          		'label'     => Mage::helper('customcategory')->__('Target'),
          		'name'      => 'target',
          		'values'    => array(
              		array(
                  		'value'     => '_blank',
                  		'label'     => Mage::helper('customcategory')->__('Open in new window'),
              		),

              		array(
                  		'value'     => '_self',
                  		'label'     => Mage::helper('customcategory')->__('Open in same window'),
              		),
          		),
      		));
	 
	  		$fieldset->addField('sort_order', 'text', array(
          		'label'     => Mage::helper('customcategory')->__('Sort Order'),
          		'required'  => false,
          		'name'      => 'sort_order',
      		));
			
			
			$fieldset->addField('store_id','multiselect',array(
			'name'      => 'stores[]',
            'label'     => Mage::helper('customcategory')->__('Store View'),
            'title'     => Mage::helper('customcategory')->__('Store View'),
            'required'  => true,
			'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true)
			));

              $fieldset->addField('status', 'select', array(
                  'label'     => Mage::helper('customcategory')->__('Status'),
                  'name'      => 'status',
                  'values'    => array(
                      array(
                          'value'     => 1,
                          'label'     => Mage::helper('customcategory')->__('Active'),
                      ),
                      array(
                          'value'     => 0,
                          'label'     => Mage::helper('customcategory')->__('Inactive'),
                      ),
                  ),
              ));
              $fieldset->addField('content', 'editor', array(
                  'name'      => 'content',
                  'label'     => Mage::helper('customcategory')->__('Content'),
                  'title'     => Mage::helper('customcategory')->__('Content'),
                  'style'     => 'width:98%; height:400px;',
                  'wysiwyg'   => false,
                  'required'  => true,
              ));
              if ( Mage::getSingleton('adminhtml/session')->getCustomcategoryData() )
              {
                  $form->setValues(Mage::getSingleton('adminhtml/session')->getCustomcategoryData());
                  Mage::getSingleton('adminhtml/session')->setCustomcategoryData(null);
              } elseif ( Mage::registry('customcategory_data') ) {
                  $form->setValues(Mage::registry('customcategory_data')->getData());
              }
              return parent::_prepareForm();
          }
      }