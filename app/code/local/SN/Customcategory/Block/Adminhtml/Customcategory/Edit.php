<?php
      class SN_Customcategory_Block_Adminhtml_Customcategory_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
      {
          public function __construct()
          {
              parent::__construct();                     
              $this->_objectId = 'id';
              $this->_blockGroup = 'customcategory';
              $this->_controller = 'adminhtml_customcategory';           
              $this->_updateButton('save', 'label', Mage::helper('customcategory')->__('Save Item')); 
              $this->_updateButton('delete', 'label', Mage::helper('customcategory')->__('Delete Item'));  
			  
			  $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        		), -100);
				
				 $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('customcategorys_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'customcategorys_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'customcategorys_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        	";
          }           
          public function getHeaderText()  
          {  
              if( Mage::registry('customcategory_data') && Mage::registry('customcategory_data')->getId() ) {  
                  return Mage::helper('customcategory')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('customcategory_data')->getTitle()));  
              } else {  
                  return Mage::helper('customcategory')->__('Add Item');  
              }  
          }  
      }