<?php
	  class SN_Customcategory_Adminhtml_CustomcategoryController extends Mage_Adminhtml_Controller_Action
      {
          protected function _initAction()
          {
              $this->loadLayout()
                  ->_setActiveMenu('customcategory/items')
                  ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
              return $this;
          }   
          public function indexAction() {
              $this->_initAction();       
              $this->_addContent($this->getLayout()->createBlock('customcategory/adminhtml_customcategory'));
              $this->renderLayout();
          }
          public function editAction()
          {
              $customcategoryId     = $this->getRequest()->getParam('id');
              $customcategoryModel  = Mage::getModel('customcategory/customcategory')->load($customcategoryId);
              if ($customcategoryModel->getId() || $customcategoryId == 0) {
                  Mage::register('customcategory_data', $customcategoryModel);
                  $this->loadLayout();
                  $this->_setActiveMenu('customcategory/items');
                  $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
                  $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
                  $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
                  $this->_addContent($this->getLayout()->createBlock('customcategory/adminhtml_customcategory_edit'))
                       ->_addLeft($this->getLayout()->createBlock('customcategory/adminhtml_customcategory_edit_tabs'));
                  $this->renderLayout();
              } else {
                  Mage::getSingleton('adminhtml/session')->addError(Mage::helper('customcategory')->__('Item does not exist'));
                  $this->_redirect('*/*/');
              }
          }
          public function newAction()
          {
              $this->_redirect('*/*/edit');
          }
          public function saveAction()
          {
              if ($data = $this->getRequest()->getPost()) {
			
			
			if(isset($_FILES['customcategoryimage']['name']) && $_FILES['customcategoryimage']['name'] != '') {
	  			 //this way the name is saved in DB
	  			$data['customcategoryimage'] = $_FILES['customcategoryimage']['name'];
				
				//Save Image Tag in DB for GRID View
				$imgName = $_FILES['customcategoryimage']['name'];
				$imgPath = Mage::getBaseUrl('media')."Customcategory/images/thumb/".$imgName;
				$data['filethumbgrid'] = '<img src="'.$imgPath.'" border="0" width="75" height="75" />';
			}
			
			$model = Mage::getModel('customcategory/customcategory');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();
				
				
				if(isset($_FILES['customcategoryimage']['name']) && $_FILES['customcategoryimage']['name'] != '') {
					try {	
						
						$path = Mage::getBaseDir('media')."/Customcategory". DS ."images". DS ;
						/* Starting upload */							
						$uploader = new Varien_File_Uploader('customcategoryimage');
						// Any extention would work
						$uploader->setAllowedExtensions(array('jpg','JPG','jpeg','gif','GIF','png','PNG'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						// We set media as the upload dir
						$uploader->save($path, $_FILES['customcategoryimage']['name'] );
						
						
						//Create Thumbnail and upload
						$imgName = $_FILES['customcategoryimage']['name'];
						$imgPathFull = $path.$imgName;
						$resizeFolder = "thumb";
						$imageResizedPath = $path.$resizeFolder.DS.$imgName;
						$imageObj = new Varien_Image($imgPathFull);
						$imageObj->constrainOnly(TRUE);
						$imageObj->keepAspectRatio(TRUE);
						$imageObj->resize(150,150);
						$imageObj->save($imageResizedPath);
						
						//Create View Size and upload
						$imgName = $_FILES['customcategoryimage']['name'];
						$imgPathFull = $path.$imgName;
						$resizeFolder = "medium";
						$imageResizedPath = $path.$resizeFolder.DS.$imgName;
						$imageObj = new Varien_Image($imgPathFull);
						$imageObj->constrainOnly(TRUE);
						$imageObj->keepAspectRatio(TRUE);
						$imageObj->resize(400,400);
						$imageObj->save($imageResizedPath);
						
						
					} catch (Exception $e) {}
				}
				
				//Mage::helper('customcategory')->generateXML();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('customcategory')->__('Customcategory was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('customcategory')->__('Unable to find Customcategory to save'));
        $this->_redirect('*/*/');
          }
          public function deleteAction()
          {
              if( $this->getRequest()->getParam('id') > 0 ) {
                  try {
                      $customcategoryModel = Mage::getModel('customcategory/customcategory');
                      $customcategoryModel->setId($this->getRequest()->getParam('id'))
                          ->delete();
                      Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                      $this->_redirect('*/*/');
                  } catch (Exception $e) {
                      Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                      $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                  }
              }
              $this->_redirect('*/*/');
          }
          /**

           * Product grid for AJAX request.

           * Sort and filter result for example.

           */
          public function gridAction()
          {
              $this->loadLayout();
              $this->getResponse()->setBody(
                     $this->getLayout()->createBlock('importedit/adminhtml_customcategory_grid')->toHtml()
              );
          }
		  public function massStatusAction()
		  {
			  $ids = $this->getRequest()->getParam('customcategory');
			  foreach($ids as $id)
			  {
				  $model = Mage::getModel('customcategory/customcategory');
				  $model->setId($id)
				  		->setStatus($this->getRequest()->getParam('status'))
						->save();
			  }
			  $this->_redirect('*/*/');
			  
		  }
		  public function massDeleteAction()
		  {
			  $ids = $this->getRequest()->getParam('customcategory');
			  foreach($ids as $id)
			  {
				  $model = Mage::getModel('customcategory/customcategory');
				  $model->setId($id)
						->delete();
			  }
			  Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
			  $this->_redirect('*/*/');
		  }
      }	