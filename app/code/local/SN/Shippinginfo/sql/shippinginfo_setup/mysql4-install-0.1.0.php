<?php
$installer = $this;  //Getting Installer Class Object In A Variable
$installer->startSetup();
$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('shippinginfo')};
CREATE TABLE {$this->getTable('shippinginfo')} (
  `shippinginfo_id` int(11) unsigned NOT NULL auto_increment,
  `state` varchar(255) NOT NULL default '',
  `content` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  PRIMARY KEY (`shippinginfo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
?>