<?php
      class SN_Shippinginfo_Block_Adminhtml_Shippinginfo_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
      {
          protected function _prepareForm()
          {
              $form = new Varien_Data_Form();
              $this->setForm($form);
              $fieldset = $form->addFieldset('shippinginfo_form', array('legend'=>Mage::helper('shippinginfo')->__('Item information')));
			  
			   $object = Mage::getModel('shippinginfo/shippinginfo')->load( $this->getRequest()->getParam('id') );
	  		 
			  
              $fieldset->addField('state', 'text', array(
                  'label'     => Mage::helper('shippinginfo')->__('State'),
                  'class'     => 'required-entry',
                  'required'  => true,
                  'name'      => 'state',
              ));
			  
			 
	  		$fieldset->addField('content', 'text', array(
          		'label'     => Mage::helper('shippinginfo')->__('content'),
          		'required'  => false,
          		'name'      => 'content',
	  		));
	       $fieldset->addField('status', 'select', array(
                  'label'     => Mage::helper('shippinginfo')->__('Status'),
                  'name'      => 'status',
                  'values'    => array(
                      array(
                          'value'     => 1,
                          'label'     => Mage::helper('shippinginfo')->__('Active'),
                      ),
                      array(
                          'value'     => 0,
                          'label'     => Mage::helper('shippinginfo')->__('Inactive'),
                      ),
                  ),
              ));
             
              if ( Mage::getSingleton('adminhtml/session')->getShippinginfoData() )
              {
                  $form->setValues(Mage::getSingleton('adminhtml/session')->getShippinginfoData());
                  Mage::getSingleton('adminhtml/session')->setShippinginfoData(null);
              } elseif ( Mage::registry('shippinginfo_data') ) {
                  $form->setValues(Mage::registry('shippinginfo_data')->getData());
              }
              return parent::_prepareForm();
          }
      }