<?php
      class SN_Shippinginfo_Block_Adminhtml_Shippinginfo_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
      {
          public function __construct()
          {
              parent::__construct();                     
              $this->_objectId = 'id';
              $this->_blockGroup = 'shippinginfo';
              $this->_controller = 'adminhtml_shippinginfo';           
              $this->_updateButton('save', 'label', Mage::helper('shippinginfo')->__('Save Item')); 
              $this->_updateButton('delete', 'label', Mage::helper('shippinginfo')->__('Delete Item'));  
			  
			  $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        		), -100);
				
				 $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('shippinginfos_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'shippinginfos_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'shippinginfos_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        	";
          }           
          public function getHeaderText()  
          {  
              if( Mage::registry('shippinginfo_data') && Mage::registry('shippinginfo_data')->getId() ) {  
                  return Mage::helper('shippinginfo')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('shippinginfo_data')->getTitle()));  
              } else {  
                  return Mage::helper('shippinginfo')->__('Add Item');  
              }  
          }  
      }