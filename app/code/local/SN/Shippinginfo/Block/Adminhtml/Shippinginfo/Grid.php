<?php
 
class SN_Shippinginfo_Block_Adminhtml_Shippinginfo_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('shippinginfoGrid');
        $this->setDefaultSort('shippinginfo_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('shippinginfo/shippinginfo')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('shippinginfo_id', array(
          'header'    => Mage::helper('shippinginfo')->__('ID'),
          'align'     =>'right',
          'width'     => '10px',
          'index'     => 'shippinginfo_id',
        ));
		
	
 
        $this->addColumn('state', array(
          'header'    => Mage::helper('shippinginfo')->__('State'),
          'align'     =>'left',
          'index'     => 'state',
          'width'     => '70px',
        ));
 
        $this->addColumn('content', array(
            'header'    => Mage::helper('shippinginfo')->__('Description'),
            'width'     => '350px',
            'index'     => 'content',
        ));
		
	
		
        $this->addColumn('status', array( 
            'header'    => Mage::helper('shippinginfo')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
           'options'   => array(
                1 => 'Active',
                0 => 'Inactive',
            ),
        ));
        return parent::_prepareColumns();
    }
	public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
	
	
}