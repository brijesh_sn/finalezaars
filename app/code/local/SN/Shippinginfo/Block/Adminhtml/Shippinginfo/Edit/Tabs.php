<?php
      class SN_Shippinginfo_Block_Adminhtml_Shippinginfo_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
      {      
          public function __construct()
          {
              parent::__construct();
              $this->setId('shippinginfo_tabs');
              $this->setDestElementId('edit_form');
              $this->setTitle(Mage::helper('shippinginfo')->__('Shippinginfo Information'));
          }   
          protected function _beforeToHtml()
          {
              $this->addTab('form_section', array(
                  'label'     => Mage::helper('shippinginfo')->__('Item Information'),
                  'title'     => Mage::helper('shippinginfo')->__('Item Information'),
                  'content'   => $this->getLayout()->createBlock('shippinginfo/adminhtml_shippinginfo_edit_tab_form')->toHtml(),
              ));
              return parent::_beforeToHtml();
          }
      }