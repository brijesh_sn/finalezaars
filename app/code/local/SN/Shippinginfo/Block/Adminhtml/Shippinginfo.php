<?php
class SN_Shippinginfo_Block_Adminhtml_Shippinginfo extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
  	
    $this->_controller = 'adminhtml_Shippinginfo';
    $this->_blockGroup = 'shippinginfo';
    $this->_headerText = Mage::helper('shippinginfo')->__('Shippinginfo Manager');
    $this->_addButtonLabel = Mage::helper('shippinginfo')->__('Add Shippinginfo');
    parent::__construct();
  }
}