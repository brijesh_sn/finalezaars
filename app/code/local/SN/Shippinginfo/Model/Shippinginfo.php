<?php
class SN_Shippinginfo_Model_Shippinginfo extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('shippinginfo/shippinginfo'); // this is location of the resource file.
    }
}
