<?php
	  class SN_Shippinginfo_Adminhtml_ShippinginfoController extends Mage_Adminhtml_Controller_Action
      {
          protected function _initAction()
          {

              $this->loadLayout()
                  ->_setActiveMenu('shippinginfo/items')
                  ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
              return $this;
          }   
          public function indexAction() {

              $this->_initAction();       
              $this->_addContent($this->getLayout()->createBlock('shippinginfo/adminhtml_shippinginfo'));
              $this->renderLayout();
          }
          public function editAction()
          {
              $shippinginfoId     = $this->getRequest()->getParam('id');
              $shippinginfoModel  = Mage::getModel('shippinginfo/shippinginfo')->load($shippinginfoId);
              if ($shippinginfoModel->getId() || $shippinginfoId == 0) {
                  Mage::register('shippinginfo_data', $shippinginfoModel);
                  $this->loadLayout();
                  $this->_setActiveMenu('shippinginfo/items');
                  $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
                  $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
                  $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
                  $this->_addContent($this->getLayout()->createBlock('shippinginfo/adminhtml_shippinginfo_edit'))
                       ->_addLeft($this->getLayout()->createBlock('shippinginfo/adminhtml_shippinginfo_edit_tabs'));
                  $this->renderLayout();
              } else {
                  Mage::getSingleton('adminhtml/session')->addError(Mage::helper('shippinginfo')->__('Item does not exist'));
                  $this->_redirect('*/*/');
              }
          }
          public function newAction()
          {
              $this->_redirect('*/*/edit');
          }
          public function saveAction()
          {
              if ($data = $this->getRequest()->getPost()) {
			
			
			
			$model = Mage::getModel('shippinginfo/shippinginfo');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				
				$model->save();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('shippinginfo')->__('Shippinginfo was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('shippinginfo')->__('Unable to find Shippinginfo to save'));
        $this->_redirect('*/*/');
          }
          public function deleteAction()
          {
              if( $this->getRequest()->getParam('id') > 0 ) {
                  try {
                      $shippinginfoModel = Mage::getModel('shippinginfo/shippinginfo');
                      $shippinginfoModel->setId($this->getRequest()->getParam('id'))
                          ->delete();
                      Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                      $this->_redirect('*/*/');
                  } catch (Exception $e) {
                      Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                      $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                  }
              }
              $this->_redirect('*/*/');
          }
          /**

           * Product grid for AJAX request.

           * Sort and filter result for example.

           */
          public function gridAction()
          {
              $this->loadLayout();
              $this->getResponse()->setBody(
                     $this->getLayout()->createBlock('importedit/adminhtml_shippinginfo_grid')->toHtml()
              );
          }
		  public function massStatusAction()
		  {
			  $ids = $this->getRequest()->getParam('shippinginfo');
			  foreach($ids as $id)
			  {
				  $model = Mage::getModel('shippinginfo/shippinginfo');
				  $model->setId($id)
				  		->setStatus($this->getRequest()->getParam('status'))
						->save();
			  }
			  $this->_redirect('*/*/');
			  
		  }
		  public function massDeleteAction()
		  {
			  $ids = $this->getRequest()->getParam('shippinginfo');
			  foreach($ids as $id)
			  {
				  $model = Mage::getModel('shippinginfo/shippinginfo');
				  $model->setId($id)
						->delete();
			  }
			  Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
			  $this->_redirect('*/*/');
		  }
      }	