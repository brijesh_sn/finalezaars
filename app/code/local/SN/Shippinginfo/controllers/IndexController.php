<?php 
class SN_Shippinginfo_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$this->loadLayout();  
        $this->renderLayout();
	}
	public function infoAction()
	{
		if($this->getRequest()->getPost())
		{
			$this->getRequest()->getPost('vid');
			
			$array= Mage::getModel('shippinginfo/shippinginfo')->getCollection()
				->addFieldToFilter('status', array('eq' => '1'))
				->addFieldToFilter('state', array('like' => $this->getRequest()->getPost('vid')))
				->getData();
			$array = $array[0];

			echo json_encode(array('status' => 'true', 'message' => $array['content'],'state' => $array['state']));
		}
		else
		{
				echo json_encode(array('status' => 'false', 'message' => 'something went wrong please try again later!!!'));
		}
	}
} 