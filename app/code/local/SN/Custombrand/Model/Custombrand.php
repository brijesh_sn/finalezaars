<?php
class SN_Custombrand_Model_Custombrand extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('custombrand/custombrand'); // this is location of the resource file.
    }
}
