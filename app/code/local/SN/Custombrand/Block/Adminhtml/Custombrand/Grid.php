<?php
 
class SN_Custombrand_Block_Adminhtml_Custombrand_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('custombrandGrid');
        $this->setDefaultSort('custombrand_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('custombrand/custombrand')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('custombrand_id', array(
          'header'    => Mage::helper('custombrand')->__('ID'),
          'align'     =>'right',
          'width'     => '10px',
          'index'     => 'custombrand_id',
        ));
		
		    $this->addColumn('brand_name', array(
          'header'    => Mage::helper('custombrand')->__('Brand Name'),
          'align'     =>'left',
          'index'     => 'brand_name',
          'width'     => '70px',
        ));
 
        $this->addColumn('supplier_id', array(
            'header'    => Mage::helper('custombrand')->__('supplier id'),
            'width'     => '350px',
            'index'     => 'supplier_id',
        ));

        $this->addColumn('status', array( 
            'header'    => Mage::helper('custombrand')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
           'options'   => array(
                2 => 'Rejected',
                1 => 'Accepted',
                0 => 'Pending',
            ),
        ));
        return parent::_prepareColumns();
    }
	public function getRowUrl($row)
    {
       // return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
	
	protected function _prepareMassaction()
    {
        $this->setMassactionIdField('custombrand_id');
        $this->getMassactionBlock()->setFormFieldName('custombrand');
 
        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('custombrand')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('custombrand')->__('Are you sure?')
        ));
 
        //$statuses = Mage::getSingleton('custombrand/custombrand')->getOptionArray();
 
        //array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('custombrand')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('custombrand')->__('Status'),
                         'values' => array(
                      array(
                          'value'     => 2,
                          'label'     => Mage::helper('custombrand')->__('Rejected'),
                      ),
                      array(
                          'value'     => 1,
                          'label'     => Mage::helper('custombrand')->__('Accepted'),
                      ),
                      array(
                          'value'     => 0,
                          'label'     => Mage::helper('custombrand')->__('Pending'),
                      ),
					  ),
        )
        )
        ));
        return $this;
    }
}