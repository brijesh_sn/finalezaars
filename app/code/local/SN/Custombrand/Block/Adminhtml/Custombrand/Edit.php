<?php
      class SN_Custombrand_Block_Adminhtml_Custombrand_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
      {
          public function __construct()
          {
              parent::__construct();                     
              $this->_objectId = 'id';
              $this->_blockGroup = 'custombrand';
              $this->_controller = 'adminhtml_custombrand';           
              $this->_updateButton('save', 'label', Mage::helper('custombrand')->__('Save Item')); 
              $this->_updateButton('delete', 'label', Mage::helper('custombrand')->__('Delete Item'));  
			  
			  $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        		), -100);
				
				 $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('custombrands_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'custombrands_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'custombrands_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        	";
          }           
          public function getHeaderText()  
          {  
              if( Mage::registry('custombrand_data') && Mage::registry('custombrand_data')->getId() ) {  
                  return Mage::helper('custombrand')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('custombrand_data')->getTitle()));  
              } else {  
                  return Mage::helper('custombrand')->__('Add Item');  
              }  
          }  
      }