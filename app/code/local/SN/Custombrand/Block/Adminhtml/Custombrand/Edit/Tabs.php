<?php
      class SN_Custombrand_Block_Adminhtml_Custombrand_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
      {      
          public function __construct()
          {
              parent::__construct();
              $this->setId('custombrand_tabs');
              $this->setDestElementId('edit_form');
              $this->setTitle(Mage::helper('custombrand')->__('Custombrand Information'));
          }   
          protected function _beforeToHtml()
          {
              $this->addTab('form_section', array(
                  'label'     => Mage::helper('custombrand')->__('Item Information'),
                  'title'     => Mage::helper('custombrand')->__('Item Information'),
                  'content'   => $this->getLayout()->createBlock('custombrand/adminhtml_custombrand_edit_tab_form')->toHtml(),
              ));
              return parent::_beforeToHtml();
          }
      }