<?php
      class SN_Custombrand_Block_Adminhtml_Custombrand_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
      {
          protected function _prepareForm()
          {
              $form = new Varien_Data_Form();
              $this->setForm($form);
              $fieldset = $form->addFieldset('custombrand_form', array('legend'=>Mage::helper('custombrand')->__('Item information')));
			  
			     $object = Mage::getModel('custombrand/custombrand')->load( $this->getRequest()->getParam('id') );
	  		   
			  
              $fieldset->addField('brand_name', 'text', array(
                  'label'     => Mage::helper('custombrand')->__('Title'),
                  'class'     => 'required-entry',
                  'required'  => true,
                  'name'      => 'brand_name',
              ));
			  
			  $fieldset->addField('custombrandimage', 'file', array(
          			'label'     => Mage::helper('custombrand')->__('Custombrand Image'),
          			'required'  => false,
          			'name'      => 'custombrandimage',
	  			));
	  
	  		if( $object->getId() ){
		  		$tempArray = array(
				  'name'      => 'filethumbnail',
				  'style'     => 'display:none;',
			  	);
		  	$fieldset->addField($imgPath, 'thumbnail',$tempArray);
	  		}
	  
	  		$fieldset->addField('link', 'text', array(
          		'label'     => Mage::helper('custombrand')->__('Link'),
          		'required'  => false,
          		'name'      => 'link',
	  		));
	  
	 		$fieldset->addField('target', 'select', array(
          		'label'     => Mage::helper('custombrand')->__('Target'),
          		'name'      => 'target',
          		'values'    => array(
              		array(
                  		'value'     => '_blank',
                  		'label'     => Mage::helper('custombrand')->__('Open in new window'),
              		),

              		array(
                  		'value'     => '_self',
                  		'label'     => Mage::helper('custombrand')->__('Open in same window'),
              		),
          		),
      		));
	 
	  		$fieldset->addField('sort_order', 'text', array(
          		'label'     => Mage::helper('custombrand')->__('Sort Order'),
          		'required'  => false,
          		'name'      => 'sort_order',
      		));
			
			
			$fieldset->addField('store_id','multiselect',array(
			'name'      => 'stores[]',
            'label'     => Mage::helper('custombrand')->__('Store View'),
            'title'     => Mage::helper('custombrand')->__('Store View'),
            'required'  => true,
			'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true)
			));

              $fieldset->addField('status', 'select', array(
                  'label'     => Mage::helper('custombrand')->__('Status'),
                  'name'      => 'status',
                  'values'    => array(
                      array(
                          'value'     => 1,
                          'label'     => Mage::helper('custombrand')->__('Active'),
                      ),
                      array(
                          'value'     => 0,
                          'label'     => Mage::helper('custombrand')->__('Inactive'),
                      ),
                  ),
              ));
              $fieldset->addField('content', 'editor', array(
                  'name'      => 'content',
                  'label'     => Mage::helper('custombrand')->__('Content'),
                  'title'     => Mage::helper('custombrand')->__('Content'),
                  'style'     => 'width:98%; height:400px;',
                  'wysiwyg'   => false,
                  'required'  => true,
              ));
              if ( Mage::getSingleton('adminhtml/session')->getCustombrandData() )
              {
                  $form->setValues(Mage::getSingleton('adminhtml/session')->getCustombrandData());
                  Mage::getSingleton('adminhtml/session')->setCustombrandData(null);
              } elseif ( Mage::registry('custombrand_data') ) {
                  $form->setValues(Mage::registry('custombrand_data')->getData());
              }
              return parent::_prepareForm();
          }
      }