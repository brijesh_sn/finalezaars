<?php
class SN_Custombrand_Block_Adminhtml_Custombrand extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_Custombrand';
    $this->_blockGroup = 'custombrand';
    $this->_headerText = Mage::helper('custombrand')->__('Custombrand Manager');
    //$this->_addButtonLabel = Mage::helper('custombrand')->__('Add Custombrand');
    parent::__construct();
    $this->_removeButton('add');
  }
}