<?php
	  class SN_Custombrand_Adminhtml_CustombrandController extends Mage_Adminhtml_Controller_Action
      {
          protected function _initAction()
          {
              $this->loadLayout()
                  ->_setActiveMenu('custombrand/items')
                  ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
              return $this;
          }   
          public function indexAction() {
              $this->_initAction();       
              $this->_addContent($this->getLayout()->createBlock('custombrand/adminhtml_custombrand'));
              $this->renderLayout();
          }
          public function editAction()
          {
              $custombrandId     = $this->getRequest()->getParam('id');
              $custombrandModel  = Mage::getModel('custombrand/custombrand')->load($custombrandId);
              if ($custombrandModel->getId() || $custombrandId == 0) {
                  Mage::register('custombrand_data', $custombrandModel);
                  $this->loadLayout();
                  $this->_setActiveMenu('custombrand/items');
                  $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
                  $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
                  $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
                  $this->_addContent($this->getLayout()->createBlock('custombrand/adminhtml_custombrand_edit'))
                       ->_addLeft($this->getLayout()->createBlock('custombrand/adminhtml_custombrand_edit_tabs'));
                  $this->renderLayout();
              } else {
                  Mage::getSingleton('adminhtml/session')->addError(Mage::helper('custombrand')->__('Item does not exist'));
                  $this->_redirect('*/*/');
              }
          }
          public function newAction()
          {
              $this->_redirect('*/*/edit');
          }
          public function saveAction()
          {
              if ($data = $this->getRequest()->getPost()) {
			
			
			if(isset($_FILES['custombrandimage']['name']) && $_FILES['custombrandimage']['name'] != '') {
	  			 //this way the name is saved in DB
	  			$data['custombrandimage'] = $_FILES['custombrandimage']['name'];
				
				//Save Image Tag in DB for GRID View
				$imgName = $_FILES['custombrandimage']['name'];
				$imgPath = Mage::getBaseUrl('media')."Custombrand/images/thumb/".$imgName;
				$data['filethumbgrid'] = '<img src="'.$imgPath.'" border="0" width="75" height="75" />';
			}
			
			$model = Mage::getModel('custombrand/custombrand');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();
				
				
				if(isset($_FILES['custombrandimage']['name']) && $_FILES['custombrandimage']['name'] != '') {
					try {	
						
						$path = Mage::getBaseDir('media')."/Custombrand". DS ."images". DS ;
						/* Starting upload */							
						$uploader = new Varien_File_Uploader('custombrandimage');
						// Any extention would work
						$uploader->setAllowedExtensions(array('jpg','JPG','jpeg','gif','GIF','png','PNG'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						// We set media as the upload dir
						$uploader->save($path, $_FILES['custombrandimage']['name'] );
						
						
						//Create Thumbnail and upload
						$imgName = $_FILES['custombrandimage']['name'];
						$imgPathFull = $path.$imgName;
						$resizeFolder = "thumb";
						$imageResizedPath = $path.$resizeFolder.DS.$imgName;
						$imageObj = new Varien_Image($imgPathFull);
						$imageObj->constrainOnly(TRUE);
						$imageObj->keepAspectRatio(TRUE);
						$imageObj->resize(150,150);
						$imageObj->save($imageResizedPath);
						
						//Create View Size and upload
						$imgName = $_FILES['custombrandimage']['name'];
						$imgPathFull = $path.$imgName;
						$resizeFolder = "medium";
						$imageResizedPath = $path.$resizeFolder.DS.$imgName;
						$imageObj = new Varien_Image($imgPathFull);
						$imageObj->constrainOnly(TRUE);
						$imageObj->keepAspectRatio(TRUE);
						$imageObj->resize(400,400);
						$imageObj->save($imageResizedPath);
						
						
					} catch (Exception $e) {}
				}
				
				//Mage::helper('custombrand')->generateXML();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('custombrand')->__('Custombrand was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('custombrand')->__('Unable to find Custombrand to save'));
        $this->_redirect('*/*/');
          }
          public function deleteAction()
          {
              if( $this->getRequest()->getParam('id') > 0 ) {
                  try {
                      $custombrandModel = Mage::getModel('custombrand/custombrand');
                      $custombrandModel->setId($this->getRequest()->getParam('id'))
                          ->delete();
                      Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                      $this->_redirect('*/*/');
                  } catch (Exception $e) {
                      Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                      $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                  }
              }
              $this->_redirect('*/*/');
          }
          /**

           * Product grid for AJAX request.

           * Sort and filter result for example.

           */
          public function gridAction()
          {
              $this->loadLayout();
              $this->getResponse()->setBody(
                     $this->getLayout()->createBlock('importedit/adminhtml_custombrand_grid')->toHtml()
              );
          }
		  public function massStatusAction()
		  {
			  $ids = $this->getRequest()->getParam('custombrand');
			  foreach($ids as $id)
			  {
				  $model = Mage::getModel('custombrand/custombrand');
				  $model->setId($id)
				  		->setStatus($this->getRequest()->getParam('status'))
						->save();
			  }
			  $this->_redirect('*/*/');
			  
		  }
		  public function massDeleteAction()
		  {
			  $ids = $this->getRequest()->getParam('custombrand');
			  foreach($ids as $id)
			  {
				  $model = Mage::getModel('custombrand/custombrand');
				  $model->setId($id)
						->delete();
			  }
			  Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
			  $this->_redirect('*/*/');
		  }
      }	