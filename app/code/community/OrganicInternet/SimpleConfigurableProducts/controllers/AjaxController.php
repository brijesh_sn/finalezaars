<?php
require_once 'Mage/Catalog/controllers/ProductController.php';

class OrganicInternet_SimpleConfigurableProducts_AjaxController extends Mage_Catalog_ProductController
{
    public function coAction()
    {
       $product = $this->_initProduct();
       if (!empty($product)) {
           $this->loadLayout(false);
           $this->renderLayout();
       }
    }

    public function imageAction()
    {
       $product = $this->_initProduct();
       if (!empty($product)) {
           $this->loadLayout(false);
           $this->renderLayout();
       }
    }

    public function galleryAction()
    {
       $product = $this->_initProduct();
       if (!empty($product)) {
           #$this->_initProductLayout($product);
           $this->loadLayout();
           $this->renderLayout();
       }
    }

    //Copy of parent _initProduct but changes visibility checks.
    //Reproducing functionality like this is far from great for future compatibilty
    //but at the moment I don't see a better alternative.
    protected function _initProduct()
    {
        
        $categoryId = (int) $this->getRequest()->getParam('category', false);
        $productId  = (int) $this->getRequest()->getParam('id');
        $parentId   = (int) $this->getRequest()->getParam('pid');

        if (!$productId || !$parentId) {
            return false;
        }

        $parent = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($parentId);

        if (!Mage::helper('catalog/product')->canShow($parent)) {
            return false;
        }

        $childIds = $parent->getTypeInstance()->getUsedProductIds();
        if (!is_array($childIds) || !in_array($productId, $childIds)) {
            return false;
        }

        $product = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($productId);
        // @var $product Mage_Catalog_Model_Product
        if (!$product->getId()) {
            return false;
        }
        if ($categoryId) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            Mage::register('current_category', $category);
        }
        $product->setCpid($parentId);
        Mage::register('current_product', $product);
        Mage::register('product', $product);
        return $product;
    }

     public function customAction()
    {

      $parentId   = (int) $this->getRequest()->getParam('pid');
      $qty = (int) $this->getRequest()->getParam('qty');
      $product=Mage::getModel('catalog/product')->load($parentId);
	  
	//$_coreHelper = $this->helper('core');
	
	$_producte = Mage::getModel('catalog/product')->load($parentId);
	$current = Mage::app()->getStore()->getCurrentCurrencyCode(); 
	$baseCurrencyrate =  Mage::app()->getStore()->getCurrentCurrencyRate();			
      
     // $custom = Mage::getModel('catalog/product')->loadByAttribute('sku',$product->sku);
      
      //print_r($custom->getData());die;
    if(!empty($product->getTierPrice()))
    { 
         $tierprice = array();
         $tierqty = array();
       
       foreach ($product->getTierPrice() as $key => $value) {
          $tierprice[$key]=$value['price'];
          $tierqty[$key] = intval($value['price_qty']);

       }
       
       $maintier=$tierqty;
          rsort($tierqty);
          
          foreach ($tierqty as $a) {
              
              if ($a <= $qty)
              {
                
                
                $key1=array_search($a, $maintier);
                $price=$tierprice[$key1];
                break;
              }
              else{
                $key1="";
                //$price=$product->getPrice();
                $price=$product->getFinalPrice();
                

            }
        
          }
          
        $total = $price*$qty;
        $formattedPrice = Mage::helper('core')->currency($price, true, false);

	if($current == 'AED'){  		
		 $orignalprice =   $total  *  $baseCurrencyrate;
		$_price = round($orignalprice,3);
		$totalprice =  $current . number_format(round($_price,2),3);
		//$totalprice =  $_coreHelper->formatPrice($a, true);
	} else {
		 $orignalprice = ($price*$qty);
		 $totalprice= $current . number_format(round($orignalprice,2),3);
	}		

	  // $totalprices = Mage::helper('core')->currency($totalprice, true, false);
        echo json_encode(array('tierprice'=>$formattedPrice,'totalprice'=>$totalprice));
       

    }
    else{  

		$tierprice = array();
		$tierqty = array();

            $total= $product->getFinalPrice()*$qty;
	
	if($current == 'AED'){  		
		 $orignalprice =   $total  *  $baseCurrencyrate;
		$_price = round($orignalprice,3);
		$totalprice = $current .number_format(round($_price,2),3);
		//$totalprice =  $_coreHelper->formatPrice($a, true);
	} else {
		 $orignalprice = $total;
		 $totalprice= $current .number_format(round($orignalprice,2),3);
	}
            //$totalprice= Mage::helper('core')->currency($totalprice, true, false);
            echo json_encode(array('totalprice'=>$totalprice));
      }
    
    }
  
}
