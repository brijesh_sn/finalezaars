<?php
$installer = $this;
$installer->startSetup();

$entity = $this->getEntityTypeId('customer');
$this->addAttribute($entity, 'supplier_banner', array(
    'type' => 'text',
    'label' => 'Supplier Banner file',
    'input' => 'text',
    'visible' => TRUE,
    'required' => FALSE,
    'default_value' => '',
    'adminhtml_only' => '1'
));
$this->addAttribute($entity, 'supplier_url', array(
    'type' => 'text',
    'label' => 'Supplier Store Url',
    'input' => 'text',
    'visible' => TRUE,
    'required' => TRUE,
    'default_value' => '',
    'adminhtml_only' => '1'
));
Mage::helper('marketplace')->setSupplierDataInstalled(true);

$installer->endSetup();