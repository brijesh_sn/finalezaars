<?php
$installer = $this;
$installer->startSetup();

$entity = $this->getEntityTypeId('customer');
$this->addAttribute($entity, 'supplier_url_new', array(
    'type' => 'text',
    'label' => 'Supplier Store Url',
    'input' => 'text',
    'visible' => TRUE,
    'required' => TRUE,
    'default_value' => '',
    'adminhtml_only' => '1'
));
Mage::helper('marketplace')->setSupplierDataInstalled(true);

$installer->endSetup();