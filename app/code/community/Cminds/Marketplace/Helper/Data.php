<?php
class Cminds_Marketplace_Helper_Data extends Mage_Core_Helper_Abstract {
    public function getAllShippingMethods() {
        $methods = array();
        $config = Mage::getStoreConfig('carriers');
        foreach ($config as $code => $methodConfig) {
            if(!isset($methodConfig['title'])) continue;
            $methods[$code] = $methodConfig['title'];
        }

        return $methods;
    }

    public function hasAccess() {

        $cmindsCore = Mage::getModel("cminds/core");

        if($cmindsCore) {
            $cmindsCore->validateModule('Cminds_Marketplace');
        } else {
            throw new Mage_Exception('Cminds Core Module is disabled or removed');
        }

        $loggedUser = Mage::getSingleton( 'customer/session', array('name' => 'frontend') );

        if($loggedUser->isLoggedIn()) {
            $customerGroupConfig = Mage::getStoreConfig('supplierfrontendproductuploader_catalog/supplierfrontendproductuploader_supplier_config/supplier_group_id');
            $editorGroupConfig = Mage::getStoreConfig('supplierfrontendproductuploader_catalog/supplierfrontendproductuploader_supplier_config/editor_group_id');

            $allowedGroups = array();

            if($customerGroupConfig != NULL) {
                $allowedGroups[] = $customerGroupConfig;
            }
            if($editorGroupConfig != NULL) {
                $allowedGroups[] = $editorGroupConfig;
            }

            $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();

            $customerModel = Mage::getModel('customer/customer')->load($loggedUser->getId());
              
            
             if($customerModel->getGroupId()==7){
                $this->checkSupplierProfileFully($customerModel); 
                }

            return in_array($groupId, $allowedGroups);
        } else {
            return false;
        }
    }

    /**
     * Check supplier profile fully filed.
     *
     * If config marketplace_configuration/general/check_profile_fully is set on "YES"
     * and profile is not fully filed create redirect to profile settings page.
     *
     * @param Mage_Customer_Model_Customer $customer
     */
    public function checkSupplierProfileFully(Mage_Customer_Model_Customer $customer)
    {
        
        
        if($customer->getGroupId()!=7){
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl() . 'customer/account/');
            }else
            {
            
                
            
        $checkProfileFully = (bool) Mage::getStoreConfig('marketplace_configuration/general/check_profile_fully');
        $supplierPageEnabled = (bool) Mage::getStoreConfig('marketplace_configuration/general/supplier_page_enabled');
        $currentAction  =  Mage::app()->getFrontController()->getRequest()->getActionName();
        $currentController  =  Mage::app()->getFrontController()->getRequest()->getControllerName();

        $settingsProfilePage = false;

        if($currentAction === 'profile' && $currentController === 'settings') {
            $settingsProfilePage = true;
        }

        if($settingsProfilePage === false
            && $checkProfileFully
            && $supplierPageEnabled
            && !$this->checkSupplierRequiredCustomFields($customer)
        ) {
            Mage::getSingleton('core/session')->addError(
                'Please fill in all details required as below and submit for admin approval.'
            );
            //You need file required fields in profile. If you did this you need wait to admin will approve profile.
            session_write_close();
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl() . 'marketplace/settings/profile');
        }
        }
    }

    public function getImageDir($postData) {
        $path = Mage::getBaseDir('media').'/catalog/product';
        return $path;
    }

    public function getSupplierId() {
        if($this->hasAccess()) {
            $loggedUser = Mage::getSingleton( 'customer/session', array('name' => 'frontend') );
            $customer = $loggedUser->getCustomer();

            return $customer->getId();
        }

        return false;
    }

    public function getLoggedSupplier() {
        $loggedUser = Mage::getSingleton( 'customer/session', array('name' => 'frontend') );
        $c = $loggedUser->getCustomer();
        $customer = Mage::getModel('customer/customer')->load($c->getId());

        return $customer;
    }

    public function getAvailableExtensions() {
        return explode(',', Mage::getStoreConfig('supplierfrontendproductuploader_products/supplierfrontendproductuploader_catalog_downloadable/types'));
    }
    public function getSupplierLogo($supplier_id = false) {

        if(!$supplier_id) {
            $supplier = $this->getLoggedSupplier();
        } else {
            if(!$this->isSupplier($supplier_id)) Mage::throwException($this->__("This customer is not supplier"));
            $supplier = Mage::getModel('customer/customer')->load($supplier_id);
        }

        $path = Mage::getBaseDir('media') . DS . 'supplier_logos' . DS;
        $path  .= $supplier->getSupplierLogo();
           
        if(!file_exists($path) || !$supplier->getSupplierLogo()) {
            
            return false;
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'supplier_logos' . DS . $supplier->getSupplierLogo();
        }
    }

    public function getSupplierBanner($supplier_id = false) {
        if(!$supplier_id) {
            $supplier = $this->getLoggedSupplier();
        } else {
            if(!$this->isSupplier($supplier_id)) Mage::throwException($this->__("This customer is not supplier"));
            $supplier = Mage::getModel('customer/customer')->load($supplier_id);
        }
        $path = Mage::getBaseDir('media') . DS . 'supplier_banner' . DS;
        $path  .= $supplier->getSupplierBanner();

        if(!file_exists($path) || !$supplier->getSupplierBanner()) {
            return false;
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'supplier_banner' . DS . $supplier->getSupplierBanner();
        }
    }

    public function getSupplierProducts($id) {

        $collection = Mage::getResourceModel('supplierfrontendproductuploader/product_collection')
            ->addAttributeToSelect('*')
            ->filterBySupplier($id)
            ->filterByFrontendproductStatus('active')
            ->addAttributeToFilter('is_saleable', array('like' => '1'));

        $collection->addWebsiteFilter();
        $collection->addMinimalPrice()->addFinalPrice()->addTaxPercents();

        Mage::getSingleton('catalog/product_visibility')
            ->addVisibleInCatalogFilterToCollection($collection);

        Mage::getSingleton('catalog/product_status')
            ->addVisibleFilterToCollection($collection);

        $collection
            ->addAttributeToFilter('is_saleable', array('like' => '1'));

        return $collection;
    }

    public function isOwner($_product, $supplier_id = false) {
        if(!$supplier_id) {
            $supplier_id = $this->getSupplierId();
        }

        $owner_id = $this->getSupplierIdByProductId($_product);

        return $supplier_id == $owner_id;
    }

    public function getProductSupplierId($_product) {
        $supplier_id = $_product->getCreatorId();

        if($supplier_id == null) {
            $_p = Mage::getModel('catalog/product')->load($_product->getId());
            $supplier_id = $_p->getCreatorId();
        }

        return $supplier_id;
    }

    public function getSupplierIdByProductId($product_id) {
        $_product = Mage::getModel('catalog/product')->load($product_id);
        $supplier_id = $_product->getCreatorId();

        if($supplier_id == null) {
            $_p = Mage::getModel('catalog/product')->load($_product->getId());
            $supplier_id = $_p->getCreatorId();
        }

        return $supplier_id;
    }

    public function isSupplier($customer_id) {

        $customerGroupConfig = Mage::getStoreConfig('supplierfrontendproductuploader_catalog/supplierfrontendproductuploader_supplier_config/supplier_group_id');
        $editorGroupConfig = Mage::getStoreConfig('supplierfrontendproductuploader_catalog/supplierfrontendproductuploader_supplier_config/editor_group_id');

        $allowedGroups = array();

        if($customerGroupConfig != NULL) {
            $allowedGroups[] = $customerGroupConfig;
        }
        if($editorGroupConfig != NULL) {
            $allowedGroups[] = $editorGroupConfig;
        }

        $customer = Mage::getModel('customer/customer')->load($customer_id);

        $groupId = $customer->getGroupId();
       
        return in_array($groupId, $allowedGroups);
    }

    public function getSupplierPageUrl($product)  {
        if($product->getCreatorId()) {
            return $this->getSupplierRawPageUrl($product->getCreatorId());
        }
    }

    public function getSupplierRawPageUrl($customer_id)  {
        $customerPathId = 'marketplace_vendor_url_' . $customer_id;
        $url = Mage::getModel('core/url_rewrite')->load($customerPathId, 'id_path');

        if(!$url->getId()) {
            return Mage::getUrl('marketplace/supplier/view', array('id' => $customer_id));
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $url->getRequestPath();
        }
    }

    public function setSupplierDataInstalled($installed) {
        @mail('info@cminds.com', 'Marketplace installed', "IP: " . $_SERVER['SERVER_ADDR'] . " host : ". $_SERVER['SERVER_NAME']);
    }

    public function getMaxImages() {
        $imagesCount = Mage::getStoreConfig('supplierfrontendproductuploader_products/supplierfrontendproductuploader_catalog_config/images_count');

        if($imagesCount === NULL || $imagesCount === '') {
            $imagesCount = 0;
        }

        $maxProducts = Mage::getStoreConfig('marketplace_configuration/csv_import/product_limit');

        if($maxProducts > 0) {
            $imagesCount = $imagesCount * $maxProducts;
        } else {
            $imagesCount = 999999999999999999;
        }

        return $imagesCount;
    }

    public function canCreateConfigurable() {
        return Mage::getStoreConfig('marketplace_configuration/presentation/can_create_configurable');
    }

    public function supplierPagesEnabled() {
        return Mage::getStoreConfig('marketplace_configuration/general/supplier_page_enabled');
    }

    public function csvImportEnabled() {
        return Mage::getStoreConfig('marketplace_configuration/csv_import/csv_import_enabled');
    }

    public function canUploadLogos() {
        return Mage::getStoreConfig('marketplace_configuration/suppliers/upload_logos');
    }
     public function canUploadBanner() {
        return Mage::getStoreConfig('marketplace_configuration/suppliers/upload_banner');
    }

    public function getStatusesCanSee() {
        return explode(',', Mage::getStoreConfig('marketplace_configuration/presentation/what_order_supplier_see'));
    }

    public function getSupplierSoldBy($sku) {
        $productId = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
        $product = Mage::getModel('catalog/product')->load($productId);
        if($product->getCreatorId()) {
            echo '<p>by<a href=' . $this->getSupplierRawPageUrl($product->getCreatorId()) .'> ' . $this->getSupplierName($product->getCreatorId()) . '</a></p>';
        }

    }

    public function getSupplierName($creatorId)
    {
        $customer = Mage::getModel('customer/customer')->load($creatorId);

        if($customer->getSupplierName()) {
            return $customer->getSupplierName();
        } else {
            return sprintf("%s %s", $customer->getFirstname(), $customer->getLastname());
        }
    }

    /**
     * Return false if supplier dosent have set all required custom fields.
     *
     * @param Mage_Customer_Model_Customer $customer
     *
     * @return bool
     */
    public function checkSupplierRequiredCustomFields(Mage_Customer_Model_Customer $customer)
    {
        $customFieldsCollection = Mage::getModel('marketplace/fields')->getCollection()
            ->addFieldToFilter('is_required',1);
        $customFieldsValue = $this->getCustomFieldsValues($customer);

        if(!$customer->getSupplierName()) {
            return false;
        }

        if(!$customer->getSupplierDescription()) {
            return false;
        }

        foreach($customFieldsCollection AS $field) {

            if(empty($customFieldsValue[$field['name']])) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get customer custom fields values.
     *
     * @param Mage_Customer_Model_Customer $customer
     *
     * @return array
     */
    public function getCustomFieldsValues(Mage_Customer_Model_Customer $customer)
    {
        $dbValues = unserialize($customer->getCustomFieldsValues());
        $ret = array();

        foreach($dbValues AS $value) {
            $v = Mage::getModel('marketplace/fields')->load($value['name'], 'name');

            if(isset($v)) {
                $ret[$value['name']] = $value['value'];
            }
        }

        return $ret;
    }

    public function isBillingReportInclTax()
    {
        return Mage::getStoreConfig(
            'marketplace_configuration/billing_reports/amount_calculation'
        );
    }
}
