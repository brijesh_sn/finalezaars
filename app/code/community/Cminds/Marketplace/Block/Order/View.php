<?php
class Cminds_Marketplace_Block_Order_View extends Mage_Core_Block_Template {
    public function _construct() {
        $this->setTemplate('marketplace/order/view.phtml');
    }
    public function getOrder() {
        $id = Mage::registry('order_id');
        return Mage::getModel('sales/order')->load($id);
    }
    public function getItems() {
        $id = Mage::registry('order_id');
        $_order = Mage::getModel('sales/order')->load($id);
        $_items = array();

        foreach($_order->getAllItems() AS $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());

            if($product->getCreatorId() == Mage::helper('marketplace')->getSupplierId()) {
                $_items[] = $item;
            }
        }

        return $_items;
    }

    public function getCurrentTab() {
        return Mage::app()->getRequest()->getParam('tab', 'products');
    }

    public function getSupplierItems($orderItems, $modelItems) {
        $otherSupplierItems = array();
        $orderItemsIds = array();
        foreach($orderItems as $orderItem) {
            $orderItemsIds[] = $orderItem->getId();
        }

        $items = Mage::getModel($modelItems)->getCollection()->addFieldToFilter('parent_id', array("in" => $orderItemsIds));

        foreach ($items as $item) {
            $productId = $item->getProductId();
            if(!Mage::helper('marketplace')->isOwner($productId)) {
                $otherSupplierItems[] = $item->getParentId();
            }
        }
        return $otherSupplierItems;
    }

    public function canCreateInvoice($orderItems) {
        $canCreateInvoice = false;
        foreach ($orderItems as $orderItem) {
            if(Mage::helper('marketplace')->isOwner($orderItem->getProductId()) && $orderItem->getQtyInvoiced() == 0) {
                $canCreateInvoice = true;
            }
        }
        return $canCreateInvoice;
    }

    public function canCreateShipment($orderItems) {
        $canCreateShipment = false;
        foreach ($orderItems as $orderItem) {
            if(Mage::helper('marketplace')->isOwner($orderItem->getProductId()) && $orderItem->getQtyShipped() == 0) {
                $canCreateShipment = true;
            }
        }
        return $canCreateShipment;
    }

    public function getSupplierShippingCosts($supplierShippingMethod)
    {
        if(!$supplierShippingMethod) {
            return false;
        }

        $supplierShippingMethod = unserialize($supplierShippingMethod);
        $costs = 0;
        foreach($supplierShippingMethod as $productId => $price) {
            $product = Mage::getModel('catalog/product')->load($productId);
            if($product->getCreatorId() == Mage::helper('marketplace')->getSupplierId()) {
                $costs = $price;
            }
        }
        return $costs;
    }
}