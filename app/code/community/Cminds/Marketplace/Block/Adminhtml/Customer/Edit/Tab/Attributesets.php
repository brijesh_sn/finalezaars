<?php

class Cminds_Marketplace_Block_Adminhtml_Customer_Edit_Tab_Attributesets extends Mage_Adminhtml_Block_Widget_Grid
{
    private $_selectedAttributes;
    private $_selectedAllAttributes;

    public function __construct()
    {
        parent::__construct();
        $this->setId('id');
        $this->setDefaultSort('attribute_set_name');
        $this->setDefaultDir('asc');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('eav/entity_attribute_set')->getCollection()->addFieldToFilter('entity_type_id', 4);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('in_products', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'field_name' => 'attributes_ids[]',
            'values'    => $this->_getSelectedAttributes(),
            'align'     => 'center',
            'index'     => 'attribute_set_id'
        ));
        $this->addColumn('all_attributes', array(
            'type'      => 'checkbox',
            'field_name' => 'all_attributes_ids[]',
            'values'    => $this->_getSelectedAllAttributes(),
            'align'     => 'center',
            'column_css_class' => 'no-display',
            'header_css_class' => 'no-display',
            'index'     => 'attribute_set_id'
        ));

        $this->addColumn('attribute_set_name', array(
            'header'    => Mage::helper('catalog')->__('Attribute sets Name'),
            'index'     => 'attribute_set_name',
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/attributeSets', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return false;
    }

    public function getMultipleRows($item)
    {
        return false;
    }

    private function _getSelectedAttributes() {
        $supplier_id = Mage::app()->getRequest()->getParam('id');

        if(!$this->_selectedAttributes) {
            $attributes = Mage::getModel('marketplace/attributesets')->getCollection()->addFilter('supplier_id', $supplier_id);

            $_selectedAttributes = array();

            foreach($attributes AS $link) {
                $_selectedAttributes[] = $link->getAttributeSetId();
            }

            $allAttributes = $this->_getSelectedAllAttributes();

            foreach($allAttributes AS $attribute_id) {
                if(in_array($attribute_id, $_selectedAttributes)) {
                    $this->_selectedAttributes[] = $attribute_id;
                }
            }
        }

        return $this->_selectedAttributes;
    }

    private function _getSelectedAllAttributes() {
        if(!$this->_selectedAllAttributes) {
            $attributes = Mage::getModel('eav/entity_attribute_set')->getCollection();
            $this->_selectedAllAttributes = array();

            foreach($attributes AS $link) {
                $this->_selectedAllAttributes[] = $link->getAttributeSetId();
            }
        }
        return $this->_selectedAllAttributes;
    }
}
