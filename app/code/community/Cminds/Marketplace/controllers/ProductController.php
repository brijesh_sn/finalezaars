<?php
class Cminds_Marketplace_ProductController extends Cminds_Marketplace_Controller_Action {
    public function preDispatch() {
        parent::preDispatch();
        $hasAccess = $this->_getHelper()->hasAccess();

        if(!$hasAccess) {
            $this->getResponse()->setRedirect($this->_getHelper('supplierfrontendproductuploader')->getSupplierLoginPage());
        }
    }
    public function chooseTypeAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        if($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();

            if($postData['type'] == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/create', array('attribute_set_id' => $postData['attribute_set_id'], 'type' => $postData['type'])));
            } elseif($postData['type'] == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
                $this->getResponse()->setRedirect(Mage::getUrl('marketplace/product/createConfigurable', array('attribute_set_id' => $postData['attribute_set_id'], 'type' => $postData['type'])));
            } elseif($postData['type'] == Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL) {
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/create', array('attribute_set_id' => $postData['attribute_set_id'], 'type' => $postData['type'])));
            } elseif($postData['type'] == Mage_Downloadable_Model_Product_Type::TYPE_DOWNLOADABLE) {
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/create', array('attribute_set_id' => $postData['attribute_set_id'], 'type' => $postData['type'])));
            }elseif($postData['type'] == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/create', array('attribute_set_id' => $postData['attribute_set_id'], 'type' => $postData['type'])));
            }
             else {
                Mage::getSingleton('core/session')->addError($this->__("Unsupported Product Type"));
                $this->getResponse()->setRedirect(Mage::getUrl('marketplace/product/chooseType'));
            }

        }

        $this->_renderBlocks(true);

    }

    public function cloneAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $id = $this->_request->getParam('id', null);

        if($id == null) {
            //throw new Exception('No product id');
            Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
        }

        $p = Mage::getModel('catalog/product')->load($id);

        if($p->getData('creator_id') != $this->_getHelper()->getSupplierId()) {
            //throw new Exception('No product');
             Mage::getSingleton('core/session')->addError($this->__("No product id"));
             $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
        }

        Mage::register('supplier_product_id', $id);


        $this->_renderBlocks(true, false, false, true);

    }

    public function createConfigurableAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $params = $this->getRequest()->getParams();

        if(!isset($params['attribute_set_id'])) {
            $this->getResponse()->setRedirect(Mage::getUrl('marketplace/product/chooseType'));
            Mage::getSingleton('core/session')->addError($this->__("Missing Attribute Set ID"));
            return;
        }

        Mage::register('is_configurable', false);
        Mage::register('cminds_configurable_request', $params);
        $this->_renderBlocks(true, true, false, true);
    }

    public function editConfigurableAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $params = $this->getRequest()->getParams();
        if(isset($params['id'])) {
            $product = Mage::getModel('catalog/product')->load($params['id']);

            if($product->getData('creator_id') != $this->_getHelper()->getSupplierId()) {
               // throw new Exception('No product');
                 Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));

            }

            Mage::register('cminds_configurable_request', $params);

        }
        $this->_renderBlocks(true, true, false, true);
    }

    public function associatedProductsAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $id = $this->getRequest()->getParam('id');
        $product = Mage::getModel('catalog/product')->load($id);

        if(!$product->getId()) {
            throw new Exception($this->__('Super Product Not Found'));
        }

        Mage::register('product_object', $product);
        $this->_renderBlocks(true);
    }
    public function editAction() {
        
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $id = $this->_request->getParam('id', null);

        if($id == null) {
            //throw new Exception('No product id');
             Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
        }

        $p = Mage::getModel('catalog/product')->load($id);

        if($p->getData('creator_id') != $this->_getHelper()->getSupplierId()) {
            //throw new Exception('No product');
             Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
        }

        Mage::register('supplier_product_id', $id);
        if($p->getCreatedUsingCode()) {
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('supplier/codes/edit', array('product_id' => $id)));
            return;
        }

        $this->_renderBlocks(true, false, false, true);
    }

    public function uploadFile() {
        if(isset($_FILES['downloadable_upload']['name']) && ($_FILES['downloadable_upload']['tmp_name'] != NULL))
        {

            try {
                $uploader = new Varien_File_Uploader('downloadable_upload');
                $uploader->setAllowedExtensions($this->_getHelper()->getAvailableExtensions());
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);

                $path = Mage::getBaseDir('media');
                $uploader->save($path , $_FILES['downloadable_upload']['name']);
                $file = $_FILES['downloadable_upload']['name'];
                $fileUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $uploader->getUploadedFileName() ;
                $ret = array('success' => true, 'url' => $fileUrl, 'name' => $uploader->getUploadedFileName());
                return $ret['name'];
            } catch(Exception $e) {
                Mage::throwException($e->getMessage());
            }
        }
    }
    

    public function saveAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
		
		$current = Mage::app()->getStore()->getCurrentCurrencyCode();        
        $baseCurrency = Mage::app()->getStore()->getBaseCurrencyCode();
        $baseCurrencyrate =  Mage::app()->getStore()->getCurrentCurrencyRate();
      
	  // echo '<pre>';
       // print_r($this->_request->getPost());
       // exit;
        if($this->_request->isPost()) {
            $autoApprove = Mage::getStoreConfig('supplierfrontendproductuploader_catalog/general/allow_auto_approval_products');
            $postData = $this->_request->getPost();
            $editMode = false;
          
            try {
                if(isset($postData['product_id']) && $postData['product_id'] != NULL) {
                    $product = Mage::getModel('catalog/product')->load($postData['product_id']);

                    if(!$product->getId()) {
                        throw new Exception('Product does not exists');
                    }

                    if($product->getData('creator_id') != $this->_getHelper()->getSupplierId()) {
                        throw new Exception('Product does not belongs to this supplier');
                    }
                    $editMode = true;
                } else {
                    $product = Mage::getModel('catalog/product');
                    if(!isset($postData['is_cloned']) && $this->_getSupplierHelper()->isProductCodeEnabled() && isset($postData['supplier_product_code']) && $postData['supplier_product_code'] != '') {
                        if(Mage::getModel('supplierfrontendproductuploader/product')->checkIfAlreadyExist($postData['supplier_product_code'])) {
                            throw new Exception('Product with the same Supplier Product Code already exists');
                        }
                    }
                }

                $productValidator = Mage::getModel('supplierfrontendproductuploader/product');
                $productValidator->setData($postData);
                $productValidator->validate();

                $product->setName($postData['name']);
                $product->setShortDescription($postData['short_description']);
                $product->setDescription($postData['description']);

                
                if(number_format($postData['special_price']) != 0 || $postData['special_price']=="") {
                    
                    if($postData['special_price']=="")
                    {
                        $product->setSpecialPrice($postData['special_price']);
                    }
                    else
                    {
                        if($current == 'AED'){                    
                           $orignalprice =  $postData['special_price'] / $baseCurrencyrate;
                          // $product->setSpecialPrice(round($orignalprice,3));
                           $product->setSpecialPrice(number_format(round($orignalprice,2),3));                    
						} else {
                           //$product->setSpecialPrice(number_format($postData['special_price'], 3));
                           $product->setSpecialPrice(number_format(round($postData['special_price'],2),3));
                        }
                    }                    

                    if($postData['special_price_from_date'] != NULL || $postData['special_price_from_date'] == '') {
                        $product->setSpecialFromDate($postData['special_price_from_date']);
                        $product->setSpecialFromDateIsFormated(true);
                    }
                    if($postData['special_price_to_date'] != NULL || $postData['special_price_to_date'] == '') {
                        $product->setSpecialToDate($postData['special_price_to_date']);
                        $product->setSpecialToDateIsFormated(true);
                    }
                }
                
                if(!$editMode) {
                    if(!isset($postData['sku']) || $postData['sku'] == NULL) {
                        $product->setSku($this->_getSupplierHelper()->generateSku());
                    } else {
                        $cProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $postData['sku']);

                        if($cProduct) {
                            throw new Exception('Product with this SKU already exists in catalog');
                        }

                        $product->setSku($postData['sku']);
                    }
                    if(!isset($postData['attribute_set_id']) || empty($postData['attribute_set_id'])) {
                        throw new Exception('Missing Attribute Set ID');
                    }
                    
                    if ($postData['type'] == 'simple'){
                        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
                    } elseif ($postData['type'] == 'configurable') {
                        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE);
                    }elseif ($postData['type'] == 'bundle') {
                        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_BUNDLE);
                    }elseif ($postData['type'] == 'virtual') {
                        
                        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL);
                    } elseif ($postData['type'] == 'downloadable') {
                        $product->setTypeId(Mage_Downloadable_Model_Product_Type::TYPE_DOWNLOADABLE);
                    }
                    $product->setAttributeSetId($postData['attribute_set_id']);
                    $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
                    $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
                    $product->setTaxClassId(Mage::getStoreConfig('supplierfrontendproductuploader_products/supplierfrontendproductuploader_catalog_config/tax_class_id'));
                    $product->setData('admin_product_note', null);
                }

                if(isset($postData['weight'])) {
                    $product->setWeight($postData['weight']);
                }
				
				if(isset($postData['color'])) {
                    $atributeCode = 'color';
                    $_product = Mage::getModel('catalog/product');
                    $attr = $_product->getResource()->getAttribute($atributeCode);
                    //var_dump($attr->usesSource());
                    if ($attr->usesSource()) {
                        $new = $postData['color'];
                        $postData['color'] = $attr->getSource()->getOptionId($new);
                        $product->setColor($postData['color']);
                    }
                   // echo "<pre/>"; print_r($color_id); die;
                    
                }
                //echo $postData['weight_unit'];die;
                 if(isset($postData['weight_unit'])) {
                    
                    $product->setWeightUnit($postData['weight_unit']);
                }
                
                if($current == 'AED'){                    
                    $orignalprice =  $postData['price'] / $baseCurrencyrate;
                    //$product->setPrice(round($orignalprice,3));
                    $product->setPrice(number_format(round($orignalprice,2),3));                     
                } else {
                   // $product->setPrice(number_format($postData['price'],3));
                    $product->setPrice(number_format(round($postData['price'],2),3));
                }
                
                
                if(isset($postData['qty'])) {
                    $product->setStockData(array(
                        'is_in_stock' => ($postData['qty'] > 0) ? 1 : 0,
                        'qty' => $postData['qty']
                    ));
                }

                $product->setCategoryIds($postData['category']);
                $product->setWebsiteIDs(array(Mage::app()->getStore()->getWebsiteId()));
                $product->setCreatedAt(strtotime('now'));
                
                
                if(isset($postData['attributes'])) {
                    foreach($postData['attributes'] as $attrCode){

                        $super_attribute= Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product',$attrCode);
                        $configurableAtt = Mage::getModel('catalog/product_type_configurable_attribute')->setProductAttribute($super_attribute);

                        $newAttributes[] = array(
                            'id'             => $configurableAtt->getId(),
                            'label'          => $configurableAtt->getLabel(),
                            'position'       => $super_attribute->getPosition(),
                            'values'         => $configurableAtt->getPrices() ? $product->getPrices() : array(),
                            'attribute_id'   => $super_attribute->getId(),
                            'attribute_code' => $super_attribute->getAttributeCode(),
                            'frontend_label' => $super_attribute->getFrontend()->getLabel(),
                        );
                    }
                }

                if(isset($newAttributes) && is_array($newAttributes) && count($newAttributes) > 0){
                    $product->setCanSaveConfigurableAttributes(true);
                    $product->setConfigurableAttributesData($newAttributes);
                }

                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                /*************************************************/
                 $ret = $this->uploadFile();
                if($ret != NULL) {
                    $product->setLinksTitle('Test Product');
                    $product->setLinksPurchasedSeparately('0');
                    $downloadData = array();


                    $downloadData['link'][0] = array(
                        'link_id' => '',
                        'title' => $ret,
                        'price' => $product->getPrice(),
                        'number_of_downloads' => $product->getNumberOfDownloads(),
                        'is_shareable' => $product->getLinkId(),
                        'file' => $product->getLinkFile(),
                        'type' => 'file',
                        'link_url' => Mage::getBaseUrl('media') . $ret,
                        'sort_order' => $product->getSortOrder(),
                        'is_delete' => 0
                    );

                    $product->setDownloadableData($downloadData);
                } elseif(isset($postData['file_url']) && $postData['file_url']) {
                    if(!$editMode) {
                        $downloadData = array();

                        $downloadData['link'][0] = array(
                            'link_id' => '',
                            'title' => $postData['file_url'],
                            'price' => $product->getPrice(),
                            'number_of_downloads' => $product->getNumberOfDownloads(),
                            'is_shareable' => $product->getLinkId(),
                            'file' => $product->getLinkFile(),
                            'type' => 'url',
                            'link_url' => $postData['file_url'],
                            'sort_order' => $product->getSortOrder(),
                            'is_delete' => 0
                        );

                        $product->setDownloadableData($downloadData);
                    } else {
                        $linkPurchasedItems = Mage::getModel('downloadable/link')->getCollection()
                            ->addFieldToFilter('product_id', $product->getId())->load();
                        $currentPurchasedItemsT = $linkPurchasedItems->getItems();

                        foreach( $currentPurchasedItemsT as $c){
                            $c->setLinkUrl($postData['file_url']);
                            $c->save();
                        }
                    }
                }
                /**************************************************/
                /************ unset tire price*****************/
                $product->unsTierPrice();
                
                /********************** brand **************************/
                $_category = Mage::getResourceModel('catalog/category_collection')
                 ->addFieldToFilter('name', $postData['brand'])
                ->getFirstItem();
                $categoryId = $_category->getId();
                $categories = $product->getCategoryIds();
                $children = Mage::getModel('catalog/category')->load(42);
                $_categories = $children->getChildrenCategories();
                foreach ($_categories as $category) {
                    if (array_search($category->getId(), $categories))
                      {
                            $key =array_search($category->getId(), $categories);
                            unset($key);
                      }
                }
                $categories[] = $categoryId;
                $product->setCategoryIds($categories);
                /********************** brand **************************/
                $product->save();


                 $product1 = Mage::getModel('catalog/product')->load($product->getId());
                
                /************ video section *****************/
                if(isset($postData['video_gallery'])) {
                    
                    $this->videocheckAction($postData['video_gallery'],$product1->getId());
                }
                /************ video section *****************/

                /************ set tire price*****************/
				$newdata = array();
                if(count($postData['tier_price']) > 0) {
                    foreach ($postData['tier_price'] as $key => $value) {
                      
                        $tireprice = $value['price'] / $baseCurrencyrate;
                        $postData['tier_price'][$key] = array(
                            'website_id' => $value['website_id'],
                            'cust_group' => $value['cust_group'],
                            'price_qty' => $value['price_qty'],
                            'price' => number_format(round($tireprice,2),3),
                            );                      
                    }
					
                    if($current == 'AED'){ 
                        $product1->setTierPrice($postData['tier_price']);                    
                    } else {                       
                       $product1->setTierPrice($postData['tier_price']);
                    }                    
                }
                //$product1->setTierPrice($postData['tier_price']);
                /************ set tire price*****************/
                $product1->save();
            
                unset($postData['name'], $postData['description'], $postData['short_description'], $postData['sku'], $postData['weight'], $postData['price'], $postData['qty'], $postData['category']);

                $product = Mage::getModel('catalog/product')->load($product->getId());

                if(!isset($postData['image'])) {
                    $postData['image'] = array();
                }

                $existingImages = array();

                if($product->getId() && $editMode) {
                    $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
                    $mediaGalleryAttribute = Mage::getModel('catalog/resource_eav_attribute')->loadByCode($product->getEntityTypeId(), 'media_gallery');
                    $gallery = $product->getMediaGalleryImages();

                    foreach ($gallery as $image) {
                        if(!in_array($image->getFile(), $postData['image'])) {
                            $mediaApi->remove($product->getId(), $image->getFile());
                            $mediaGalleryAttribute->getBackend()->removeImage($product, $image->getFile());

                        } else {
                            $existingImages[] = $image->getFile();

                            if($postData['main_photo'] == $image->getFile()) {
                                Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('image'=>$image->getFile()), 0);
                            }
                        }
                    }
                }

                $onlyOneImage = false;

                if(count($postData['image']) == 1) {
                    $onlyOneImage = true;
                }

                foreach($postData['image'] AS $image) {
                    if($image != '' && $image && $image != NULL && !in_array($image, $existingImages)) {
                        $attrs = null;

                        if($image == $postData['main_photo'] || $onlyOneImage) {
                            $attrs = array('image','small_image','thumbnail');
                        }
                        if(isset($postData['is_cloned'])) {

                            $product->addImageToMediaGallery($this->_getSupplierHelper()->getImageDir($postData) . $image, $attrs, false, false);

                        } else {
                            $product->addImageToMediaGallery($this->_getSupplierHelper()->getImageCacheDir($postData) . $image, $attrs, true, false);
                        }
                    }
                }

                $ommitIndex = array('submit', 'main_photo', 'image', 'product_id', 'special_price', 'special_price_to_date', 'special_price_from_date', 'notify_admin_about_change');

                foreach($postData AS $index => $value) {
                    if(!in_array($index, $ommitIndex) && $value != '') {
                        $product->setData($index, $value);
                    }
                }

                if($editMode) {
                    $product->setSmallImage($postData['main_photo']);
                    $product->setImage($postData['main_photo']);
                    $product->setThumbnail($postData['main_photo']);
                } else {
                    $product->setData('frontendproduct_product_status', Cminds_Supplierfrontendproductuploader_Model_Product::STATUS_PENDING);
                    $product->setData('creator_id', $this->_getHelper()->getSupplierId());
                    if($autoApprove) {
                        $p = $product;
                        $p->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
                        $p->setData('frontendproduct_product_status', Cminds_Supplierfrontendproductuploader_Model_Product::STATUS_APPROVED);
                        $p->setStockData(array(
                            'is_in_stock' => 1
                        ));
                    }
                }

                $product->save();

                if(!$editMode) {
                    Mage::log($this->_getHelper()->__('Supplier '. $this->_getHelper()->getSupplierId() .' created product : ' . $product->getId()));
                    $this->_getHelper('supplierfrontendproductuploader/email')->notifyOnSupplierAddNew($product);
                } else {
                    if(isset($postData['notify_admin_about_change']) && $postData['notify_admin_about_change'] == 1) {
                        $this->_getHelper('supplierfrontendproductuploader/email')->notifyAdminOnProductChange($product);
                    }
                }
                if ($postData['type'] == 'configurable') {

               Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/product/associatedProducts/', array('id' => $product->getId())));
                }
                else{
                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
                }
            } catch (Exception $ex) {
                
                Mage::getSingleton('core/session')->addError($ex->getMessage());
                Mage::log($ex->getMessage());
                Mage::getSingleton("supplierfrontendproductuploader/session")->setProductData($postData);

                if($postData['type'] == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
                    $redirectController = 'marketplace';
                    $redirectAction = 'configurable';
                } else {
                    $redirectController = 'supplier';
                    $redirectAction = '';
                }
                if($editMode) {
                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl($redirectController .'/product/edit' . $redirectAction . '/', array('id' =>  $postData['product_id'], 'type' => $postData['type'])));
                } else {
                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl($redirectController . '/product/create' . $redirectAction . '/', array('attribute_set_id' =>  $postData['attribute_set_id'], 'type' => $postData['type'])));
                }
            }
        }
    }
    public function removevideoAction()
    {      
         if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $product = Mage::getModel('catalog/product')->load( $this->getRequest()->getParam('pid'));
        $mediaGalleryData = $product->getData('video_gallery');
        $resourceModel = Mage::getResourceSingleton('videogallery/backend_video');
        if (!isset($mediaGalleryData['videos']) || !is_array($mediaGalleryData['videos'])) {
            return $this;
        }
         $toDelete = array();
        foreach ($mediaGalleryData['videos'] as &$video) {

            if ($video['value_id'] ==  $this->getRequest()->getParam('vid')) {
                $video['removed'] = 1;
                $toDelete[] = $this->getRequest()->getParam('vid');
            }
        }

       
       if($resourceModel->deleteGallery($toDelete))
       {
         echo json_encode(array('successs'=>true));
         
       }
       else
       {
             echo json_encode(array('failure'=>false));
       }
        
    }
    public function videocheckAction($data1,$productid)
    {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        foreach ($data1 as $key => $value) {
            # code...
       
        $url =  $value['url'];
		$label =  $value['label'];
        
        $video = null;
        $data = array('result' => false , 'message' => '');
        
        try 
        {
            $video = Mage::helper('videogallery/video')->getVideoByUrl($url);
            
            if (!$video )
            {
                $data = array('result' => false , 'message' => 'Could not locate a supported video at that URL.' );
            }
        }
        catch (Exception $e)
        {
            $data = array('result' => false , 'message' => $e->getMessage());
        }
        
        
        if ($video)
        {


            $thumbnail = $video->getThumbnail() ? $video->getThumbnail() : $this->getPlaceholder();

            try {
                    $resourceModel = Mage::getResourceSingleton('videogallery/backend_video');
                    $attributeModel = Mage::getResourceModel('catalog/eav_attribute')->setEntityTypeId(Mage::getModel('eav/entity')->setType('catalog_product'));
                    $attributeModel->load('video_gallery', 'attribute_code');
                    $videoBackendModel = Mage::getModel('videogallery/backend_video')->setAttribute($attributeModel);
       
                        $catalogProductEntityMediaVideoGallery = array(
                            'entity_id' => $productid,
                            'attribute_id' => $attributeModel->getId(),
                            'thumbnail' => $thumbnail,
                            'provider' => $video->getProvider(),
                            'value' => $video->getVideoValue(),
                        );
  
                      
                
                        $valueId = $resourceModel->insertGallery($catalogProductEntityMediaVideoGallery);

                        $catalogProductEntityMediaVideoGalleryValue = array(
                            'value_id' => $valueId,
                            'label' => $label,
                            'description' => '',
                            'position' => 1,
                            'disabled' => false,
                            'store_id' => 0
                        );
                
                     $resourceModel->insertGalleryValueInStore($catalogProductEntityMediaVideoGalleryValue);       
                }
                catch (Exception $e)
                {
                    echo "\n[Error] Error inserting videos.  Check video urls.\n";
                }
        
        }
    
    }
    }
    
    protected function getPlaceholder()
    {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $baseDir = Mage::getBaseDir('media');
        
        // Check if placeholder defined in config
        $isConfigPlaceholder = Mage::getStoreConfig("catalog/placeholder/image_placeholder");
        $configPlaceholder   = '/placeholder/' . $isConfigPlaceholder;
        if ($isConfigPlaceholder && file_exists($baseDir . $configPlaceholder)) {
            $thumbnail = $configPlaceholder;
        }
        else {
            // Replace file with skin or default skin placeholder
            $skinBaseDir     = Mage::getDesign()->getSkinBaseDir();
            $skinPlaceholder = "/images/catalog/product/placeholder/image.jpg";
            $thumbnail = $skinPlaceholder;
            if (file_exists($skinBaseDir . $thumbnail)) {
                $skinDir = $skinBaseDir;
            }
            else {
                $skinDir = Mage::getDesign()->getSkinBaseDir(array('_theme' => 'default'));
                if (!file_exists($skinDir . $thumbnail)) {
                    $skinDir = Mage::getDesign()->getSkinBaseDir(array('_theme' => 'default', '_package' => 'base'));
                }
                if (!file_exists($skinDir . $thumbnail)) {
                    $skinDir = Mage::getDesign()->getSkinBaseDir(array('_theme' => 'default', '_package' => 'default'));
                }
            }
            $thumbnail = str_replace('adminhtml','frontend', $skinDir) . $thumbnail;
        }
        
        return str_replace( Mage::getBaseDir() , "", $thumbnail);
    }
    public function saveAssociatedProductMainAction(){
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        if($this->_request->isPost()) {
             $post = $this->_request->getPost('associated');
              foreach ($post as $key => $associateddata) {
                
                    $this->saveAssociatedProductAction($associateddata);
              }
        }
    }
    public function saveAssociatedProductAction($post) {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
		
        if($post) {
          /*  echo "<pre>";
            print_r($post); die;*/
            $post = $post;
			$current = Mage::app()->getStore()->getCurrentCurrencyCode();
            $baseCurrency = Mage::app()->getStore()->getBaseCurrencyCode();
            $baseCurrencyrate =  Mage::app()->getStore()->getCurrentCurrencyRate();
			 
            if($current == 'AED'){            
               $aedprice =  $post['price'] / $baseCurrencyrate;             
               //$optprice = round($aedprice,3);
               $optprice = number_format(round($aedprice,2),3);
            } else {          
              // $optprice = number_format($post['price'],3);
               $optprice = number_format(round($post['price'],2),3);
            }
           //echo $optprice; die;
            try { 
                $transaction = Mage::getModel('core/resource_transaction');
                $configurableProduct = Mage::getModel('catalog/product')
                    ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
                    ->load($post['super_product_id']);

                    if (!$configurableProduct->isConfigurable()) {
                        $this->_redirect('*/*/');
                        return;
                        }
                    $transaction->addObject($configurableProduct);

                    if(!isset($post['product_id']) || $post['product_id'] == 0)
                    {
                        $product = Mage::getModel('catalog/product')
                            ->setStoreId(0)
                            ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
                            ->setAttributeSetId($configurableProduct->getAttributeSetId());

                        $transaction->addObject($product);
                        $product->setStockData(array(
                            'is_in_stock' => ($post['qty'] > 0) ? 1 : 0,
                            'qty' => $post['qty']
                        ));

                            foreach ($product->getTypeInstance()->getEditableAttributes() as $attribute)
                            {
                                if ($attribute->getIsUnique()
                                    || $attribute->getAttributeCode() == 'url_key'
                                    || $attribute->getFrontend()->getInputType() == 'gallery'
                                    || $attribute->getFrontend()->getInputType() == 'media_image'
                                    || !$attribute->getIsVisible()) {
                                    continue;
                                }

                                $product->setData(
                                    $attribute->getAttributeCode(),
                                    $configurableProduct->getData($attribute->getAttributeCode())
                                );
                            }

                        $product->addData($post);
                        $product->setWebsiteIds($configurableProduct->getWebsiteIds());
                        $result['attributes'] = array();

                        foreach ($configurableProduct->getTypeInstance()->getConfigurableAttributes() as $attribute)
                        {
                            $value = $product->getAttributeText($attribute->getProductAttribute()->getAttributeCode());
                            $result['attributes'][] = array(
                                'label'         => $value,
                                'value_index'   => $product->getData($attribute->getProductAttribute()->getAttributeCode()),
                                'attribute_id'  => $attribute->getProductAttribute()->getId()
                            );
                        }
                        $values = array();
                    
                    foreach($post['options'] AS $index => $option) {
                            
                                
                            $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                                ->setAttributeFilter($option['attribute_id'])
                                ->setStoreFilter(Mage_Core_Model_App::ADMIN_STORE_ID)
                                ->addFieldToFilter('tdv.value', $post[$index]);
								
							/************* Sachin 14-06-2017 ************/    
                             if(!empty( $option['colorvalue'])){
                                $values[] = $option['colorvalue'];
                             }
                             if(!empty( $option['sizevalue'])){
                                $values[] = $option['sizevalue'];
                             }
                             //$post[$index]= $post[$index];
                            
                            /*************** END ************************/   	
                                
                             if ($collection->getSize() > 0) {
                               
                                //$values[] = $collection->getFirstItem()->getId();
                                $post[$index]=$collection->getFirstItem()->getId();
                                
                            } 
                        

                        
                    }
                        
                    $id = Mage::getModel('catalog/product')->getResource()->getIdBySku($configurableProduct->getSku() . '-' . Mage::getModel('catalog/product_url')->formatUrlKey(implode('-', $values)));

                    if ($id) {
                        throw new Exception(Mage::helper('marketplace')->__('Associated Product with same configuration already exists.'));
                    }
                   
                    foreach($post AS $name => $value) {
                        $product->setData($name, $value);
                    }

                    $product->setName($post['name']);
					$product->setConfigFlag(1);
                    $product->setSku($configurableProduct->getSku() . '-' . Mage::getModel('catalog/product_url')->formatUrlKey(implode('-', $values)));

                    $product->validate();
                    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                    $product->save();
                    /***********************************************/
                    $configurableModel = Mage::getModel('marketplace/product_configurable');
					$configurableModel->setProduct($configurableProduct);
					$configurableProductsData = $configurableModel->getConfigurableProductValues();

					$additionalPrice = 0;

					$currentnew = $_SESSION['store_default']['currency_code'];
                    $currencyModel = Mage::getModel('directory/currency');
                    $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
                    $currencies = $currencyModel->getConfigAllowCurrencies();
                    $defaultCurrencies = $currencyModel->getConfigBaseCurrencies();
                     
                    $rates=$currencyModel->getCurrencyRates($defaultCurrencies, $currencies);
					
                if(!isset($post['product_id']) || $post['product_id'] == 0) {
                    if(!$this->_validateValues($configurableProductsData, $post)) {
                        throw new Exception(Mage::helper('marketplace')->__("Simple product with this options is already created."));
                    }

                    $configurableAttributesData = $configurableProduct->getTypeInstance()->getConfigurableAttributesAsArray();
                    $i = 0;
					if($currentnew == 'AED'){            
                        $aedpricenew =  $post['price'] / $rates[$baseCurrencyCode]['AED'];                        
                        //$optpricenew = round($aedpricenew,3);
                        $optpricenew = number_format(round($aedpricenew,2),3);	
						$simplenew =  $product->getPrice() / $rates[$baseCurrencyCode]['AED'];
                       // $simplePrice = round($simplenew,3);
                        $simplePrice = number_format(round($simplenew,2),3);
                    
                    } else {          
                        //$optpricenew = number_format($post['price'],3);
                        $optpricenew = number_format(round($post['price'],2),3);
                        //$simplePrice = number_format($product->getPrice(),3);
                        $simplePrice = number_format(round($product->getPrice(),2),3);
                    }
					
                    foreach($post['options'] AS $index => $option) {
                        if(!isset($post[$index]) || $post[$index] == '') continue;
                        
                        $productData = array(
                            'attribute_id' => $option['attribute_id'],
                            'value_index' => (int) $post[$index],
                            'is_percent' => '0',
                            'pricing_value' => $optpricenew
                        );

                        $configurableProductsData[$product->getId()][] = $productData;
                        $configurableAttributesData[$i]['values'][] = $productData;
                        $additionalPrice[] =  $optpricenew;
                       
                        $i++;
                    }
                   
                }else{
                    $superAttributes = $configurableModel->getSuperAttributes();
                    foreach($superAttributes AS $attribute) {
                        $simpleProductData = $product->getData($attribute['attribute_code']);
                        $configurableProductsData[$product->getId()][] = array(
                            'attribute_id' => $attribute['attribute_id'],
                            'value_index' => $simpleProductData,
                            'is_percent' => '0',
                            'pricing_value' => $simplePrice
                        );
                    }

                }
				if($currentnew == 'AED'){            
                    $aedpricenew =  $post['price'] / $rates[$baseCurrencyCode]['AED'];
                    //$optpricenew = round($aedpricenew,3);
					$optpricenew = number_format(round($aedpricenew,2),3);
                    $configprc =  $configurableProduct->getPrice() / $rates[$baseCurrencyCode]['AED'];
                    //$configurablePrice = round($configprc,3);
                    $configurablePrice = number_format(round($configprc,2),3);
                } else {          
                    //$optpricenew = number_format($post['price'],3);
                    $optpricenew = number_format(round($post['price'],2),3);
                    //$configurablePrice = number_format($configurableProduct->getPrice(),3);
                    $configurablePrice = number_format(round($configurableProduct->getPrice(),2),3);
                }
                $configurableProduct->setCanSaveConfigurableAttributes(true);
                //$product->setPrice($post['price'] + $additionalPrice);
                $product->setPrice($configurablePrice + $additionalPrice);

                $configurableProduct->setConfigurableProductsData($configurableProductsData);
                $configurableProduct->setConfigurableAttributesData($configurableAttributesData);
                $configurableProduct->setCanSaveConfigurableAttributes(true);
				$configurableProduct->setConfigFlag(1);
                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                $configurableProduct->save();

                $p = Mage::getModel('catalog/product')->load($product->getId());
                $p->setPrice($optpricenew + $additionalPrice); // $configurableProduct->getPrice()
                $p->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE)->save();

                } else {
                 
                   
                    $product = Mage::getModel('catalog/product')->load($post['product_id']);
                    $product->setName($post['name']);
                    $product->setStockData(array(
                        'is_in_stock' => ($post['qty']> 0) ? 1 : 0,
                        'qty' => $post['qty']
                    ));
                    $product->setWeight($post['weight']);
                    $product->setWeightUnit($post['weight_unit']);
                    $product->setPrice($optprice);
					$product->setConfigFlag(1);
                    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                    $prices1 = $product->getFormatedTierPrice();
                    $product->unsTierPrice();
                    $product->save();
                    $product->setTierPrice($prices1);
                    $product->save();
                     


                    if(!$product->getId()) {
                        throw new Exception($this->__("Product doesn't not exists"));
                    }

                    if(!Mage::helper('marketplace')->isOwner($product->getId())) {
                        throw new Exception($this->__("Product doesn't belongs to you"));
                    }
                }
                
                $this->getResponse()->setRedirect(Mage::getUrl('marketplace/product/associatedProducts', array('id' => $post['super_product_id'])));
                
               /* Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/product/associatedProducts', array('id' => $post['super_product_id'])));*/
            } catch (Exception $e) {
               
                
                if(!isset($post['product_id']) || $post['product_id'] == 0) {
                    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                    $product->delete();
                }
                Mage::getSingleton('core/session')->addError("Something went wrong please contact to main admin!!!");
                Mage::logException($e);
                $this->getResponse()->setRedirect(Mage::getUrl('marketplace/product/associatedProducts', array('id' => $post['super_product_id'])));
            }
        }
    }

    private function _validateValues($configurable_values, $values_selected) {
        $isValid = true;

        foreach($values_selected['options'] AS $index => $value) {
            foreach($configurable_values as $product) {
                $matchedProductValues = 0;
                $countValues = count($product);

                foreach($product AS $confValue) {
                    if($confValue['attribute_id'] == $value['attribute_id'] &&
                        $confValue['value_index'] == $values_selected[$index]) {
                        $matchedProductValues++;
                    }
                }

                if($matchedProductValues >= $countValues) {
                    $isValid = false;
                }
            }
        }

        return $isValid;
    }

    public function deleteAssociatedAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $id = $this->_request->getParam('id', null);

        try {
            if($id == null) {
               // throw new Exception('No product id');
                 Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
            }

            $p = Mage::getModel('catalog/product')->load($id);
            $ids = Mage::getModel('catalog/product_type_configurable')
                ->getParentIdsByChild( $p->getId() );

            if(count($ids) == 0) {
                throw new Exception($this->__('Product is not associated to any configurable products'));
            }

            if($p->getData('creator_id') != $this->_getHelper()->getSupplierId()) {
                //throw new Exception('No product');
                 Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
            }

            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

            $p->delete();
            Mage::getSingleton('core/session')->addSuccess($this->__("Product %s was successfully deleted", $p->getName()));
        } catch(Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
            Mage::logException($e);
        }
        $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
    }

    public function attachToConfigurableAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $id = $this->_request->getParam('id', null);
        $configurableId = $this->_request->getParam('configurable', null);

        try {
            if($id == null) {
               // throw new Exception($this->__('No product id'));
                 Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
            }

            if($configurableId == null) {
               // throw new Exception($this->__('No product id'));
                 Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
            }

            $configurableProduct = Mage::getModel('catalog/product')->load($configurableId);
            $product = Mage::getModel('catalog/product')->load($id);

            $configurableModel = Mage::getModel('marketplace/product_configurable');
            $configurableModel->setProduct($configurableProduct);
            $configurableProductsData = $configurableModel->getConfigurableProductValues();

            $additionalPrice = 0;
            $configurableProductsData[$product->getId()][] = array(
                'is_percent' => '0',
            );

            $configurableProduct->setCanSaveConfigurableAttributes(true);
            $product->setPrice($configurableProduct->getPrice() + $additionalPrice);

            $configurableProduct->setConfigurableProductsData($configurableProductsData);
            $configurableProduct->save();
            Mage::getSingleton('core/session')->addSuccess($this->__("Product %s was successfully attached", $product->getName()));
        } catch(Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
            Mage::logException($e);
        }

        $this->getResponse()->setRedirect(Mage::getUrl('marketplace/product/associatedProducts', array('id' => $configurableId)));
    }

    public function changeAssociatedStatusAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $id = $this->_request->getPost('product_id', null);
        
        $configurableId = $this->_request->getPost('configurable_id', null);

        try {
            if($id == null) {
                //throw new Exception($this->__('No product id'));
                 Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
            }

            if($configurableId == null) {
                //throw new Exception($this->__('No product id'));
                 Mage::getSingleton('core/session')->addError($this->__("No product id"));
                $this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
            }

            $configurableProduct = Mage::getModel('catalog/product')->load($configurableId);
            $product = Mage::getModel('catalog/product')->load($id);

            $configurableModel = Mage::getModel('marketplace/product_configurable');
            $configurableModel->setProduct($configurableProduct);
            $configurableProductsData = $configurableModel->getConfigurableProductValues();

            $additionalPrice = 0;

            if($this->_request->getPost('status') == 'true') {
                $configurableProductsData[$product->getId()][] = array(
                    'is_percent' => '0',
                );

                $configurableProduct->setCanSaveConfigurableAttributes(true);
                $product->setPrice($configurableProduct->getPrice() + $additionalPrice);
            } else {
                if(isset($configurableProductsData[$product->getId()])) {
                    unset($configurableProductsData[$product->getId()]);
                }
            }

            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

            $configurableProduct->setConfigurableProductsData($configurableProductsData);
            $configurableProduct->save();
        } catch(Exception $e) {
            Mage::logException($e);
        }
    }

    public function addattributeAction() {
        if($this->_getHelper()->getSupplierId()=="")
        {
            $this->_redirect('supplier');
            return true;
        }
        $attribute_code = $this->getRequest()->getParam('attributeid');
        $attribute_value =$this->getRequest()->getParam('attributevalue');
        

        $attribute_model = Mage::getModel('catalog/resource_eav_attribute');
        $attribute = $attribute_model->load($attribute_code);

        if(!$this->attributeValueExists($attribute_code, $attribute_value))
        {
            $value['option'] = array($attribute_value,$attribute_value);
            
            $result = array('value' => $value);
            $attribute->setData('option',$result);
           
            if( $attribute->save())
               {

                 echo json_encode(array('successs'=>true));
                 
               }
               else
               {
                     echo json_encode(array('failure'=>false));
               }
        }

        /*$attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;
        $attribute_table= $attribute_options_model->setAttribute($attribute);
        $options= $attribute_options_model->getAllOptions(false);
        
        foreach($options as $option)
        {
            if ($option['label'] == $attribute_value)
            {
                return $option['value'];
            }
        }
       return false;*/

    }

    public function attributeValueExists($attribute_code, $attribute_value)
    {
        $attribute_model        = Mage::getModel('eav/entity_attribute');
        $attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;

        $attribute              = $attribute_model->load($attribute_code);

        $attribute_table        = $attribute_options_model->setAttribute($attribute);
        $options                = $attribute_options_model->getAllOptions(false);

        foreach($options as $option)
        {
            if ($option['label'] == $attribute_value)
            {
                return $option['value'];
            }
        }

        return false;
    }
     public function viewmoreproductAction()
    {
        if(Mage::getSingleton('core/session')->setViewmoreflag(1))
        {
            echo json_encode(array('successs'=>true));
        }else
        {
            echo json_encode(array('successs'=>false));
        }
         
    }
    public function viewmorepopularproductAction()
    {
        if(Mage::getSingleton('core/session')->setPopularviewmoreflag(1))
        {
            echo json_encode(array('successs'=>true));
        }else
        {
            echo json_encode(array('successs'=>false));
        }
         
    }
	/**************** Get Media images for confogurables child products (simple) ***/
    public function childProductsimagesAction(){
        $product_id = $this->getRequest()->getParam('childid');
        $product = Mage::getModel('catalog/product')->load($product_id);

        $gallery_images = Mage::getModel('catalog/product')->load($product->getId())->getMediaGalleryImages();

        $items = array();

        foreach($gallery_images as $g_image) {
            $items[] = $g_image['url'];
        }
        if(sizeof($items) > 0){
            echo json_encode(array('successs'=>true,'data' => $items)); 
        }else{
            echo json_encode(array('successs'=>false)); 
        }
        exit;
    }
    /****************************** END 21-06-2017 ***************************************/
    
}