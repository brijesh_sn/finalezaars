<?php

class Cminds_Marketplace_OrderController extends Cminds_Marketplace_Controller_Action {
    public function preDispatch() {
        parent::preDispatch();
//        $this->_getHelper()->validateModule();
        $hasAccess = $this->_getHelper()->hasAccess();

        if(!$hasAccess) {
            $this->getResponse()->setRedirect($this->_getHelper('supplierfrontendproductuploader')->getSupplierLoginPage());
        }
    }
    public function indexAction() {
        $this->_renderBlocks(false, true);
    }

    public function importShippingAction () {
        $this->_renderBlocks(false, true);
    }
    public function viewAction() {

        // @todo: add validation if products from orders belongs to the supplier
        $id = $this->getRequest()->getParam('id');
        
        Mage::register('order_id', $id);
		//Mage::getSingleton('core/session')->addError($this->__("No product id"));
        //$this->getResponse()->setRedirect(Mage::getUrl('supplier/product/list'));
        $this->_renderBlocks();
    }


    public function exportCsvAction() {
        $orderItemsCollection = $this->_prepareOrderCollection();
        $productCsv = array();

        foreach($orderItemsCollection AS $orderItem) {
            $order = $this->getOrder($orderItem->getOrderId());
            $shippingAddress = $order->getShippingAddress();
            $productCsv[] = array(
                'Store SKU' => $orderItem->getSku(),
                'Order #' => $order->getIncrementId(),
                'Sale Date' => $orderItem->getName(),
                'Name' => $orderItem->getName(),
                'Quantity' => $orderItem->getQtyOrdered(),
                'Price' => $orderItem->getPrice(),
                'Discount' => $orderItem->getOriginalPrice() - $orderItem->getPrice(),
                'Shipping Address - First Name' => $shippingAddress ? $shippingAddress->getFirstname() : '',
                'Shipping Address - Last Name' => $shippingAddress ? $shippingAddress->getLastname() : '',
                'Shipping Address - Street' => $shippingAddress ? $shippingAddress->getStreet(1) . ' ' . $shippingAddress->getStreet(2) : '',
                'Shipping Address - City' => $shippingAddress ? $shippingAddress->getCity() : '',
                'Shipping Address - Region' => $shippingAddress ? $shippingAddress->getRegion() : ''
            );
        }

        Mage::helper('supplierfrontendproductuploader')->prepareCsvHeaders("order_export_" . date("Y-m-d") . ".csv");
        echo Mage::helper('supplierfrontendproductuploader')->array2Csv($productCsv);
    }



    private function _prepareOrderCollection() {
        $eavAttribute   = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $supplier_id    = Mage::helper('marketplace')->getSupplierId();
        $code           = $eavAttribute->getIdByCode('catalog_product', 'creator_id');
        $table          = "catalog_product_entity_int";
        $tableName      = Mage::getSingleton("core/resource")->getTableName($table);
        $orderTable = Mage::getSingleton('core/resource')->getTableName('sales/order');

        $collection = Mage::getModel('sales/order_item')->getCollection();
        $collection->getSelect()
            ->joinInner(array('o' => $orderTable), 'o.entity_id = main_table.order_id', array())
            ->joinInner(array('e' => $tableName), 'e.entity_id = main_table.product_id AND e.attribute_id = ' . $code, array() )
            ->where('main_table.parent_item_id is null')
            ->where('e.value = ?', $supplier_id)
            ->group('o.entity_id')
            ->order('o.entity_id DESC');

        if($this->getFilter('autoincrement_id')) {
            $collection->getSelect()->where('o.increment_id LIKE ?', "%".$this->getFilter('autoincrement_id')."%");
        }
        if($this->getFilter('status')) {
            $collection->getSelect()->where('o.status = ?', $this->getFilter('status'));
        }

        if($this->getFilter('from') && strtotime($this->getFilter('from'))) {
            $datetime = new DateTime($this->getFilter('from'));
            $collection->getSelect()->where('main_table.created_at >= ?', $datetime->format('Y-m-d') . " 00:00:00");
        }
        if($this->getFilter('to') && strtotime($this->getFilter('to'))) {
            $datetime = new DateTime($this->getFilter('to'));
            $collection->getSelect()->where('main_table.created_at <= ?', $datetime->format('Y-m-d') . " 23:59:59");
        }

        return $collection;
    }

    private function getFilter($key) {
        return $this->getRequest()->getPost($key);
    }

    private function getOrder($order_id) {
        if(!isset($this->_orders[$order_id])) {
            $this->_orders[$order_id] = Mage::getModel('sales/order')->load($order_id);
        }
        return $this->_orders[$order_id];
    }

    public function importShipmentAction()
    {
        try {
            if (isset($_FILES["import_shipment"])) {
                if ($_FILES["import_shipment"]["error"] > 0) {
                    switch ($_FILES["import_shipment"]["error"]) {
                        case 1:
                            throw new Mage_Exception("The uploaded file exceeds the upload_max_filesize directive in php.ini");
                            break;
                        case 2:
                            throw new Mage_Exception("The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form");
                            break;
                        case 3:
                            throw new Mage_Exception("The uploaded file was only partially uploaded.");
                            break;
                        case 4:
                            throw new Mage_Exception("No file was uploaded.");
                            break;
                        case 6:
                            throw new Mage_Exception("Missing a temporary folder.");
                            break;
                        case 7:
                            throw new Mage_Exception("Failed to write file to disk.");
                            break;
                        case 8:
                            throw new Mage_Exception("A PHP extension stopped the file upload.");
                            break;
                    }
                } else {
                    $csvObject = new Varien_File_Csv();
                    $s = $csvObject->getData($_FILES["import_shipment"]['tmp_name']);
                    if(isset($s[0][0])) {
                        if(!is_numeric($s[0][0])) {
                            unset($s[0]);
                        }
                        foreach($s AS $shipment) {
                            $this->_createShipment($shipment);
                        }
                    }
                }
            } else {
                throw new Mage_Exception("No file selected");
            }
            $this->getResponse()->setRedirect(Mage::getUrl('*/*/index/'));

        }catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
            $this->getResponse()->setRedirect(Mage::getUrl('*/*/index/'));
        }
    }

    private function _createShipment($line) {

        $order = Mage::getModel('sales/order')->loadByIncrementId($line[0]);

        $products = array();

        foreach($order->getAllItems() AS $item) {
            if($item->getProductId() && Mage::helper('marketplace')->isOwner($item->getProductId())) {
                $products[$item->getId()] = $item->getQtyOrdered();
            }

        }
        if($order->getState() == 'canceled') {
            throw new Exception('You cannot create shipment for canceled order');
        }
        $shipment = $order->prepareShipment($products);

        $shipment->sendEmail(false)
            ->setEmailSent(false)
            ->register()
            ->save();

        foreach($shipment->getAllItems() AS $item) {
            $orderItem = Mage::getModel('sales/order_item')->load($item->getOrderItemId());
            $orderItem->setQtyShipped($item->getQty() + $orderItem->getQtyShipped());
            $orderItem->save();
        }

        $sh = Mage::getModel('sales/order_shipment_track')
            ->setShipment($shipment)
            ->setData('title', $line[2])
            ->setData('number', $line[1])
            ->setData('carrier_code', 'custom')
            ->setData('order_id', $order->getId());

        $sh->save();

        $loggedUser = Mage::getSingleton( 'customer/session', array('name' => 'frontend') );
        $customer = $loggedUser->getCustomer();

        $comment = $customer->getFirstname() .' '.$customer->getLastname() . ' (#'.$customer->getId().') created shipment for ' . count($products) . ' item(s)';

        $order->addStatusHistoryComment($comment);

        $fullyShipped = true;

        foreach ($order->getAllItems() as $item) {
            if ($item->getQtyToShip()>0 && !$item->getIsVirtual()
                && !$item->getLockedDoShip())
            {
                $fullyShipped = false;
            }
        }

        if($fullyShipped) {
            if($order->getState() != Mage_Sales_Model_Order::STATE_PROCESSING) {
                $state = Mage_Sales_Model_Order::STATE_PROCESSING;
            } elseif($order->getState() == Mage_Sales_Model_Order::STATE_PROCESSING) {
                $state = Mage_Sales_Model_Order::STATE_COMPLETE;
            }

            if($state) {
                $order->setData('state', $state);

                $status = $order->getConfig()->getStateDefaultStatus($state);
                $order->setStatus($status);
            }
        }
        $order->save();
    }

    public function downloadShipmentCsvAction()
    {
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=shipment_schema.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $headers = array();
        $headers[] = 'ID';
        $headers[] = 'Tracking Code';
        $headers[] = 'Title';

        echo implode(',', $headers);
    }
}
