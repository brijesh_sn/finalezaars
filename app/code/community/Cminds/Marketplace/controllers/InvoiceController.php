<?php

class Cminds_Marketplace_InvoiceController extends Cminds_Marketplace_Controller_Action {
    public function preDispatch() {
        parent::preDispatch();
        $hasAccess = $this->_getHelper()->hasAccess();

        if(!$hasAccess) {
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::helper('customer')->getLoginUrl());
        }
    }
    public function createAction() {
        $id = $this->getRequest()->getParam('id');
        Mage::register('order_id', $id);
        $this->_renderBlocks();
    }
    public function viewAction() {
        $id = $this->getRequest()->getParam('id');
        Mage::register('shipment_id', $id);
        $this->_renderBlocks();
    }
    public function saveAction() {
        $post = $this->_request->getPost();
        Mage::register("vendor_creation", true);

        try {
            $transaction = Mage::getModel('core/resource_transaction');
            $order = Mage::getModel('sales/order')->load($post['order_id']);


            foreach($post['product'] AS $product_id => $qty) {

                if($qty <= 0) {
                    unset($post['product'][$product_id]);
                }
                $itemModel = Mage::getModel('sales/order_item')->load($product_id);

                if(!$itemModel->getProductId() || !Mage::helper('marketplace')->isOwner($itemModel->getProductId())) {
                    throw new Exception('You cannot ship non-owning products');
                }

                if($itemModel->getQtyOrdered() < ($itemModel->getQtyInvoiced() + intval($qty))) {
                    throw new Exception('You cannot ship more products than it was ordered');
                }

            }

            if($order->getState() == 'canceled') {
                throw new Exception('You cannot create shipment for canceled order');
            }

            $invoice = Mage::getModel('marketplace/invoice', $order)->prepareInvoice($post['product']);

            // echo $invoice->getGrandTotal();
            // echo  $invoice->setGrandTotal($order->getGrandTotal()); // Sachin - 07-03-2018
            //echo $invoice->getShippingAmount();

            /********** Brijesh Dhami Admin invoice list & view total correction ************/
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $_items = $invoice->getAllItems();  
            $_count = count($_items);

            $baseCode = Mage::app()->getBaseCurrencyCode();      
            $current = Mage::app()->getStore()->getCurrentCurrencyCode(); 
            $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies(); 
            $rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCode, array_values($allowedCurrencies));

            foreach ($_items as $_item): 
                $storecode = $invoice->getOrderCurrencyCode(); 
                $pid = $_item->getProductId();
                $Qnt = $_item->getQty();
                $_productsss = Mage::getModel('catalog/product')->load($pid);
                $method = $_productsss->getShippingMethod();          

                $sql  = "SELECT * FROM `marketplace_supplier_shipping_methods` WHERE `supplier_id`= '".$_productsss->getCreatorId()."'  AND `name` = '".$method."'";
                    $rowssss = $connection->fetchAll($sql);
                    if(count($rowssss) > 0){
                    if($rowssss[0]['flat_rate_available'] == true){
                        $shipping_price[$pid] = $rowssss[0]['flat_rate_fee'] * $Qnt[1];
                        $shipping_baseprice[$pid] = $rowssss[0]['flat_rate_fee'];
                    }
                    if($rowssss[0]['table_rate_available'] == true){
                        $shipping_price[$pid] = $rowssss[0]['table_rate_fee'] * $Qnt[1];
                        $shipping_baseprice[$pid] = $rowssss[0]['table_rate_fee'];
                    }
                    }else{
                        $shipping_price[$pid] = '';
                        $shipping_baseprice[$pid] = '';
                    }

                    $shipbaseprice = $shipping_baseprice[$pid];

                    $shipbaseprice123 = $shipping_baseprice[$pid];

                    $shipprice = Mage::helper('core')->currency($shipping_baseprice[$pid], true, false);  
                    
                        if($current == 'OMR'){
                            if($storecode == 'AED'){ 
                                $shipbaseprice = number_format(round(($shipbaseprice123 * $rates['AED']),2),3); 
                            }
                            else{
                                $total_omr = ($shipbaseprice123);
                                $shipbaseprice = number_format(round($total_omr,2),3);          
                            }
                        }else{

                            if($storecode == 'AED'){
                                $finalsubprice_AED = ($shipbaseprice123 * $rates['AED']);
                                $shipbaseprice = number_format(round($finalsubprice_AED,2),3);
                               
                             }
                             else{
                                $total_omr = ($shipbaseprice123);
                                //echo $current.round($total,3);
                                $shipbaseprice = number_format(round($total_omr,2),3);
                                //echo Mage::helper('core')->currency($_item->getGrandTotal()); 
                             }
                        } 
                    $shipbasepricess += str_replace(",","",$shipbaseprice);
                  
                endforeach;

            $abovediscount = ($invoice->getSubtotal() + $shipbasepricess);
            
            $finalamount = ($abovediscount - $discount_amount) + $invoice->getTaxAmount();
            //print_r($shipbasepricess); 
            /*if($current == 'OMR'){
                if($storecode == 'AED'){                    
                    $finalamount = number_format(round(($finalamountss * $rates['AED']),2),3); 
                }
                else{                   
                    $finalamount = $finalamountss;          
                }
            }else{

                if($storecode == 'AED'){
                    $finalsubprice_AED = ($finalamountss * $rates['AED']);
                    $finalamount = number_format(round($finalsubprice_AED,2),3);
                    
                 }
                 else{                    
                    $finalamount = $finalamountss;
                   
                 }
            } */


            
           
            // echo "<pre/>";
            // print_r($finalamountss);
            // print_r($finalamount);
           //  exit;
            $invoice->setShippingAmount($shipbasepricess);
            $invoice->setGrandTotal($finalamount);
            $invoice->setBaseGrandTotal($finalamount);
            /*********** End code ****************/    

            $invoice->register()
                ->save()
                ->sendEmail((isset($post['notify_customer']) && $post['notify_customer'] == '1'))
                ->setEmailSent(false);

            $invoice->getOrder()->setIsInProcess(true);

            //$amount = 0;

            foreach($invoice->getAllItems() AS $item) {
                $orderItem = Mage::getModel('sales/order_item')->load($item->getOrderItemId());
                $orderItem->setQtyInvoiced($item->getQty() + $orderItem->getQtyInvoiced());
            }


            $loggedUser = Mage::getSingleton('customer/session', array('name' => 'frontend') );
            $customer = $loggedUser->getCustomer();

            $comment = $customer->getFirstname() .' '.$customer->getLastname() . ' (#'.$customer->getId().') created invoice for ' . count($post['product']) . ' item(s)';

            $order->addStatusHistoryComment($comment);

            //$order->setTotalPaid($order->getGrandTotal()); 

            $fullyInvoiced = true;

            foreach ($order->getAllItems() as $item) {
                if ($item->getQtyToInvoiced() > 0) {
                    $fullyInvoiced = false;
                }
            }

            if($fullyInvoiced) {
                if($order->getState() != Mage_Sales_Model_Order::STATE_PROCESSING) {
                    $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                    $order->setState($state, true);

                } elseif($order->getState() == Mage_Sales_Model_Order::STATE_PROCESSING) {
//                    $state = Mage_Sales_Model_Order::STATE_COMPLETE;
                }

            }

           // $invoice->getOrder()->setTotalPaid($order->getGrandTotal());
            //$invoice->getOrder()->setGrandTotal($order->getGrandTotal());

            $transaction->addObject($invoice);
            $transaction->addObject($orderItem);
            $transaction->addObject($order);

            $transaction->save();
            Mage::getSingleton('core/session')->addSuccess('Invoice for order #'.$order->getIncrementId().' was created');
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('*/order/view/', array('id' => $post['order_id'], 'tab' => 'invoice')));
        } catch (Exception $e) {
            if (null !== $order->getIncrementId()) {
                $order->addStatusHistoryComment('Failed to create invoice - '. $e->getMessage())
                    ->save();
            }
            Mage::getSingleton('core/session')->addError($e->getMessage());
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('*/invoice/create/', array('id' => $post['order_id'], 'tab' => 'invoice')));
        }
    }

    public function printAction()
    {
        if ($invoiceId = $this->getRequest()->getParam('id')) {
            if ($invoice = Mage::getModel('sales/order_invoice')->load($invoiceId)) {

        /************** Mayur panchal 21-03-2018 ********************/
            $invoiceitems =  $invoice;      
       
            $_items_product = $invoiceitems->getAllItems();
            $count = count($_items_product);
            $count_p = 0;

            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
           
            foreach ($_items_product as $_item_p) {
                
                $pid = $_item_p->getProductId();
                $Qnt = $_item_p->getQty();
                $_productsss = Mage::getModel('catalog/product')->load($pid);
                $method = $_productsss->getShippingMethod();     

                $sql  = "SELECT * FROM `marketplace_supplier_shipping_methods` WHERE `supplier_id`= '".$_productsss->getCreatorId()."'  AND `name` = '".$method."'";

                $rowssss = $connection->fetchAll($sql);                

                if(count($rowssss) > 0){

                if($rowssss[0]['flat_rate_available'] == true){
                    $shipping_price[$pid] = $rowssss[0]['flat_rate_fee'] * $Qnt[1];
                    $shipping_baseprice[$pid] = $rowssss[0]['flat_rate_fee'];
                }
                if($rowssss[0]['table_rate_available'] == true){
                    $shipping_price[$pid] = $rowssss[0]['table_rate_fee'] * $Qnt[1];
                    $shipping_baseprice[$pid] = $rowssss[0]['table_rate_fee'];
                }
                }else{
                    $shipping_price[$pid] = '';
                    $shipping_baseprice[$pid] = '';
                }
                if($count > 1){
                     $shipprice += $shipping_baseprice[$pid];

                }else{

                    $shipprice = $shipping_baseprice[$pid];
                }
            }

            $shipprice = number_format(round($shipprice,2),3);
            $invoice->setShippingAmount($shipprice);             

        /*************** end 21-03-2018 *********/
            $getSubtotal = $invoice->getSubtotal();
            $setSubtotal = number_format(round($getSubtotal,2),3);
            $setSubtotal = $invoice->setSubtotal($setSubtotal);

            $total = $invoice->getSubtotal() + $invoice->getShippingAmount();  // Custom total
            $total = number_format(round($total,2),3);
            $invoice->setGrandTotal($total); // Custom total
         

                
                $pdf = Mage::getModel('sales/order_pdf_invoice')->setIsSupplier(true)->getPdf(array($invoice));

                $this->_prepareDownloadResponse('invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').
                    '.pdf', $pdf->render(), 'application/pdf');
            }
        }
        else {
            $this->_forward('noRoute');
        }
    }
}
