<?php

class Cminds_Marketplace_SettingsController extends Cminds_Marketplace_Controller_Action {
    public function preDispatch() {
        parent::preDispatch();
        $hasAccess = $this->_getHelper()->hasAccess();

        if(!$hasAccess) {
            $this->getResponse()->setRedirect($this->_getHelper('supplierfrontendproductuploader')->getSupplierLoginPage());
        }
    }
    public function shippingAction() {
        if(!Mage::getStoreConfig('marketplace_configuration/presentation/change_shipping_costs')) {
            $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
            $this->getResponse()->setHeader('Status','404 File not found');
            $this->_forward('defaultNoRoute');
        }

        $this->_renderBlocks();
    }

    public function shippingSaveAction() {
        $postData = $this->getRequest()->getPost();

        if(!Mage::getStoreConfig('marketplace_configuration/presentation/change_shipping_costs')) {
            $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
            $this->getResponse()->setHeader('Status','404 File not found');
            $this->_forward('defaultNoRoute');
        }

        try {
			$current = Mage::app()->getStore()->getCurrentCurrencyCode();
            $baseCurrency = Mage::app()->getStore()->getBaseCurrencyCode();
            $baseCurrencyrate =  Mage::app()->getStore()->getCurrentCurrencyRate();
			
            $transaction = Mage::getModel('core/resource_transaction');
            $removedItems = explode(',', $postData['removedItems']);

            foreach($removedItems AS $item) {
                if(!$item) continue;
                $shipping = Mage::getModel('marketplace/methods')->load($item);

                if($shipping->getId()) {
                    $shipping->delete();
                }
            }
            if(!isset($postData['id'])) $postData['id'] = array();

            foreach($postData['id'] AS $i => $k) {
                $shipping = Mage::getModel('marketplace/methods')->load($k);

                $shipping->setSupplierId(Mage::helper('marketplace')->getSupplierId());
                $shipping->setName($postData['shipping_name'][$i]);
                $shipping->setFlatRateFee(0);
                $shipping->setFlatRateAvailable(0);
                $shipping->setTableRateAvailable(0);
                $shipping->setTableRateCondition(0);
                $shipping->setTableRateFee(0);
                $shipping->setFreeShipping(0);

                if(!isset($postData['shipping_method'][$i])) {
                    $shippingMethod = false;
                }
                if(isset($postData['shipping_method'][$i]) && isset($postData['shipping_method'][$i][$k])) {
                    $shippingMethod = $postData['shipping_method'][$i][$k];
                } else {
                    $shippingMethod = false;
                }

                if(!$shippingMethod && isset($postData['shipping_method']) && isset($postData['shipping_method'][$i]) && is_array($postData['shipping_method'][$i])) {
                    $shippingMethod = reset($postData['shipping_method'][$i]);
                }

                if ($shippingMethod && $shippingMethod == "flat_rate") {
					
					if($current == 'AED'){            
                        $aedprice =  $postData['flat_rate_fee'][$i] / $baseCurrencyrate;
                       // $flat_rate_fee = round($aedprice,3);
                        $flat_rate_fee = number_format(round($aedprice,2),3);
                    } else {          
                       //$flat_rate_fee = number_format($postData['flat_rate_fee'][$i],3);
                       $flat_rate_fee = number_format(round($postData['flat_rate_fee'][$i],2),3);

                    }
					
                    $shipping->setFlatRateAvailable(1);
                    $shipping->setFlatRateFee($flat_rate_fee);
                } else {
                    $shipping->setFlatRateFee(0);
                    $shipping->setFlatRateAvailable(0);
                }
                if ($shippingMethod && $shippingMethod == "table_rate") {
                    $shipping->setTableRateAvailable(1);
                    if($k != '') {
						if($current == 'AED'){            
                            $tableaedprice =  $postData['table_rate_fee'][$i][$k] / $baseCurrencyrate;
                            //$table_rate_fee = round($tableaedprice,3);
                            $table_rate_fee = number_format(round($tableaedprice,2),3);
                        } else {          
                           //$table_rate_fee = number_format($postData['table_rate_fee'][$i][$k],3);
                           $table_rate_fee = number_format(round($postData['table_rate_fee'][$i][$k],2),3);                           
                        }
                       // echo $table_rate_fee ;
                       // die;
                        $shipping->setTableRateFee($table_rate_fee);
                    } else {
						if($current == 'AED'){            
                            $tableaedprice =  $postData['table_rate_fee'][$i] / $baseCurrencyrate;
                           // $table_rate_fee = round($tableaedprice,3);
                            $table_rate_fee = number_format(round($tableaedprice,2),3);
                        } else {          
                          // $table_rate_fee = number_format($postData['table_rate_fee'][$i],3);
                           $table_rate_fee = number_format(round($postData['table_rate_fee'][$i],2),3);

                        }   
                       // echo "bar";
                       // die;                    
                        $shipping->setTableRateFee($table_rate_fee);
                    }

                    if(isset($postData['table_rate_condition'][$i])) {
                        $shipping->setTableRateCondition($postData['table_rate_condition'][$i]);
                    } else {
                        $shipping->setTableRateCondition(1);
                    }
                    $shipping->save();
                    $this->_parseUploadedCsv($i,$shipping->getId());
                } else {
                    $shipping->setTableRateFee(0);
                    $shipping->setTableRateAvailable(0);
                }
                if ($shippingMethod && $shippingMethod == "free_shipping") {
                    $shipping->setFreeShipping(1);
                } else {
                    $shipping->setFreeShipping(0);
                }
                $transaction->addObject($shipping);
            }
            $transaction->save();

            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl() . 'marketplace/settings/shipping/');
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl() . 'marketplace/settings/shipping/');
            Mage::log($e->getMessage());
        }
    }

    public function profileAction() {
        if(!Mage::getStoreConfig('marketplace_configuration/general/supplier_page_enabled')) {
            $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
            $this->getResponse()->setHeader('Status','404 File not found');
            $this->_forward('defaultNoRoute');
        }

        $this->_renderBlocks(true, true, false, true);
    }

     public function logoutAction() {
        
          Mage::getSingleton('customer/session')->logout();
         $this->_redirect('supplier');
    }



    public function profileSaveAction() {
        $postData = $this->getRequest()->getPost();
        
        
        if(!Mage::getStoreConfig('marketplace_configuration/general/supplier_page_enabled')) {
            $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
            $this->getResponse()->setHeader('Status','404 File not found');
            $this->_forward('defaultNoRoute');
        }

        try {
                $transaction = Mage::getModel('core/resource_transaction');
                $customerData = false;
            
            if(Mage::getSingleton('customer/session')->isLoggedIn()) {
                $customerData = Mage::getModel('customer/customer')->load(Mage::getSingleton('customer/session')->getId());

                            $loginid= Mage::getModel('customer/session')->getId();
               
                
            }

            if(!$customerData) {
                throw new ErrorException('Supplier does not exists');
            }


            $waitingForApproval = false;

            if(isset($postData['submit'])) {
                $changed = false;
                $forceChange = false;
                 $customername=Mage::getModel('customer/customer')->getCollection()->addAttributeToFilter('supplier_name', array('eq' => $postData['name']))->getData();
                 $customerurl=Mage::getModel('customer/customer')->getCollection()->addAttributeToFilter('Supplier_url', array('eq' => $postData['url']))->getData();
                 
                 if($postData['trade_license_expiry_date'])
                 {
                    
                    $customerData->setTradeLicenseExpiryDate($postData['trade_license_expiry_date']);
                 }
                
                if($postData['name'] != $customerData->getSupplierName()) {
                    $changed = true;
                    $forceChange = true;

                    if(!empty($customername))
                    {
                     if($customername[0]['entity_id']!= $loginid)
                        {
                            throw new ErrorException('Supplier Name is exists');
                        }
                        else
                        {
                            $customerData->setSupplierNameNew($postData['name']);
                        }
                    }else{
                        $customerData->setSupplierNameNew($postData['name']);
                    }
                    
                }
                else
                {
                    if($customername[0]['entity_id']!= $loginid)
                    {
                            throw new ErrorException('Supplier Name is exists');
                    }
                }



                if($postData['url'] != $customerData->getSupplierUrl()) {
                    $changed = true;
                    $forceChange = true;

                    if(!empty($customerurl))
                    {
                     if($customerurl[0]['entity_id']!= $loginid)
                        {
                            throw new ErrorException('Supplier Name is exists');
                        }
                        else
                        {
                            $customerData->setSupplierUrlNew($postData['url']);
                        }
                    }else{
                        $customerData->setSupplierUrlNew($postData['url']);
                    }
                   
                }
                else
                {
                    if($customerurl[0]['entity_id']!= $loginid)
                    {
                            throw new ErrorException('Supplier URL is exists');
                    }
                }


                $path = Mage::getBaseDir('media') . DS . 'supplier_logos' . DS;
                /************************ Banner ******************************/
                $path1 = Mage::getBaseDir('media') . DS . 'supplier_banner' . DS;
                /************************ Banner ******************************/

                if(isset($postData['remove_logo'])) {
                    $s = $customerData->getSupplierLogo();

                    if(file_exists($path . $s)) {
                        unlink($path . $s);
                    }

                    $customerData->setSupplierLogo(null);
                }
                /************************ Banner ******************************/
                if(isset($postData['remove_banner'])) {
                    
                    $s = $customerData->getSupplierBanner();
                    
                    if(file_exists($path1 . $s)) {
                        unlink($path1 . $s);
                    }

                    $customerData->setSupplierBanner(null);
                }
                  /************************ Banner ******************************/
                
                 if(isset($_FILES['logo']['name']) and (file_exists($_FILES['logo']['tmp_name']))) {
                    $uploader = new Varien_File_Uploader('logo');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);

                    $nameSplit = explode('.', $_FILES['logo']['name']);
                    $ext = $nameSplit[count($nameSplit)-1];
                    $newName = md5($_FILES['logo']['name'] . time()) . '.' . $ext;
                    $customerData->setSupplierLogo($newName);
                    $uploader->save($path, $newName);
                    $imageUrl = $path . $newName;
                    $imageResized = $path . $newName;

                    // resize image only if the image file exists and the resized image file doesn't exist
                    // the image is resized proportionally with the width/height 135px
                    //!file_exists($imageResized)&&
                    if (file_exists($imageUrl)) :
                        $imageObj = new Varien_Image($imageUrl);
                        $imageObj->constrainOnly(TRUE);
                        $imageObj->keepAspectRatio(TRUE);
                        $imageObj->keepFrame(FALSE);
                        $imageObj->resize(350, 350);
                        $imageObj->save($imageResized);

                    endif;

//                    $changed = true;
                }

                /************************ Banner ******************************/
                
                if(isset($_FILES['banner']['name']) and (file_exists($_FILES['banner']['tmp_name']))) {
                    $uploader1 = new Varien_File_Uploader('banner');
                    $uploader1->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader1->setAllowRenameFiles(false);
                    $uploader1->setFilesDispersion(false);

                    $nameSplit1 = explode('.', $_FILES['banner']['name']);
                    $ext1 = $nameSplit1[count($nameSplit1)-1];
                    $newName1 = md5($_FILES['banner']['name'] . time()) . '.' . $ext1;
                    $customerData->setSupplierBanner($newName1);
                    $uploader1->save($path1, $newName1);

//                    $changed = true;
                }
                /************************ Banner ******************************/


                if(trim(strip_tags($postData['description'])) != trim(strip_tags($customerData->getSupplierDescription())) || $forceChange) { //|| $forceChange
                    $customerData->setSupplierDescriptionNew(strip_tags($postData['description'], '<ol><li><b><span><a><i><u><p><br><h1><h2><h3><h4><h5><div>'));

                   /* if(!$changed) {
                        $customerData->setSupplierNameNew($postData['name']);
                    }*/
                    $changed = true;
                }
            
                if(isset($postData['profile_enabled'])) {
                    $customerData->setSupplierProfileVisible(1);
                } else {
                    $customerData->setSupplierProfileVisible(0);
                }

                if($customerData->hasDataChanges() && $changed) {
                    $customerData->setData('rejected_notfication_seen', 2);
                    $waitingForApproval = true;

                }

                $customFieldsCollection = Mage::getModel('marketplace/fields')->getCollection();
                $customFieldsValues = array();
                $oldCustomFieldsValues = unserialize($customerData->getCustomFieldsValues());

                foreach($customFieldsCollection AS $field) {
                    if(isset($postData[$field->getName()])) {
                        if($field->getIsRequired() && $postData[$field->getName()] == '') {
                            throw new Exception("Field ".$field->getName()." is required");
                        }
                        
                        if($field->getType() == 'date' && !strtotime($postData[$field->getName()])) {
                            throw new Exception("Field ".$field->getName()." is not valid date");
                        }

                        $oldValue = $this->_findValue($field->getName(), $oldCustomFieldsValues);

                        if($oldValue != $postData[$field->getName()] && $field->getMustBeApproved()) {
                            $customerData->setData('rejected_notfication_seen', 2);
                            $waitingForApproval = true;
                        }

                        $customFieldsValues[] = array('name' => $field->getName(), 'value' => $postData[$field->getName()]);
                    }

                    if(isset($postData[$field->getUrl()])) {
                        if($field->getIsRequired() && $postData[$field->getUrl()] == '') {
                            throw new Exception("Field ".$field->getUrl()." is required");
                        }
                        
                        if($field->getType() == 'date' && !strtotime($postData[$field->getUrl()])) {
                            throw new Exception("Field ".$field->getUrl()." is not valid date");
                        }

                        $oldValue = $this->_findValue($field->getUrl(), $oldCustomFieldsValues);

                        if($oldValue != $postData[$field->getUrl()] && $field->getMustBeApproved()) {
                            $customerData->setData('rejected_notfication_seen', 2);
                            $waitingForApproval = true;
                        }

                        $customFieldsValues[] = array('name' => $field->getUrl(), 'value' => $postData[$field->getUrl()]);
                    }
                    else
                    {

                    }
                }
               
                if($waitingForApproval) {
                    
                    $customerData->setSupplierNameNew($postData['name']);
                    $customerData->setSupplierUrlNew($postData['url']);
                    $customerData->setSupplierDescriptionNew(trim(strip_tags($postData['description'], '<ol><li><b><span><a><i><u><p><br><h1><h2><h3><h4><h5><div>')));
                    $customerData->setNewCustomFieldsValues(serialize($customFieldsValues));
                } else {

                    
                    $customerData->setCustomFieldsValues(serialize($customFieldsValues));
                }

                $customerData->setCompanyName($postData['company_name']);
                $customerData->setAuthRepresentative($postData['auth_representative']);
                $customerData->setContactNumber($postData['country_code']."-".$postData['contact_number']);
                $customerData->setBankName($postData['bank_name']);
                $customerData->setAccountNo($postData['account_no']);
                $customerData->setIfscCode($postData['ifsc_code']);
                $customerData->setCompanyAddress($postData['company_address']);
                $customerData->setCompanyCity($postData['company_city']);
                $customerData->setCompanyState($postData['company_state']);
                $customerData->setCompanyCountry($postData['company_country']);
                $customerData->setShipto($postData['shipto']);
                $customerData->setTradeLicenceNo($postData['trade_licence_no']);
                
                
                

            
            } elseif(isset($postData['clear'])) {
                $customerData->setSupplierNameNew(null);
                $customerData->setSupplierDescriptionNew(null);
                $customerData->setNewCustomFieldsValues(null);
            }

            $transaction->addObject($customerData);
            $transaction->save();
    
            if($waitingForApproval) {   
                Mage::helper('marketplace/email')->notifyAdminOnProfileChange($customerData); 
                Mage::getSingleton('core/session')->addSuccess($this->_getHelper()->__('Profile was changed and waiting for admin approval'));
            } else {
                Mage::getSingleton('core/session')->addSuccess($this->_getHelper()->__('Your profile was changed'));
            }

            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl() . 'marketplace/settings/profile/');
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl() . 'marketplace/settings/profile/');
            Mage::log($e->getMessage());
        }
    }

    private function _parseUploadedCsv($index, $methodId) {
        $changed = false;
        $parsedData = array();
        if (isset($_FILES["table_rate_file"])) {
            $changed = true;
                if(file_exists($_FILES["table_rate_file"]["tmp_name"][$index])) {
                if (($handle = fopen($_FILES["table_rate_file"]["tmp_name"][$index], "r")) !== FALSE) {
                    while (($row = fgetcsv($handle)) !== FALSE)
                    {
                        $parsedData[] = $row;
                    }
                    fclose($handle);
                } else {
                    throw new ErrorException('Cannot handle uploaded CSV');
                }
            }
        }
        if(!$parsedData) return;

        if($parsedData[0][0] == 'Country') {
            unset($parsedData[0]);
        }

        if($changed) {
            $supplierRate = Mage::getModel("marketplace/rates")
                ->load(Mage::helper('marketplace')->getSupplierId(), 'supplier_id');

            if(!$supplierRate->getId()) {
                $supplierRate->setSupplierId(Mage::helper('marketplace')->getSupplierId());
            }

            $supplierRate->setRateData(serialize($parsedData));
            $supplierRate->setMethodId($methodId);
            $supplierRate->save();
        }
    }

    private function _findValue($name, $data) {
        if(!is_array($data)) return false;
        
        foreach($data AS $value) {
            if($value['name'] == $name) {
                return $value['value'];
            }
        }

        return false;
    }
}
