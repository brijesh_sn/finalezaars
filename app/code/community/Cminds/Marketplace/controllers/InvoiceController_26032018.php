<?php

class Cminds_Marketplace_InvoiceController extends Cminds_Marketplace_Controller_Action {
    public function preDispatch() {
        parent::preDispatch();
        $hasAccess = $this->_getHelper()->hasAccess();

        if(!$hasAccess) {
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::helper('customer')->getLoginUrl());
        }
    }
    public function createAction() {
        $id = $this->getRequest()->getParam('id');
        Mage::register('order_id', $id);
        $this->_renderBlocks();
    }
    public function viewAction() {
        $id = $this->getRequest()->getParam('id');
        Mage::register('shipment_id', $id);
        $this->_renderBlocks();
    }
    public function saveAction() {
        $post = $this->_request->getPost();
        Mage::register("vendor_creation", true);

        try {
            $transaction = Mage::getModel('core/resource_transaction');
            $order = Mage::getModel('sales/order')->load($post['order_id']);

            foreach($post['product'] AS $product_id => $qty) {

                if($qty <= 0) {
                    unset($post['product'][$product_id]);
                }
                $itemModel = Mage::getModel('sales/order_item')->load($product_id);

               // echo '<pre>';
               // print_r($itemModel);exit;

                if(!$itemModel->getProductId() || !Mage::helper('marketplace')->isOwner($itemModel->getProductId())) {
                    throw new Exception('You cannot ship non-owning products');
                }

                if($itemModel->getQtyOrdered() < ($itemModel->getQtyInvoiced() + intval($qty))) {
                    throw new Exception('You cannot ship more products than it was ordered');
                }

            }

            if($order->getState() == 'canceled') {
                throw new Exception('You cannot create shipment for canceled order');
            }

            $invoice = Mage::getModel('marketplace/invoice', $order)->prepareInvoice($post['product']);

            //$invoice->setGrandTotal($order->getGrandTotal()); // Sachin - 07-03-2018

            $invoice->register()
                ->save()
                ->sendEmail((isset($post['notify_customer']) && $post['notify_customer'] == '1'))
                ->setEmailSent(false);

            $invoice->getOrder()->setIsInProcess(true);

            foreach($invoice->getAllItems() AS $item) {
                $orderItem = Mage::getModel('sales/order_item')->load($item->getOrderItemId());
                //echo '<pre>';
                //print_r($orderItem);exit;
                $orderItem->setQtyInvoiced($item->getQty() + $orderItem->getQtyInvoiced());
            }
            //exit;

            $loggedUser = Mage::getSingleton('customer/session', array('name' => 'frontend') );
            $customer = $loggedUser->getCustomer();

            $comment = $customer->getFirstname() .' '.$customer->getLastname() . ' (#'.$customer->getId().') created invoice for ' . count($post['product']) . ' item(s)';

            $order->addStatusHistoryComment($comment);

            $fullyInvoiced = true;

            foreach ($order->getAllItems() as $item) {
                if ($item->getQtyToInvoiced() > 0) {
                    $fullyInvoiced = false;
                }
            }

            if($fullyInvoiced) {
                if($order->getState() != Mage_Sales_Model_Order::STATE_PROCESSING) {
                    $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                    $order->setState($state, true);

                } elseif($order->getState() == Mage_Sales_Model_Order::STATE_PROCESSING) {
//                    $state = Mage_Sales_Model_Order::STATE_COMPLETE;
                }

            }

            $transaction->addObject($invoice);
            $transaction->addObject($orderItem);
            $transaction->addObject($order);

            $transaction->save();
            Mage::getSingleton('core/session')->addSuccess('Invoice for order #'.$order->getIncrementId().' was created');
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('*/order/view/', array('id' => $post['order_id'], 'tab' => 'invoice')));
        } catch (Exception $e) {
            if (null !== $order->getIncrementId()) {
                $order->addStatusHistoryComment('Failed to create invoice - '. $e->getMessage())
                    ->save();
            }
            Mage::getSingleton('core/session')->addError($e->getMessage());
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('*/invoice/create/', array('id' => $post['order_id'], 'tab' => 'invoice')));
        }
    }

    public function printAction()
    {
        if ($invoiceId = $this->getRequest()->getParam('id')) {
            if ($invoice = Mage::getModel('sales/order_invoice')->load($invoiceId)) {


            /************** Mayur panchal 21-03-2018 ********************/

            $invoiceitems =  $invoice;      
       
            $_items_product = $invoiceitems->getAllItems();

            $count = count($_items_product);

            $count_p = 0;

            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
           
            foreach ($_items_product as $_item_p) {
                
                $pid = $_item_p->getProductId();

                $Qnt = $_item_p->getQty();

                $_productsss = Mage::getModel('catalog/product')->load($pid);

                $method = $_productsss->getShippingMethod();     

                $sql  = "SELECT * FROM `marketplace_supplier_shipping_methods` WHERE `supplier_id`= '".$_productsss->getCreatorId()."'  AND `name` = '".$method."'";

                $rowssss = $connection->fetchAll($sql);                

                if(count($rowssss) > 0){

                if($rowssss[0]['flat_rate_available'] == true){
                    $shipping_price[$pid] = $rowssss[0]['flat_rate_fee'] * $Qnt[1];
                    $shipping_baseprice[$pid] = $rowssss[0]['flat_rate_fee'];
                }
                if($rowssss[0]['table_rate_available'] == true){
                    $shipping_price[$pid] = $rowssss[0]['table_rate_fee'] * $Qnt[1];
                    $shipping_baseprice[$pid] = $rowssss[0]['table_rate_fee'];
                }
                }else{
                    $shipping_price[$pid] = '';
                    $shipping_baseprice[$pid] = '';
                }
                if($count > 1){
                     $shipprice += $shipping_baseprice[$pid];

                }else{

                    $shipprice = $shipping_baseprice[$pid];
                }
            }
            $baseshipprice = $shipprice;
            $shipprice = number_format(round($shipprice,2),3);
            $invoice->setShippingAmount($shipprice);             

            /*************** end 21-03-2018 *********/
            $getSubtotal = $invoice->getSubtotal();
            $setSubtotal = number_format(round($getSubtotal,2),3);
            $setSubtotal = $invoice->setSubtotal($setSubtotal);

            $total = $getSubtotal + $baseshipprice;  // Custom total
            
            $total = number_format(round($total,2),3);

            $invoice->setGrandTotal($total); // Custom total
         
                     $pdf = Mage::getModel('sales/order_pdf_invoice')->setIsSupplier(true)->getPdf(array($invoice));
                $this->_prepareDownloadResponse('invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').
                    '.pdf', $pdf->render(), 'application/pdf');
            }
        }
        else {
            $this->_forward('noRoute');
        }
    }
}
