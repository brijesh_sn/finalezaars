<?php 
class Cminds_Marketplace_Model_Observer extends Mage_Core_Model_Abstract
{
    public function onOrderPlaced($observer) {
        if(!Mage::helper('supplierfrontendproductuploader')->isEnabled()) {
            return;
        }

        $orderId = $observer->getEvent()->getOrder()->getId();
        $order = Mage::getModel('sales/order')->load($orderId);
        $items = $order->getAllItems();
        $data = array();
        $datas = array();

        foreach ($items as $item)
        {
            $data = array();
            $product = Mage::getModel('catalog/product')->load($item->getProductId());

            if($product->getData('creator_id') != NULL && $product->getData('creator_id') != 0) {
                $i = Mage::getModel('sales/order_item')->load($item->getId());

                if($i) {
                    $vendorIncomes = Mage::helper('marketplace/profits')->getVendorIncome($product, $item->getPrice());

                    if($vendorIncomes) {
                        $i->setVendorFee($vendorIncomes['percentage']);
                        $i->setVendorIncome($vendorIncomes['income']);
                        $i->save();
                    }
                }
            }
        }
        $estimateData = Mage::getSingleton('checkout/session')->getEstimateData();
        $supplierShippingMethodDescription = array();
        if($estimateData){
            foreach($estimateData as $itemId => $price) {
                if(!is_numeric($itemId)) continue;
                $productId = Mage::getModel('sales/quote_item')->load($itemId, 'item_id')
                    ->getProductId();
                if($productId) {
                    $supplierShippingMethodDescription[$productId] = $price;
                }
            }
            $order->setSupplierShippingMethod(serialize($supplierShippingMethodDescription));
            $order->save();
        }

    }

    public function onOrderSave($observer) {

        if(!Mage::helper('supplierfrontendproductuploader')->isEnabled()) {
            return;
        }
        $order = $observer->getOrder();
        if($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE){
            $orderId = $order->getId();
            $order = Mage::getModel('sales/order')->load($orderId);
            $items = $order->getAllItems();

            if(!$order->getCustomerId()) return;

            foreach ($items as $item) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());

                if($product->getData('creator_id') != NULL) {
                    if($product->getData('creator_id') == $order->getCustomerId()) continue;

                    $s = Mage::getModel('marketplace/torate')->getCollection()
                        ->addFieldToFilter('supplier_id', $product->getData('creator_id'))
                        ->addFieldToFilter('customer_id', $order->getCustomerId());

                    if($s->count() <= 0) {
                        Mage::getModel('marketplace/torate')
                            ->setData('supplier_id', $product->getData('creator_id'))
                            ->setData('order_id', $orderId)
                            ->setData('product_id', $item->getProductId())
                            ->setData('customer_id', $order->getCustomerId()) ->save();
                    }
                }
            }
        }
    }

    public function navLoad($observer) {
        $event = $observer->getEvent();
        $items = $event->getItems();
        if(Mage::helper('marketplace')->csvImportEnabled()) {
            /*$items['IMPORT'] =  [
                'label'     => 'Import',
                'url'   	=> null,
                'parent'    => 'PRODUCT',
                'action_names' => [
                    'cminds_marketplace_import_products',
                ],
                'sort'     => 1.5
            ];*/
            $items['IMPORT_PRODUCTS'] =  [
                'label'     => 'Import Products',
                'url'   	=> 'marketplace/import/products',
                'parent'    => 'PRODUCT',
                'action_names' => [
                    'cminds_marketplace_import_products'
                ],
                'sort'     => 3
            ];
        }

        $items['ORDERS'] =  [
            'label'     => 'Orders',
            'url'   	=> 'marketplace/order',
            'parent'    => null,
            'action_names' => [
                'cminds_marketplace_order_index',
                'cminds_marketplace_order_view',
                'cminds_marketplace_shipment_create',
                'cminds_marketplace_invoice_create',
                'cminds_marketplace_shipment_view',
                'cminds_marketplace_order_importshipping',
                'cminds_marketplace_rma_supplier_list'
            ],
            'sort'     => 2.5
        ];

        $items['ORDER_LIST'] =  [
            'label'     => 'Order List',
            'url'   	=> 'marketplace/order',
            'parent'    => 'ORDERS',
            'action_names' => [
                    'cminds_marketplace_order_index',
                    'cminds_marketplace_order_view'
                ],
            'sort'     => 0
        ];

        /*$items['IMPORT_SIPMENTS'] =  [
            'label'     => 'Import Shipping',
            'url'   	=> 'marketplace/order/importshipping',
            'parent'    => 'ORDERS',
            'action_names' => [
                    'cminds_marketplace_order_importshipping'
                ],
            'sort'     => 1
        ];*/

         if(Mage::helper('marketplace')->supplierPagesEnabled()) {
             $items['SUPPLIER_PAGE'] = [
                 'label'     => 'My Profile Page',
                 'url'   	=> 'marketplace/settings/profile',
                 'parent'    => 'SETTINGS',
                 'sort' => -1
             ];

             $items['SETTINGS']['action_names'] = array_merge($items['SETTINGS']['action_names'],['cminds_marketplace_settings_profile']);

         }
         if(Mage::getStoreConfig('marketplace_configuration/presentation/change_shipping_costs')) {
            $items['SHIPPING_METHODS'] = [
                'label' => 'Shipping Methods',
                'url' => 'marketplace/settings/shipping',
                'parent' => 'SETTINGS',
                'sort' => 3
            ];
            $items['SETTINGS']['action_names'] = array_merge($items['SETTINGS']['action_names'],['cminds_marketplace_settings_shipping']);
         }

        $items['REPORTS_ORDERS'] = [
            'label' => 'Orders',
            'url' => 'marketplace/reports/orders',
            'parent' => 'REPORTS',
            'sort' => -1,

        ];

         $items['REPORTS_PRODUCTS'] = [
            'label' => 'Products',
            'url' => null,
            'parent' => 'REPORTS',
            'sort' => 1,
            'fix_label' => true
         ];

        $items['REPORTS_BESTSELLERS'] = [
            'label' => 'Bestsellers',
            'url' => 'marketplace/reports/bestsellers',
            'parent' => 'REPORTS',
            'sort' => 2,
            'fix_label_children' => true
        ];

        $items['REPORTS_ORDERED_ITEMS'] = [
            'label'     => 'Ordered Items',
            'url'   	=> 'supplier/product/ordered',
            'parent'    => 'REPORTS',
            'sort'      => 3,
            'fix_label_children' => true
        ];

        $items['REPORTS_MOST_VIEWED'] = [
            'label'     => 'Most Viewed',
            'url'   	=> 'marketplace/reports/mostViewed',
            'parent'    => 'REPORTS',
            'sort'      => 4,
            'fix_label_children' => true
        ];

        $items['REPORTS_LOW_STACK'] = [
            'label'     => 'Low stack',
            'url'   	=> 'marketplace/reports/lowStock',
            'parent'    => 'REPORTS',
            'sort'      => 5,
            'fix_label_children' => true
        ];


        $items['REPORTS']['action_names'] = array_merge($items['REPORTS']['action_names'],
            [
                'cminds_marketplace_reports_orders',
                'cminds_supplierfrontendproductuploader_product_ordered',
                'cminds_marketplace_reports_bestsellers',
                'cminds_marketplace_reports_mostViewed',
                'cminds_marketplace_reports_lowStock',
            ]);

        if(Mage::helper('marketplace')->supplierPagesEnabled()) {
            $customerData = Mage::getSingleton('customer/session')->getCustomer();

            $items['MY_SUPPLIER_PAGE'] = [
                'label'     => 'Shop View',
                'url'   	=> Mage::helper('marketplace')->getSupplierRawPageUrl($customerData->getId()),
                'parent'    => null,
                'sort'      => 4.5
            ];
        }



        $observer->getEvent()->setItems($items);
    }

    public function chooseTemplate($t) {
        $template = $t->getBlock()->getTemplate();
        $showIn = Mage::getStoreConfig('marketplace_configuration/presentation/show_sold_by_in');
        /**
         * marketplace/model/config/soldby.php
         */
        $array = explode(",",$showIn);

        if($template == 'checkout/cart/item/default.phtml' && in_array('checkout_cart', $array)) {
            $t->getBlock()->setTemplate("marketplace/checkout/cart/item/default.phtml");
        }
        elseif ($template == 'checkout/onepage/review/item.phtml' && in_array('checkout', $array)) {
            $t->getBlock()->setTemplate("marketplace/checkout/onepage/review/item.phtml");
        }
        elseif($template == 'checkout/cart/minicart/default.phtml' && in_array('checkout_minicart', $array)) {
            $t->getBlock()->setTemplate("marketplace/checkout/cart/minicart/default.phtml");
        }
        elseif($template == 'sales/order/items/renderer/default.phtml' && in_array('order_view', $array)) {
            $t->getBlock()->setTemplate("marketplace/sales/order/items/renderer/default.phtml");
        }
        elseif($template == 'email/order/items/shipment/default.phtml' && in_array('emails', $array)) {
            $t->getBlock()->setTemplate("marketplace/email/order/items/shipment/default.phtml");
        }
        elseif($template == 'email/order/items/order/default.phtml' && in_array('emails', $array)) {
            $t->getBlock()->setTemplate("marketplace/email/order/items/order/default.phtml");
        }
        elseif($template == 'email/order/items/invoice/default.phtml' && in_array('emails', $array)) {
            $t->getBlock()->setTemplate("marketplace/email/order/items/invoice/default.phtml");
        }
        elseif($template == 'email/order/items/creditmemo/default.phtml' && in_array('emails', $array)) {
            $t->getBlock()->setTemplate("marketplace/email/creditmemo/items/invoice/default.phtml");
        }
    }

    public function onSaveShippingMethod($observer) {
        $request = $observer->getControllerAction()->getRequest();
        $postData = $request->getPost();

        if(
        (isset($postData['shipping_method']) && $postData['shipping_method'] == 'marketplace_shipping_marketplace_shipping') ||
        (isset($postData['estimate_method']) && $postData['estimate_method'] == 'marketplace_estimated_time_marketplace_estimated_time')
        ) {
            Mage::getSingleton('checkout/session')->setEstimateData($postData['estimatetime']);
            $quote = Mage::getModel('checkout/session')->getQuote();
            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->getShippingAddress()->collectShippingRates()->save();
        }
    }

    public function onOrderPlaceMethod($observer) {
        foreach(Mage::getSingleton('checkout/session')->getEstimateData() AS $quote_item_id => $price) {
            $i = Mage::getModel('sales/order_item')->load($quote_item_id, 'quote_item_id');
            $i->setShippingPrice($price);
            $i->save();
        }
        Mage::getSingleton('checkout/session')->unsEstimateData();
    }
}