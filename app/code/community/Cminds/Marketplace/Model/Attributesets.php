<?php
class Cminds_Marketplace_Model_Attributesets extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('marketplace/attributesets');
    }
}