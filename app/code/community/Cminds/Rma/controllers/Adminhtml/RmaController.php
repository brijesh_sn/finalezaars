<?php

class Cminds_Rma_Adminhtml_RmaController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Return Merchandise Authorization'));
        $this->loadLayout();
        $this->_setActiveMenu('rma');
        $this->_addContent($this->getLayout()->createBlock('cminds_rma/adminhtml_rma_list'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $rma = Mage::getModel('cminds_rma/rma');
        if ($rmaId = $this->getRequest()->getParam('id', false)) {
            $rma->load($rmaId);

            if (!$rma->getId()) {
                $this->_getSession()->addError(
                    $this->__('This RMA no longer exists.')
                );

                return $this->_redirect(
                    '*/*/list'
                );
            }
        }

        Mage::register('rma_data', $rma);

        $editBlock = $this->getLayout()->createBlock(
            'cminds_rma/adminhtml_rma_edit'
        );

        $this->loadLayout()
            ->_addContent($editBlock)
            ->_addLeft($this->getLayout()->createBlock('cminds_rma/adminhtml_rma_edit_tabs'))
            ->renderLayout();
    }

    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $rma = Mage::getModel('cminds_rma/rma')->load($this->getRequest()->getParam('id'));

                if (!$rma->getId()) {
                    throw new Exception($this->__("RMA does not exists !"));
                }

                $rma->setStatusId($postData['status_id']);
                $rma->setIsPackageOpened($postData['is_package_opened']);
                $rma->setReasonId($postData['reason_id']);
                $rma->setAdditionalInformation($postData['additional_information']);

                $rma->save();

                if ($postData['comment'] && $rma->getId()) {
                    $comment = Mage::getModel('cminds_rma/rma_comment');
                    $comment->setRmaId($rma->getId());
                    $comment->setCommentBody($postData['comment']);
                    $comment->save();
                }

                if ($postData['note'] && $rma->getId()) {
                    $comment = Mage::getModel('cminds_rma/rma_comment');
                    $comment->setRmaId($rma->getId());
                    $comment->setCommentBody($postData['note']);
                    $comment->setIsCustomerNotified((int)isset($postData['notify_customer']));
                    $comment->save();
                }

                $this->_getSession()->addSuccess(
                    $this->__('RMA has been saved.')
                );

                return $this->_redirect(
                    '*/*/'
                );
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cminds_rma')->__('No data found to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $rma = Mage::getModel('cminds_rma/rma')->load($id);
                $savedRma = $rma;
                $rma->delete();

                $this->_getSession()->addSuccess(
                    $this->__('RMA #%s has been removed.', $savedRma->getId())
                );

                return $this->_redirect(
                    '*/*/'
                );
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cminds_rma')->__('Missing ID'));
        $this->_redirect('*/*/');
    }

    public function creditmemoAction()
    {
        try {
            if (!$id = $this->getRequest()->getParam('id', null)) {
                throw new Exception("RMA with this ID");
            }
            $rma = Mage::getModel('cminds_rma/rma')->load($id);
            $order = $rma->getOrder();

            $rmaItems = $rma->getAllItems();

            $creditmemoData = array(
                'qtys' => array(),
                'shipping_amount' => null,
                'adjustment_positive' => '0',
                'adjustment_negative' => null);

            foreach ($rmaItems AS $item) {
                $creditmemoData['qtys'][$item->getItemId()] = $item->getQty();
            }

            $comment = 'Comment for Credit Memo';

            $notifyCustomer = true;
            $includeComment = false;
            $refundToStoreCreditAmount = '1';

            if ($order->getId() && $order->canCreditmemo()) {
                $service = Mage::getModel('sales/service_order', $order);
                $data = isset($data) ? $data : array();

                $creditmemo = $service->prepareCreditmemo($creditmemoData);
                if ($refundToStoreCreditAmount) {
                    if ($order->getCustomerIsGuest()) {
                    }
                    $refundToStoreCreditAmount = max(
                        0,
                        min($creditmemo->getBaseCustomerBalanceReturnMax(), $refundToStoreCreditAmount)
                    );
                    if ($refundToStoreCreditAmount) {
                        $refundToStoreCreditAmount = $creditmemo->getStore()->roundPrice($refundToStoreCreditAmount);
                        $creditmemo->setBaseCustomerBalanceTotalRefunded($refundToStoreCreditAmount);
                        $refundToStoreCreditAmount = $creditmemo->getStore()->roundPrice(
                            $refundToStoreCreditAmount * $order->getStoreToOrderRate()
                        );
                        $creditmemo->setBsCustomerBalTotalRefunded($refundToStoreCreditAmount);
                        $creditmemo->setCustomerBalanceRefundFlag(true);
                    }
                }
                $creditmemo->setPaymentRefundDisallowed(true)->register();

                if (!empty($comment)) {
                    $creditmemo->addComment($comment, $notifyCustomer);
                }

                Mage::getModel('core/resource_transaction')
                    ->addObject($creditmemo)
                    ->addObject($order)
                    ->save();
                $creditmemo->sendEmail($notifyCustomer, ($includeComment ? $comment : ''));
                $rma->close();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('cminds_rma')->__('Credit Memo has been created'));
            } else {
                MAge::throwException($this->__("Credit Memo cannot be created"));
            }

            return $this->_redirect(
                '*/*/'
            );
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }
    }
}
