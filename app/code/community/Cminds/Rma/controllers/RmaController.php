<?php

class Cminds_Rma_RmaController extends Mage_Core_Controller_Front_Action {
   public function listAction() {
      $isLoggedIn = Mage::getSingleton('customer/session')->isLoggedIn();
        if($isLoggedIn) :
            
            $customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
            if($customerGroupId == 7) :
                $this->_redirect('supplier');
                return true;
            endif;
        endif;
      $this->loadLayout();
      $this->getLayout()->getBlock('head')->setTitle($this->__('My RMA Requests'));
      $this->renderLayout();
   }

   public function createAction() {
      $isLoggedIn = Mage::getSingleton('customer/session')->isLoggedIn();
        if($isLoggedIn) :
            
            $customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
            if($customerGroupId == 7) :
                $this->_redirect('supplier');
                return true;
            endif;
        endif;
      $this->loadLayout();
      $this->getLayout()->getBlock('head')->setTitle($this->__('Create RMA Request'));
      $this->renderLayout();
   }

   public function getProductCollectionAction() {
      
      $orderId = $this->getRequest()->getParam('order_id', null);

      if(!$orderId) {
         $this->_trigger404();
         return;
      }

      Mage::register('marketplace_rma_order', $orderId);

      echo $this->getLayout()->createBlock('cminds_rma/rma_create_products')->toHtml();
   }

   public function viewAction() {
      $rmaId = $this->getRequest()->getParam('rma_id', null);

      Mage::register('marketplace_rma', $rmaId);
      $rma = Mage::getModel('cminds_rma/rma')->load($rmaId);
      if($rma->getCustomerId() != Mage::getSingleton('customer/session')->getCustomer()->getId()) {
         $this->_trigger404();
         return;
      }
      $this->loadLayout();
      $this->getLayout()->getBlock('head')->setTitle($this->__('RMA #%s - %s', $rma->getAutoincrementId(), $rma->getStatusLabel()));
      $this->renderLayout();
   }

   public function formPostAction() {
      $postData = $this->getRequest()->getPost();

      if(!isset($postData['order_id'])) $this->_forceError("No Order Selected");

      $order = Mage::getModel('sales/order')->load($postData['order_id']);

      if($order->getCustomerId() != Mage::getSingleton('customer/session')->getCustomer()->getId()) {
         $this->_trigger404();
         return;
      }

      if(!isset($postData['order_id'])) $this->_forceError("No Order Selected");

      try {
         Mage::getModel("cminds_rma/rma")->setData($postData)->save();
         $this->getResponse()->setRedirect(Mage::getUrl("*/*/list"));
      } catch(Exception $e) {
         Mage::getSingleton('core/session')->addError($e->getMessage());
      }
   }

   public function cancelAction() {
      $params = $this->getRequest()->getParams();

      if(!isset($params['rma_id'])) $this->_forceError("No RMA Selected");

      $order = Mage::getModel('cminds_rma/rma')->load($params['rma_id']);

      if($order->getCustomerId() != Mage::getSingleton('customer/session')->getCustomer()->getId()) {
         $this->_trigger404();
         return;
         }

      try {
         $rma = Mage::getModel("cminds_rma/rma")->load($params['rma_id']);
         $rma->setData("status_id", Cminds_Rma_Model_Rma::DEFAULT_CANCELED_ID);
         $rma->save();

         $this->getResponse()->setRedirect(Mage::getUrl("*/*/list"));
      } catch(Exception $e) {
         Mage::getSingleton('core/session')->addError($e->getMessage());
      }
   }

   private function _forceError($msg) {
      Mage::getSingleton('core/session')->addError($this->__($msg));
      $this->getResponse()->setRedirect(Mage::getUrl("*/*/create"));
      exit;
   }

   protected function _trigger404() {
   		$this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
		$this->getResponse()->setHeader('Status','404 File not found');
		$pageId = Mage::getStoreConfig('web/default/cms_no_route');
		if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
		    $this->_forward('defaultNoRoute');
		}
   }
}
