<?php

class Cminds_SupplierSubscriptions_Model_Observer extends Mage_Core_Model_Abstract
{

    public function onCustomerSaveBefore(Varien_Event_Observer $observer)
    {
        /**
         * get params from edit form in admin panel
         */
        if(Mage::helper('suppliersubscriptions')->isEnabled()) {
            $customer = $observer->getEvent()->getCustomer();
            $currentPlan = Mage::app()->getRequest()->getParam('current_plan');
            $planFromDate = Mage::app()->getRequest()->getParam('plan_from_date');
            $planToDate = Mage::app()->getRequest()->getParam('plan_to_date');

            if (!empty($currentPlan)) {
                $customer->setCurrentPlan($currentPlan);
                $customer->setPlanFromDate($planFromDate);
                $customer->setPlanToDate($planToDate);
            }
        }

    }

    /**
     * Check is supplier is approved.
     *
     * Event: customer_login
     * @param Varien_Event_Observer $observer
     */
    public function onCustomerLogin(Varien_Event_Observer $observer)
    {
        if(Mage::helper('suppliersubscriptions')->isEnabled()) {
            $customer = $observer->getEvent()->getCustomer();
            $customerId = $customer->getId();
            $customer = Mage::getModel('customer/customer')->load($customerId);
            $defaultPlan = Mage::getStoreConfig('suppliersubscriptions_catalog/general/default_plan');

            if (!Mage::helper('suppliersubscriptions')->isSupplier($customer->getId())) {
                return;
            }

            if(!$customer->getCurrentPlan()) {
                $planToDate = date('Y-m-d H:i:s', strtotime("+9 months"));

                $customer->setPlanFromDate(date('Y-m-d H:i:s'))
                    ->setPlanToDate($planToDate)
                    ->setCurrentPlan($defaultPlan)
                    ->save();
            }
        }

    }

    public function onOrderPlaceAfter(Varien_Event_Observer $observer)
    {
        if(Mage::helper('suppliersubscriptions')->isEnabled()) {
            /* @var $order Mage_Sales_Model_Order */
            $order = $observer->getEvent()->getOrder();
            $productPlan = array();

            $plansCollection = Mage::getModel('suppliersubscriptions/plan')->getCollection();
            foreach ($plansCollection as $plan) {
                /* @var $plan Cminds_SupplierRegistrationExtended_Model_Plan */
                $productPlan[$plan->getProductId()] = $plan;
            }

            /**
             * Return if there is no plans in system.
             */
            if (empty($productPlan)) {
                return;
            }

            foreach ($order->getItemsCollection() as $item) {
                /* @var $item Mage_Sales_Model_Order_Item */
                if (isset($productPlan[$item->getProductId()])) {
                    $qty = (int)$item->getQtyOrdered();
                    $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
                    /* @var $currentPlan Cminds_SupplierRegistrationExtended_Model_Plan */
                    $currentPlan = $productPlan[$item->getProductId()];
                    $time = time();
                    if ((int)$customer->getCurrentPlan() == (int)$currentPlan->getId()) {
                        $time = strtotime($customer->getPlanToDate());
                    }
                    $planToDate = date('Y-m-d H:i:s',strtotime("+{$qty} month", $time));


                    $customer->setPlanFromDate(date('Y-m-d H:i:s'))
                        ->setPlanToDate($planToDate)
                        ->setCurrentPlan($currentPlan->getId())
                        ->save();
                    return;
                }
            }
        }

    }

    public function chosenTemplate()
    {

        if(Mage::helper('suppliersubscriptions')->isEnabled()) {
            $layout = Mage::getSingleton('core/layout');
            $form = $layout->getBlock('cminds_supplierfrontendproductuploader.product_create');

            if($form && $form->getTemplate() == 'supplierfrontendproductuploader/product/edit/form.phtml') {
                $form->setTemplate('/suppliersubscriptions/product/edit/form.phtml');
            }
            elseif($form && $form->getTemplate() == 'supplierfrontendproductuploader/product/create/form.phtml') {
                $form->setTemplate('/suppliersubscriptions/product/create/form.phtml');
            }
        }

    }

    public function navLoad($observer) {
        $event = $observer->getEvent();
        $items = $event->getItems();
        if(Mage::helper('suppliersubscriptions')->isEnabled()) {
            $items['RENEW'] =  [
                'label'     => 'Renew',
                'url'   	=> 'supplier/plan/renew',
                'parent'    => 'SETTINGS',
                'action_names' => [
                    'cminds_supplierfrontendproductuploader_plan_renew',
                ],
                'sort'     => 5
            ];

            $items['UPGRADE'] =  [
                'label'     => 'Upgrade',
                'url'   	=> 'supplier/plan/list',
                'parent'    => 'SETTINGS',
                'action_names' => [
                    'cminds_supplierfrontendproductuploader_plan_list',
                ],
                'sort'     => 6
            ];
        }
        $observer->getEvent()->setItems($items);
    }

    public function validateSubscriptionPlan(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $route = $event->getControllerAction()->getRequest()->getRouteName();
        $actionName = $event->getControllerAction()->getRequest()->getActionName();

        if($route != 'cminds_supplierfrontendproductuploader' ) {
            return $this;
        }

        if(!in_array($actionName, array('create', 'chooseType', 'clone'))) {
            return $this;
        }

        $helper = Mage::helper('suppliersubscriptions');

        if(!$helper->isEnabled()) {
            return $this;
        }

        $planProductCount = $helper->getSupplierPlanProducts();
        $productCount = $helper->getSupplierProductsCount();
        $planActive = $helper->isSupplierPlanActive();

        if ($planActive && $planProductCount > $productCount) {
            return $this;
        }
        if (!$planActive) {
            $message = $helper->__("Your plan is deactivated.");
        } else {
            $message = $helper->__("The products amount limit has been reached.");
        }

        Mage::getSingleton('core/session')->addError($message);
        Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('supplier/'));
        Mage::app()->getResponse()->sendResponse();
        exit;
    }

}


