<?php
class Cminds_Supplierfrontendproductuploader_Block_Navbar extends Mage_Core_Block_Template {
	private $_markedProductIds = null;

	public $nav_items = [
		'HOME' => [
			'label'     		=> 'Home',
			'url'   			=> 'supplier',
			'parent'    		=> null,
			'action_names' => [
				'cminds_supplierfrontendproductuploader_index_index'
			],
			'sort' => 0
		],
		'PRODUCT' => [
			'label'     		=> 'Product',
			'url'   			=> null,
			'class' =>'product',
			'parent'    		=> null,
			'action_names' => [
				'cminds_supplierfrontendproductuploader_product_chooseType',
				'cminds_supplierfrontendproductuploader_product_create',
				'cminds_marketplace_import_products',
				'cminds_supplierfrontendproductuploader_product_list',
				'cminds_supplierfrontendproductuploader_product_edit',
				'cminds_supplierfrontendproductuploader_product_clone',
			],
			'sort' => 2
		],
		'ADD_PRODUCT' => [
			'label'     => 'Add a Product',
			'url'   	=> 'supplier/product/chooseType',
			'parent'    => 'PRODUCT',
			'action_names' => [
				'cminds_supplierfrontendproductuploader_product_chooseType',
				',cminds_supplierfrontendproductuploader_product_create',
			],
			'sort' => 1
		],
		'PRODUCT_LIST' => [
			'label'     => 'Product List',
			'url'   	=> 'supplier/product/list',
			'parent'    => 'PRODUCT',
			'action_names' => [
				'cminds_supplierfrontendproductuploader_product_list',
				'cminds_supplierfrontendproductuploader_product_edit',
				'cminds_supplierfrontendproductuploader_product_clone',
			],
			'sort' => 2
		],
		'SETTINGS' => [
			'label'     => 'Settings',
			'url'   	=> null,
			'parent'    => null,
			'action_names' => [
				'cminds_supplierfrontendproductuploader_settings_notifications',
				'cminds_supplierfrontendproductuploader_plan_renew',
 				'cminds_supplierfrontendproductuploader_plan_list',
 				'cminds_supplierfrontendproductuploader_settings_addbrand',
				'cminds_supplierfrontendproductuploader_settings_addcategory',
				'cminds_supplierfrontendproductuploader_settings_notifications',
				'cminds_supplierfrontendproductuploader_settings_addstore',
			],
			'sort' => 1
		],
		'NOTIFICATIONS' => [
			'label'     => 'Notifications',
			'url'   	=> 'supplier/settings/notifications',
			'parent'    => 'SETTINGS',
			'action_names' => [
				'cminds_supplierfrontendproductuploader_settings_notifications',
			],
			'sort' => 4
		],

		'CUSTOMBRAND' => [
			'label'     => 'Add Brand',
			'url'   	=> 'supplier/settings/addbrand',
			'parent'    => 'SETTINGS',
			'action_names' => [
				'cminds_supplierfrontendproductuploader_settings_addbrand',
			],
			'sort' => 1
		],

		'CUSTOMCATEGORY' => [
			'label'     => 'Add Category',
			'url'   	=> 'supplier/settings/addcategory',
			'parent'    => 'SETTINGS',
			'action_names' => [
				'cminds_supplierfrontendproductuploader_settings_addcategory',
			],
			'sort' => 2
		],
		'CUSTOMSTORE' => [
			'label'     => 'Add Store',
			'url'   	=> 'supplier/settings/addstore',
			'parent'    => 'SETTINGS',
			'action_names' => [
				'cminds_supplierfrontendproductuploader_settings_addstore',
			],
			'sort' => 2.2
		],

		'REPORTS' => [
			'label'     => 'Reports',
			'url'   	=> null,
			'parent'    => null,
			'action_names' => [
				'cminds_supplierfrontendproductuploader_product_ordered',
			],
			'sort' => 4
		],
		'REPORTS_ORDERED_ITEMS' => [
			'label'     => 'Ordered Items',
			'url'   	=> 'supplier/product/ordered',
			'parent'    => 'REPORTS',
			'sort' => 0
		]/*,
		'BACK' => [
			'label'     => 'Back to Home Page',
			'url'   	=> '/',
			'parent'    => null,
			'sort' => 5
		]*/


	];

	public function getFirstLevelMenuItems() {
		$arr = [];
		foreach($this->nav_items as $key => $item) {
			if($item['parent'] == null) {
				$arr[$key] = $item;
			}
		}
		$this->aasort($arr,"sort");
		return $arr;
	}

	public function getChildsMenuItems($parent) {
		$arr = [];
		foreach($this->nav_items as $item) {
			if($item['parent'] == $parent) {
				$arr[] = $item;
			}
		}
		$this->aasort($arr,"sort");
		return $arr;
	}

	public function addMenuItem($key,$value) {

		$this->nav_items[$key] = $value;

	}

	public function getMarkedProductCount() {
		if($this->_markedProductIds == NULL) {
			$this->_markedProductIds = $this->getMarkedProduct();
		}

		return count($this->_markedProductIds);
	}

	public function hasMarkedProducts() {
		return ($this->getMarkedProductCount() > 0);
	}

	public function getMarkedProduct() {
		$count = array();

		$collection = Mage::getResourceModel('supplierfrontendproductuploader/product_collection')
			->filterBySupplier(Mage::helper('supplierfrontendproductuploader')->getSupplierId());

		foreach($collection AS $product) {
			$count[] = $product->getId();
		}

		return $count;
	}

	/**
	 * Render block HTML
	 *
	 * @return string
	 */
	protected function _toHtml()
	{
		Mage::dispatchEvent('supplierfrontendproductuploader_nav_load', array(
			'items' => &$this->nav_items,
		));
		if (!$this->getTemplate()) {
			return '';
		}
		$html = $this->renderView();
		return $html;
	}

	function aasort (&$array, $key) {
		$sorter=array();
		$ret=array();
		reset($array);
		foreach ($array as $ii => $va) {
			$sorter[$ii]=$va[$key];
		}
		asort($sorter);
		foreach ($sorter as $ii => $va) {
			$ret[$ii]=$array[$ii];
		}
		$array=$ret;
	}
}