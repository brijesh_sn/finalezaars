<?php
class Cminds_Supplierfrontendproductuploader_Model_Source_Suppliers extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {
    public function getAllOptions() {
        $collection = Mage::getModel('customer/customer')
        ->getCollection()
        ->addAttributeToSelect('*')
        ->addFieldToFilter('group_id', Mage::getStoreConfig('supplierfrontendproductuploader_catalog/supplierfrontendproductuploader_supplier_config/editor_group_id'));

        $this->_options[] = array('label'=> NULL, 'value' => NULL);
        
        foreach ($collection as $customer) {
            if(!$this->_isSet($customer->getId())) {
                $fullName = Mage::helper('supplierfrontendproductuploader')->getSupplierName($customer->getId());
                $this->_options[] = array('label'=> $fullName, 'value' => $customer->getId());
            }
        }

		if(Mage::getStoreConfig('supplierfrontendproductuploader_catalog/supplierfrontendproductuploader_supplier_config/supplier_group_id') != Mage::getStoreConfig('supplierfrontendproductuploader_catalog/supplierfrontendproductuploader_supplier_config/editor_group_id')) {

            $collection = Mage::getModel('customer/customer')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('group_id', Mage::getStoreConfig('supplierfrontendproductuploader_catalog/supplierfrontendproductuploader_supplier_config/supplier_group_id'));

            foreach ($collection as $customer) {
                if(!$this->_isSet($customer->getId())) {
                    $fullName = Mage::helper('supplierfrontendproductuploader')->getSupplierName($customer->getId());
                    $this->_options[] = array('label'=> $fullName, 'value' => $customer->getId());
                }
            }
        }
        return $this->_options;
    }
    private function _isSet($vendor_id) {
        foreach($this->_options AS $option) {
            if($option['value'] == $vendor_id) return true;
        }
        return false;
    }

    public function toOptionArray() {
        return $this->getAllOptions();
    }
}