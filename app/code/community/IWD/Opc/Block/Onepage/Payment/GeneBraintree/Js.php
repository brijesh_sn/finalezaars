<?php

$file = Mage::getBaseDir('code') . '/community/Gene/Braintree/Block/Js.php';
if (file_exists($file)) {
    if (!class_exists('Gene_Braintree_Block_Js', false))
        include_once($file);

    class IWD_Opc_Block_Onepage_Payment_GeneBraintree_Js extends Gene_Braintree_Block_Js
    {

    }
} else {
    class IWD_Opc_Block_Onepage_Payment_GeneBraintree_Js extends Mage_Core_Block_Template
    {

        public function _toHtml()
        {
            return '';
        }
    }
}
