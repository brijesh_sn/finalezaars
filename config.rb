http_path = "/"
css_dir = "skin/frontend/searchnative/default/css"
sass_dir = "skin/frontend/searchnative/default/scss"
images_dir = "skin/frontend/searchnative/default/images"
javascripts_dir = "skin/frontend/searchnative/default/js"
fonts_dir = "skin/frontend/searchnative/default/fonts"

output_style = :expanded # :expanded or :nested or :compact or :compressed
environment = :development

line_comments = false
cache = true
color_output = false # required for mixture

Sass::Script::Number.precision = 7 # chrome needs a precision of 7 to round properly